
import java.util.*;

public class Codility1BinaryGap {

    static int solution1(int N) {
        String s = Integer.toBinaryString(N);
        Map<Integer, Integer> results = new HashMap<>();
        System.out.println(s);

        int length = 0;
        int start = -1;
        for ( int i = 0; i < s.length(); i++ ) {
            if (s.charAt(i) == '0') {
                if (start == -1)
                    start = i;
                length++;
            }
            else {
                if (start >= 0) {
                    results.put(start, length);
                    start = -1;
                    length = 0;
                }
            }
        }

        System.out.println(s.toCharArray());
        System.out.println(results);

        Optional<Map.Entry<Integer, Integer>> max = results
                .entrySet()
                .stream()
                .max(Comparator.comparing(Map.Entry::getValue));

        if (max.isPresent())
            return max.get().getValue();

        return 0;
    }

    static int solution2(int N) {
        String s = Integer.toBinaryString(N);

        System.out.println(s);

        boolean start = false;
        int i = 0;
        int length = 0;
        int max = 0;

        do {
            if ( s.charAt(i) == '0' ) {
                if ( !start ) {
                    start = true;
                }
                length++;
            }
            else {
                if (start) {
                    if ( length > max )
                        max = length;
                    start = false;
                    length = 0;
                }
            }
            i++;
        } while ( i < s.length() );

        return max;
    }

    static int solution3(int N) {
        String x = "";

        int n = N;
        while ( n > 0 ) {
            int a = n % 2;
            x = a + x;
            n = n / 2;
        }

        System.out.println(x);

        boolean start = false;
        int i = 0;
        int length = 0;
        int max = 0;

        do {
            if ( x.charAt(i) == '0' ) {
                if ( !start ) {
                    start = true;
                }
                length++;
            }
            else {
                if (start) {
                    if ( length > max )
                        max = length;
                    start = false;
                    length = 0;
                }
            }
            i++;
        } while ( i < x.length() );

        return max;
    }

    public static void main(String[] args) {
        System.out.println("s1: " + solution1(1041));
        System.out.println("s2: " + solution2(1041));
        System.out.println("s3: " + solution3(1041));
    }

}
