package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 53. Maximum Subarray
 * Easy
 *
 * Given an integer array nums, find the contiguous subarray (containing at least one number)
 * which has the largest sum and return its sum.
 *
 * Example:
 *
 * Input: [-2,1,-3,4,-1,2,1,-5,4],
 * Output: 6
 * Explanation: [4,-1,2,1] has the largest sum = 6.
 * Follow up:
 *
 * If you have figured out the O(n) solution, try coding another solution using the
 * divide and conquer approach, which is more subtle.
 *
 */
public class Easy_53_MaximumSubarray {

    // working, needs optimization
    static int maxSubArray(int[] nums) {
        if ( nums.length == 0 )
            return 0;

        if ( nums.length == 1 )
            return nums[0];

        int result = 0;

        int pmax = -Integer.MAX_VALUE;
        int pmaxi = -1;

        for ( int i = 0; i < nums.length; i++ ) {
            // first we need to find array pivot which is maximum value which is not in the last element
            if ( nums[i] > pmax ) { // && i < nums.length - 1 ) {
                 pmax = nums[i];
                 pmaxi = i;
            }
        }

//        System.out.println(pmax);
//        System.out.println(pmaxi);

//        if ( pmaxi == nums.length - 1 )
//            return pmax;

        int max = pmax;
        int maxi = pmaxi, maxj = -1;

        for ( int i = 0; i < nums.length - 1; i++ ) {
            result += nums[i];
            if ( result > max && i == nums.length - 1 ) {
                max = result;
            }
            int j = i + 1;
            while ( j < nums.length ) {
                result += nums[j];
                System.out.println("result= " + result + ", i= " + i + ", j= " + j);
                if ( result > max ) {
                    max = result;
                    maxi = i;
                    maxj = j;
                }
                    j++;
//                }
//                else {
//                    result -= nums[j];
//                    break;
//                }
            }
            result = 0;
        }

        System.out.println("maxi: " + maxi + " maxj: " + maxj);

        return max;
    }

    @Test
    public void testMaxSubarray() {
        Assertions.assertEquals(6, maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
        Assertions.assertEquals(-1, maxSubArray(new int[]{-1,-2}));
        Assertions.assertEquals(3, maxSubArray(new int[]{1,2}));
    }

    public static void main(String[] args) {
        System.out.println(maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
//        System.out.println(maxSubArray(new int[]{-2,1,-3,1,-1,2,1,-5,4}));
//        System.out.println(maxSubArray(new int[]{-2,6,-3,1,-1,2,1,-5,4}));
//        System.out.println(maxSubArray(new int[]{-1,-2}));
//        System.out.println(maxSubArray(new int[]{1,2}));
    }

}
