package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * 190. Reverse Bits
 * Easy
 * Reverse bits of a given 32 bits unsigned integer.
 *
 *
 * Example 1:
 *
 * Input: 00000010100101000001111010011100
 * Output: 00111001011110000010100101000000
 * Explanation: The input binary string 00000010100101000001111010011100 represents the unsigned integer 43261596,
 * so return 964176192 which its binary representation is 00111001011110000010100101000000.
 * Example 2:
 *
 * Input: 11111111111111111111111111111101
 * Output: 10111111111111111111111111111111
 * Explanation: The input binary string 11111111111111111111111111111101 represents the unsigned integer 4294967293,
 * so return 3221225471 which its binary representation is 10111111111111111111111111111111.
 *
 * Note:
 *
 * Note that in some languages such as Java, there is no unsigned integer type. In this case, both input and
 * output will be given as signed integer type and should not affect your implementation, as the internal binary
 * representation of the integer is the same whether it is signed or unsigned.
 *
 * In Java, the compiler represents the signed integers using 2's complement notation. Therefore, in Example 2 above
 * the input represents the signed integer -3 and the output represents the signed integer -1073741825
 */
public class Easy_190_ReverseBits {

    // slow and wrong
    static int reverseBits0(int n) {
        String s = Integer.toString(n, 2);
        System.out.println(s);

        char[] buffer = new char[32];

        for (int i = 0; i < s.length(); i++)
            buffer[i] = s.charAt(i);

        int i = 0;
        int j = s.length() - 1;

        while ( i < j ) {
            char tmp = buffer[i];
            buffer[i] = buffer[j];
            buffer[j] = tmp;
            i++;
            j--;
        }

        for (int k = s.length(); k < 32; k++)
            buffer[k] = '0';

        System.out.println(Arrays.toString(buffer));
        System.out.println(String.valueOf(buffer));
        System.out.println(Long.valueOf(String.valueOf(buffer), 2));

        return Integer.valueOf(String.valueOf(buffer), 2);
    }

    // lol
    static int reverseBits1(int n) {
        return Integer.reverse(n);
    }

    static int reverseBits2(int n) {
        int bits = 32;
        int[] result_bits = new int[bits];

        while (bits > 0) {
            int bit = n & 1;
            result_bits[32 - bits] = bit;
            n >>= 1;
            bits--;
        }

        System.out.println(Arrays.toString(result_bits));

        int result = 0;

        for (int i = 0; i < 32; i++) {
            result <<= 1;
            result |= result_bits[i];
        }

        System.out.println(result);

        return result;
    }

    /**
     0x55555555 = 01010101010101010101010101010101
     0xAAAAAAAA = 10101010101010101010101010101010
     0x33333333 = 00110011001100110011001100110011
     0xCCCCCCCC = 11001100110011001100110011001100
     0x0F0F0F0F = 00001111000011110000111100001111
     0xF0F0F0F0 = 11110000111100001111000011110000
     0x00FF00FF = 00000000111111110000000011111111
     0xFF00FF00 = 11111111000000001111111100000000
     0x0000FFFF = 00000000000000001111111111111111
     0xFFFF0000 = 11111111111111110000000000000000
     */
    static int reverseBits3(int n) {
        System.out.println(Integer.toString(n, 2));
        n = ( n & 0x55555555 ) << 1 | ( n & 0xaaaaaaaa ) >>> 1;
        n = ( n & 0x33333333 ) << 2 | ( n & 0xcccccccc ) >>> 2;
        n = ( n & 0x0f0f0f0f ) << 4 | ( n & 0xf0f0f0f0 ) >>> 4;
        n = ( n & 0x00ff00ff ) << 8 | ( n & 0xff00ff00 ) >>> 8;
        n = ( n & 0x0000ffff ) << 16 | ( n & 0xffff0000 ) >>> 16;
        System.out.println(Integer.toString(n, 2));
        return n;
    }

    static int reverseBits(int n) {
        System.out.println(Integer.toString(n, 2));
        int result = 0;
        // do 32 times (32-bit unsigned integer)
        for (int i = 0; i < 32; i++) {
            // add n & 1 =  to result
            System.out.println("n & 1 = " + (n % 1));
            result += n & 1;
            System.out.println(Integer.toString(result, 2));
            // unsigned divide by 2 (bitwise right shift) has to be used for sign bit to be ignored and 0 pushed in front
            n >>>= 1;
            // do not multiply by 2 (bitwise left shift) the last one
            if ( i < 31 )
                result <<= 1;
        }
        return result;
    }

    // helper function to check results
    static long signedIntToUnsignedLong(int x) {
        return ((long)x) & 0xFFFF_FFFFL;
    }

    // helper function to check results
    static int unsignedLongToSignedInt(long x) {
        return (int)(( x << 1 ) >> 1);
    }

    // input:  00000010100101000001111010011100
    // output: 00111001011110000010100101000000
    @Test
    public void testReverseBits() {
        Assertions.assertEquals(964176192, reverseBits(43261596));
        Assertions.assertEquals(964176192L, signedIntToUnsignedLong(reverseBits(43261596)));
    }

    // input:  11111111111111111111111111111101 (-3 signed = 4294967293 unsigned)
    // output: 10111111111111111111111111111111 (-1073741825 signed = 3221225471 unsigned)
    @Test
    public void testReverseBits2() {
        Assertions.assertEquals(-1073741825, reverseBits(-3));
        Assertions.assertEquals(3221225471L, signedIntToUnsignedLong(reverseBits(unsignedLongToSignedInt(-3L))));
    }

    public static void main(String[] args) {
        System.out.println(signedIntToUnsignedLong(-1073741825));
        System.out.println(unsignedLongToSignedInt(4294967293L));
        System.out.println(reverseBits(43261596));
        System.out.println(reverseBits(-3));
    }

}
