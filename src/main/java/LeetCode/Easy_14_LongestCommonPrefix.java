package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 14. Longest Common Prefix
 * Easy
 *
 * Write a function to find the longest common prefix string amongst an array of strings.
 *
 * If there is no common prefix, return an empty string "".
 *
 * Example 1:
 *
 * Input: ["flower","flow","flight"]
 * Output: "fl"
 * Example 2:
 *
 * Input: ["dog","racecar","car"]
 * Output: ""
 * Explanation: There is no common prefix among the input strings.
 * Note:
 *
 * All given inputs are in lowercase letters a-z.
 */
public class Easy_14_LongestCommonPrefix {

    //version 1

    public static String longestCommonPrefix(String[] strs) {
        String result = "";
        for (int i=0;i<strs.length;i++) {
            String str = strs[i];
            int prefixLength = 1;
            while (prefixLength <= str.length()) {
                String sub = str.substring(0, prefixLength);
//                System.out.println(sub);

                // search for the others starting with current prefix except current element
                int totalMatched = 0;
                for (int j=0;j<strs.length;j++) {
                    if ( j != i ) {
                        String other = strs[j];
//                        System.out.println(other + " vs " + sub);
                        if ( other.startsWith(sub) ) {
                            totalMatched++;
                        }
//                        System.out.println(totalMatched);
                    }
                }
                if (totalMatched == strs.length - 1)
                    result = sub;

                prefixLength++;
            }
        }
        return result;
    }

    // version 2
    public static String longestCommonPrefix2(String[] strings) {
        if ( strings == null || strings.length == 0 )
            return "";
        String result = strings[0];
        for ( int i = 1; i < strings.length; i++ )
            while ( !strings[i].startsWith(result) )
                result = result.substring(0, result.length() - 1);
        return result;
    }

    // version 2 memory repeat
    public static String longestCommonPrefix3(String[] strings) {
        if ( strings == null || strings.length == 0 )
            return "";
        String prefix = strings[0];
        for ( int i=1; i < strings.length; i++ )
            while ( !(strings[i].startsWith(prefix)) ) {
                prefix = prefix.substring(0, prefix.length() - 1);
                System.out.println(prefix);
            }
        return prefix;
    }

    @Test
    public void testLongestCommonPrefix() {
        String[] input1 = { "flower", "flow", "flight" };
        String[] input2 = { "dog", "racecar", "car" };
        Assertions.assertEquals("fl", longestCommonPrefix(input1));
        Assertions.assertEquals("", longestCommonPrefix(input2));
    }

    @Test
    public void testLongestCommonPrefix2() {
        String[] input1 = { "flower", "flow", "flight" };
        String[] input2 = { "dog", "racecar", "car" };
        Assertions.assertEquals("fl", longestCommonPrefix2(input1));
        Assertions.assertEquals("", longestCommonPrefix2(input2));
    }

    @Test
    public void testLongestCommonPrefix3() {
        String[] input1 = { "flower", "flow", "flight" };
        String[] input2 = { "dog", "racecar", "car" };
        Assertions.assertEquals("fl", longestCommonPrefix3(input1));
        Assertions.assertEquals("", longestCommonPrefix3(input2));
    }

    public static void main(String[] args) {
        String[] input1 = { "flower", "flow", "flight" };
        String[] input2 = { "dog", "racecar", "car" };
        System.out.println(longestCommonPrefix(input1));
        System.out.println(longestCommonPrefix(input2));
        System.out.println(longestCommonPrefix2(input1));
        System.out.println(longestCommonPrefix2(input2));
    }

}
