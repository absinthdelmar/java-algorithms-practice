package LeetCode;

import java.util.HashSet;
import java.util.Set;

/**
 * 258. Add Digits
 * Easy
 *
 * Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.
 *
 * Example:
 *
 * Input: 38
 * Output: 2
 * Explanation: The process is like: 3 + 8 = 11, 1 + 1 = 2.
 *              Since 2 has only one digit, return it.
 * Follow up:
 * Could you do it without any loop/recursion in O(1) runtime?
 */
public class Easy_258_AddDigits {

    static int sum_digits(int num) {
        int sum = 0;
        while ( num > 0 ) {
            sum += num % 10;
            num /= 10;
        }
        return sum;
    }

    // 2 ms
    static int addDigits0(int num) {
        Set<Integer> set = new HashSet<>();
        while ( !set.contains(num) ) {
            set.add(num);
            num = sum_digits(num);
        }
        return num;
    }

    // 1 ms
    // https://en.wikipedia.org/wiki/Digital_root
    static int addDigits1(int num) {
        // for a base 10
        return num - ( 10 - 1 ) * (( num - 1 ) / ( 10 - 1 ));
    }

    static int addDigits(int num) {
        return num == 0 ? 0 : 1 + ( num - 1 ) % ( 10 - 1 );
    }

    public static void main(String[] args) {
        System.out.println(addDigits(38));
    }

}
