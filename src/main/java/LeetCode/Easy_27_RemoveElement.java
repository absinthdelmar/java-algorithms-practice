package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * 27. Remove Element
 * Easy
 *
 * Given an array nums and a value val, remove all instances of that value in-place and return the new length.
 *
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 *
 * The order of elements can be changed. It doesn't matter what you leave beyond the new length.
 *
 * Example 1:
 *
 * Given nums = [3,2,2,3], val = 3,
 *
 * Your function should return length = 2, with the first two elements of nums being 2.
 *
 * It doesn't matter what you leave beyond the returned length.
 * Example 2:
 *
 * Given nums = [0,1,2,2,3,0,4,2], val = 2,
 *
 * Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.
 *
 * Note that the order of those five elements can be arbitrary.
 *
 * It doesn't matter what values are set beyond the returned length.
 * Clarification:
 *
 * Confused why the returned value is an integer but your answer is an array?
 *
 * Note that the input array is passed in by reference, which means modification to the input array will be known to the caller as well.
 *
 * Internally you can think of this:
 *
 * // nums is passed in by reference. (i.e., without making a copy)
 * int len = removeElement(nums, val);
 *
 * // any modification to nums in your function would be known by the caller.
 * // using the length returned by your function, it prints the first len elements.
 * for (int i = 0; i < len; i++) {
 *     print(nums[i]);
 * }
 *
 */
public class Easy_27_RemoveElement {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    // my version
    static int removeElement(int[] nums, int val) {
        int count = 0;
        for ( int i = 0; i < nums.length - count; i++ ) {
            if ( nums[i] == val ) {
                count++;
                swap(nums, i, nums.length - count);
                i--;
            }
        }
        return nums.length - count;
    }

    // more efficient version
    static int removeElement2(int[] nums, int val) {
        int len = nums.length;
        for ( int i = len-1; i >= 0; i-- )
            if ( nums[i] == val )
                nums[i] = nums[--len];
        return len;
    }

    @Test
    public void testRemoveElement() {
        int[] nums1 = new int[]{3,2,2,3};
        int[] nums2 = new int[]{0,1,2,2,3,0,4,2};
        Assertions.assertEquals(2, removeElement(nums1, 3));
        Assertions.assertEquals(5, removeElement(nums2, 2));
    }

    @Test
    public void testRemoveElement2() {
        int[] nums1 = new int[]{3,2,2,3};
        int[] nums2 = new int[]{0,1,2,2,3,0,4,2};
        Assertions.assertEquals(2, removeElement2(nums1, 3));
        Assertions.assertEquals(5, removeElement2(nums2, 2));
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{3,2,2,3};
        int[] nums2 = new int[]{0,1,2,2,3,0,4,2};
        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        int value1 = 3;
        int value2 = 2;
        int len1 = removeElement(nums1, value1);
        int len2 = removeElement(nums2, value2);
        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        for (int i=0;i<len1;i++)
            System.out.print(nums1[i]);
        System.out.println();
        for (int i=0;i<len2;i++)
            System.out.print(nums2[i]);
        System.out.println();
    }

}
