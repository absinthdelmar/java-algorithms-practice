package LeetCode;

public class Easy_326_PowerOf3 {

    // iterative
    // 11 ms 87.5% faster, less memory than 74%
    static boolean isPowerOfThree0(int n) {
        System.out.println("n = " + n);
        if ( n <= 0 || n == 2 )
            return false;
        else if ( n == 1 )
            return true;

        int rem = n % 3;

        while ( n > 3 && rem == 0 ) {
            n = n / 3;
            rem = n % 3;
            System.out.println("rem: " + rem);
            System.out.println("n: " + n);
        }

        System.out.println("last rem: " + rem);

        return rem == 0;
    }

    // recursive
    // same time and memory
    static boolean isPowerOfThree(int n) {
        if ( n <= 0 || n == 2 )
            return false;
        else if ( n == 1 )
            return true;

        int rem = n % 3;

        if ( n > 3 && rem == 0 )
            return isPowerOfThree(n / 3);

        return rem == 0;
    }

    public static void main(String[] args) {
//        int n = -2147483648;
//        System.out.println(isPowerOfThree(n));
        System.out.println(isPowerOfThree(3));
        System.out.println(isPowerOfThree(9));
        System.out.println(isPowerOfThree(27));
        System.out.println(isPowerOfThree(81));
        System.out.println();
        System.out.println(isPowerOfThree(19684));
    }
}
