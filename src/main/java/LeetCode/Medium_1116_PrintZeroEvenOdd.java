package LeetCode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntConsumer;

/**
 * 1116. Print Zero Even Odd
 * Medium
 *
 * Suppose you are given the following code:
 *
 * class ZeroEvenOdd {
 *   public ZeroEvenOdd(int n) { ... }      // constructor
 *   public void zero(printNumber) { ... }  // only output 0's
 *   public void even(printNumber) { ... }  // only output even numbers
 *   public void odd(printNumber) { ... }   // only output odd numbers
 * }
 * The same instance of ZeroEvenOdd will be passed to three different threads:
 *
 * Thread A will call zero() which should only output 0's.
 * Thread B will call even() which should only ouput even numbers.
 * Thread C will call odd() which should only output odd numbers.
 * Each of the threads is given a printNumber method to output an integer.
 * Modify the given program to output the series 010203040506... where the length of the series must be 2n.
 *
 *
 *
 * Example 1:
 *
 * Input: n = 2
 * Output: "0102"
 * Explanation: There are three threads being fired asynchronously.
 * One of them calls zero(), the other calls even(), and the last one calls odd(). "0102" is the correct output.
 * Example 2:
 *
 * Input: n = 5
 * Output: "0102030405"
 *
 */
public class Medium_1116_PrintZeroEvenOdd {

    // we can only modify ZeroEvenOdd class

    // slow 15 ms using locking on self and zeroNext boolean
    static class ZeroEvenOdd0 {
        private int n;
        private int count = 1;
        private boolean zeroNext = true;

        public ZeroEvenOdd0(int n) {
            this.n = n;
        }

        // printNumber.accept(x) outputs "x", where x is an integer.
        public void zero(IntConsumer printNumber) throws InterruptedException {
            for (int i = 1; i <= n; i++) {
                synchronized (this) {
                    while (!zeroNext)
                        wait();
                    printNumber.accept(0);
                    zeroNext = false;
                    notifyAll();
                }
            }
        }

        public void even(IntConsumer printNumber) throws InterruptedException {
            for (int i = 2; i <= n; i+=2) {
                synchronized (this) {
                    while (zeroNext || count % 2 != 0)
                        wait();
                    printNumber.accept(i);
                    zeroNext = true;
                    count++;
                    notifyAll();
                }
            }
        }

        public void odd(IntConsumer printNumber) throws InterruptedException {
            for (int i = 1; i <= n; i+=2) {
                synchronized (this) {
                    while (zeroNext || count % 2 == 0)
                        wait();
                    printNumber.accept(i);
                    zeroNext = true;
                    count++;
                    notifyAll();
                }
            }
        }
    }

    // much cleaner version using Semaphore 6ms
    static class ZeroEvenOdd {
        private int n;
//        private int evenCount;
//        private int oddCount;
        private Semaphore semaphoreZero = new Semaphore(1);
        private Semaphore semaphoreEven = new Semaphore(0);
        private Semaphore semaphoreOdd = new Semaphore(0);

        public ZeroEvenOdd(int n) {
            this.n = n;
//            this.evenCount = n / 2;
//            this.oddCount = (int)Math.ceil((double)n / 2);
        }

        // printNumber.accept(x) outputs "x", where x is an integer.
        public void zero(IntConsumer printNumber) throws InterruptedException {
            for (int i = 0; i < n; i++) {
                try {
                    semaphoreZero.acquire();
                    printNumber.accept(0);
                }
                finally {
                    if ( i % 2 != 0 )
                        semaphoreEven.release();
                    else
                        semaphoreOdd.release();
                }
            }
        }

        public void even(IntConsumer printNumber) throws InterruptedException {
//            int nextEven = 2;
//            for (int i = 0; i < evenCount; i++) {
//                semaphoreEven.acquire();
//                printNumber.accept(nextEven);
//                nextEven += 2;
//                semaphoreZero.release();
//            }
            for (int i = 2; i <= n; i+=2) {
                try {
                    semaphoreEven.acquire();
                    printNumber.accept(i);
                }
                finally {
                    semaphoreZero.release();
                }
            }
        }

        public void odd(IntConsumer printNumber) throws InterruptedException {
//            int nextOdd = 1;
//            for (int i = 0; i < oddCount; i++) {
//                semaphoreOdd.acquire();
//                printNumber.accept(nextOdd);
//                nextOdd += 2;
//                semaphoreZero.release();
//            }
            for (int i = 1; i <= n; i+=2) {
                try {
                    semaphoreOdd.acquire();
                    printNumber.accept(i);
                }
                finally {
                    semaphoreZero.release();
                }
            }
        }
    }

    // helper code below this line

    static class T implements Runnable {
        ZeroEvenOdd zeroEvenOdd;
        String data;

        T(ZeroEvenOdd zeroEvenOdd, String data) {
            this.zeroEvenOdd = zeroEvenOdd;
            this.data = data;
        }

        public void run() {
            try {
                Method method = zeroEvenOdd.getClass().getMethod(data, IntConsumer.class);
                method.invoke(zeroEvenOdd, (IntConsumer)System.out::print );
            }
            catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ZeroEvenOdd zeroEvenOdd = new ZeroEvenOdd(5);
//        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ExecutorService executorService = Executors.newWorkStealingPool();
        executorService.submit(new T(zeroEvenOdd,"zero"));
        executorService.submit(new T(zeroEvenOdd, "even"));
        executorService.submit(new T(zeroEvenOdd, "odd"));
        executorService.shutdown();
        if ( !executorService.awaitTermination(2, TimeUnit.SECONDS ) ) {
            System.out.println("Threads didn't finish in 2 seconds!");
        }
    }

}
