package LeetCode;

public class Easy_342_PowerOf4 {

    // iterative, slow
    static boolean isPowerOfFour0(int n) {
        if ( n < 1 )
            return false;
        while ( n % 4 == 0 )
            n /= 4;
        return n == 1;
    }

    // low numbers checking to speed up and bitwise mask checking
    // fast 1 ms
    static boolean isPowerOfFour(int n) {
        System.out.println("n = " + n);
        if ( n == 1 || n == 4 )
            return true;
        else if ( n < 4 )
            return false;
        // is power of 2
        // 0x5 = 0101 -> 0x55 = 01010101 -> 0x55555555 (32 bits) = 01010101010101010101010101010101
        return ( n & ( n - 1 ) ) == 0 && (n & 0x55555555) != 0;
    }

    public static void main(String[] args) {
//        int n = -2147483648;
//        System.out.println(isPowerOfFour(n));
        System.out.println(isPowerOfFour(4));
        System.out.println(isPowerOfFour(16));
        System.out.println(isPowerOfFour(32));
        System.out.println(isPowerOfFour(64));
        System.out.println(isPowerOfFour(128));
        System.out.println(isPowerOfFour(256));
   }
}
