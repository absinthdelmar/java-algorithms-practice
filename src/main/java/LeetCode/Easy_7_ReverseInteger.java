package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a 32-bit signed integer, reverse digits of an integer.
 *
 * Example 1:
 *
 * Input: 123
 * Output: 321
 * Example 2:
 *
 * Input: -123
 * Output: -321
 * Example 3:
 *
 * Input: 120
 * Output: 21
 * Note:
 *
 * Assume we are dealing with an environment which could only store integers
 * within the 32-bit signed integer range: [−2^31,  2^31 − 1].
 *
 * For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
 */
public class Easy_7_ReverseInteger {
    // dirty way
    static int reverse(int x) {
        boolean negative = x < 0;
        String s = Integer.toString(negative ? -x : x);
        char[] input = s.toCharArray();
        char[] output = new char[input.length];
        int i = input.length - 1, j = 0;
        while (i>=0 && j < input.length) {
            output[j] = input[i];
            i--;
            j++;
        }
        try {
            int result = Integer.parseInt(String.copyValueOf(output));
            return negative ? -result : result;
        }
        catch (NumberFormatException e) {
            return 0;
        }
    }

    // 2nd version

    // overflow check addition
    static int add(int x, int y) throws ArithmeticException {
        long result = (long)x + (long)y;
        if ( (int)result != result )
            throw new ArithmeticException("integer overflow");
        return (int)result;
    }

    // overflow check multiplication
    static int mult(int x, int y) throws ArithmeticException {
        long result = (long)x * (long)y;
        if ( (int)result != result )
            throw new ArithmeticException("integer overflow");
        return (int)result;
    }

    static int reverse2(int x) {
        try {
            boolean negative = x < 0;

            if ( negative )
                x = mult(x, -1);

            int lastDigit = x % 10, remainder = x / 10, reverse = 0;

            while (remainder > 0) {
                reverse = mult(reverse, 10);
                reverse = add(reverse, lastDigit);

                lastDigit = remainder % 10;
                remainder = remainder / 10;
            }
            reverse = mult(reverse, 10);
            reverse = add(reverse, lastDigit);

            return negative ? -reverse : reverse;
        }
        catch(ArithmeticException e){
            System.out.println(e.getMessage());
            return 0;
        }
    }

    // 3rd version

    static int reverse3(int x) {
        if ( x < 0 ) {
            if ( x <= -Integer.MAX_VALUE )
                return 0;
            x = x * -1;
            return reverse3number(x) * -1;
        }
        return reverse3number(x);
    }

    static int reverse3number(int x) {
        List<Integer> digits = split(x);
        long result = 0;
        for (int i=0; i <= digits.size()-1; i++) {
            result = (result * 10) + digits.get(i);
            if (result > Integer.MAX_VALUE)
                return 0;
        }
        return (int)result;
    }

    static List<Integer> split(int x) {
        List<Integer> digits = new ArrayList<>();
        int c = x / 10;
        int remainder = x % 10;
        digits.add(remainder);
        while ( c > 0 ) {
            remainder = c % 10;
            digits.add(remainder);
            c = c / 10;
        }
        return digits;
    }

    @Test
    public void testReverse2() {
        Assertions.assertEquals(reverse2(123), 321);
        Assertions.assertEquals(reverse2(-123), -321);
        Assertions.assertEquals(reverse2(120), 21);
        Assertions.assertEquals(reverse2(-1234), -4321);
        Assertions.assertEquals(reverse2(1534236469), 0);
        Assertions.assertEquals(reverse2(-99591), -19599);
        Assertions.assertEquals(reverse2(-2147483648), 0);
    }

    @Test
    public void testReverse3() {
        Assertions.assertEquals(reverse3(123), 321);
        Assertions.assertEquals(reverse3(-123), -321);
        Assertions.assertEquals(reverse3(120), 21);
        Assertions.assertEquals(reverse3(-1234), -4321);
        Assertions.assertEquals(reverse3(1534236469), 0);
        Assertions.assertEquals(reverse3(-99591), -19599);
        Assertions.assertEquals(reverse3(-2147483648), 0);
    }

    public static void main(String[] args) {
        System.out.println(reverse2(123));
        System.out.println(reverse2(-123));
        System.out.println(reverse2(120));
        System.out.println(reverse2(-1234));
        System.out.println(reverse2(1534236469));
        System.out.println(reverse2(-99591));
        System.out.println(reverse2(-2147483648));
    }
}
