package LeetCode;

/**
 * 237. Delete Node in a Linked List
 * Easy
 *
 * Write a function to delete a node (except the tail) in a singly linked list, given only access to that node.
 *
 * Given linked list -- head = [4,5,1,9], which looks like following:
 *
 * 4 -> 5 -> 1 -> 9
 *
 *
 * Example 1:
 *
 * Input: head = [4,5,1,9], node = 5
 * Output: [4,1,9]
 * Explanation: You are given the second node with value 5, the linked list should become 4 -> 1 -> 9 after calling your function.
 * Example 2:
 *
 * Input: head = [4,5,1,9], node = 1
 * Output: [4,5,9]
 * Explanation: You are given the third node with value 1, the linked list should become 4 -> 5 -> 9 after calling your function.
 *
 *
 * Note:
 *
 * The linked list will have at least two elements.
 * All of the nodes' values will be unique.
 * The given node will not be the tail and it will always be a valid node of the linked list.
 * Do not return anything from your function.
 */
public class Easy_237_DeleteNodeLinkedList {

    // 0 ms we must replace actual values of current node with next node value
    // and set current.next to current.next.next
    // so it is not an actual delete, but a replacement
    // using temp and calling a garbage collector manually shows less memory usage, but is optional
    static void deleteNode(ListNode node) {
//        ListNode temp = node.next;
        node.val = node.next.val;
        node.next = node.next.next;
//        System.gc();
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(4);
        ListNode toDelete = new ListNode(5);
        head.next = toDelete;
        head.next.next = new ListNode(1);
        head.next.next.next = new ListNode(9);
        System.out.println("initial: " + head);
        System.out.println("toDelete: " + toDelete);
        deleteNode(toDelete);
        System.out.println("final: " + head);
    }

}
