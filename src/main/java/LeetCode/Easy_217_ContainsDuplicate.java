package LeetCode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 217. Contains Duplicate
 * Easy
 *
 * Given an array of integers, find if the array contains any duplicates.
 *
 * Your function should return true if any value appears at least twice in the array, and it should return false
 * if every element is distinct.
 *
 * Example 1:
 *
 * Input: [1,2,3,1]
 * Output: true
 * Example 2:
 *
 * Input: [1,2,3,4]
 * Output: false
 * Example 3:
 *
 * Input: [1,1,1,3,3,4,3,2,4,2]
 * Output: true
 */
public class Easy_217_ContainsDuplicate {

    // basic - time limit exceeded, too slow
    static boolean containsDuplicate0(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if ( nums[i] == nums[j] && i != j )
                    return true;
            }
        }
        return false;
    }

    // sorting array first, than just using 1 iteration to compare current and next elements
    // 3 ms
    static boolean containsDuplicate1(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0, j = 1; j < nums.length; i++, j++)
            if ( nums[i] == nums[j] )
                return true;
        return false;
    }

    // ?????
    static boolean containsDuplicate2(int[] nums) {
        nums = Arrays.copyOf(nums, nums.length + 1);
        int fast = 0, slow = 0, find = 0;
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
        } while ( slow != fast ) ;
        while ( find != slow ) {
            slow = nums[slow];
            find = nums[find];
        }
        System.out.println(find);
        return find != 0;
    }

    // 4 ms
    static boolean containsDuplicate3(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++)
            if ( !set.add(nums[i]) )
                return true;
        return false;
    }

    // 2 ms using boolean array of size max(nums) - min(nums) + 1 by flagging duplicate values
    static boolean containsDuplicate(int[] nums) {
        if ( nums == null || nums.length == 0 )
            return false;

        int min = nums[0], max = nums[0];

        for (int i = 0; i < nums.length; i++) {
            min = Math.min(min, nums[i]);
            max = Math.max(max, nums[i]);
        }

        boolean[] test = new boolean[max - min + 1];

        System.out.println(test.length);

        for (int i = 0; i < nums.length; i++) {
            if ( test[nums[i] - min] )
                return true;
            test[nums[i] - min] = true;
        }

        return false;
    }

    public static void main(String[] args) {
//        System.out.println(containsDuplicate2(new int[]{1,1}));
//        System.out.println(containsDuplicate2(new int[]{1,2,3,4}));
//        System.out.println(containsDuplicate2(new int[]{7,2,3,4,5,5,6,6,7}));
//        System.out.println(containsDuplicate2(new int[]{1,1,1,1,2}));
        System.out.println(containsDuplicate2(new int[]{3,3}));
    }
}
