package LeetCode;

import java.util.ArrayList;
import java.util.List;

public class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    public String toString() {
        String dive = next != null ? "->" + next.toString() : "";
        return val + dive;
    }
    static public List<Integer> toList(ListNode head) {
        List<Integer> result = new ArrayList<>();
        ListNode temp = head;
        while (temp != null) {
            result.add(temp.val);
            temp = temp.next;
        }
        return result;
    }
    static ListNode addNode(ListNode head, int value) {
        ListNode temp, p;
        // create a new listNode with specified value
        temp = new ListNode(value);
        // if head is not specified, our new listNode is the head
        if ( head == null ) {
            head = temp;
        }
        // otherwise, go to the end and add it
        else {
            p = head;
            while ( p.next != null ) {
                p = p.next;
            }
            p.next = temp;
        }
        return head;
    }
    static ListNode merge(ListNode l1, ListNode l2) {
        if (l1 == null)
            return l2;
        if (l2 == null)
            return l1;
        if (l1.val < l2.val) {
            l1.next = merge(l1.next, l2);
            return l1;
        }
        else {
            l2.next = merge(l1, l2.next);
            return l2;
        }
    }
    static boolean isEqual(ListNode l1, ListNode l2) {
        List<Integer> expected = new ArrayList<>();
        List<Integer> actual = new ArrayList<>();
        ListNode t1 = l1, t2 = l2;
        while ( t1 != null ) {
            expected.add(t1.val);
            t1 = t1.next;
        }
        while ( t2 != null ) {
            actual.add(t2.val);
            t2 = t2.next;
        }
        System.out.println(l1);
        System.out.println(l2);
        return expected.equals(actual);
    }

    static ListNode copy(ListNode head) {
        ListNode result = null;
        ListNode temp = head;
        while ( temp != null ) {
            result = addNode(result, temp.val);
            temp = temp.next;
        }
        return result;
    }
}
