package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 1328. Break a Palindrome
 * Medium
 *
 * Given a palindromic string palindrome, replace exactly one character by any lowercase English letter so that
 * the string becomes the lexicographically smallest possible string that isn't a palindrome.
 *
 * After doing so, return the final string.  If there is no way to do so, return the empty string.
 *
 *
 *
 * Example 1:
 *
 * Input: palindrome = "abccba"
 * Output: "aaccba"
 * Example 2:
 *
 * Input: palindrome = "a"
 * Output: ""
 *
 *
 * Constraints:
 *
 * 1 <= palindrome.length <= 1000
 * palindrome consists of only lowercase English letters.
 *
 */
public class Medium_1328_BreakPalindrome {

    static String breakPalindrome(String s) {
        int n = s.length();
        if ( n == 1 )
            return "";
        char[] ch = s.toCharArray();
        for ( int i = 0; i < n; i++ ) {
            int j = n - 1 - i;
            if ( i == j )
                continue;
            if ( ch[i] != 'a' ) {
                ch[i] = 'a';
                return String.valueOf(ch);
            }
        }
        ch[n-1] = 'b';
        return String.valueOf(ch);
    }

    @Test
    public void testBreakPalindrome() {
        Assertions.assertEquals("abccbb", breakPalindrome("bbccbb"));
    }

    public static void main(String[] args) {
        System.out.println(breakPalindrome("bbccbb"));
    }

}
