package LeetCode;

import java.util.*;

/**
 * 290. Word Pattern
 * Easy
 *
 * Given a pattern and a string str, find if str follows the same pattern.
 *
 * Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.
 *
 * Example 1:
 *
 * Input: pattern = "abba", str = "dog cat cat dog"
 * Output: true
 * Example 2:
 *
 * Input:pattern = "abba", str = "dog cat cat fish"
 * Output: false
 * Example 3:
 *
 * Input: pattern = "aaaa", str = "dog cat cat dog"
 * Output: false
 * Example 4:
 *
 * Input: pattern = "abba", str = "dog dog dog dog"
 * Output: false
 * Notes:
 * You may assume pattern contains only lowercase letters, and str contains lowercase letters that may be separated
 * by a single space.
 */
public class Easy_290_WordPattern {

    // will be slow, but should be working
    // FIXME
    static boolean wordPattern0(String pattern, String str) {
        String[] p = pattern.split("");
        String[] s = str.split("\\s");
        String[] s2 = new String[s.length];
        System.out.println(Arrays.toString(p));
        System.out.println(Arrays.toString(s));

        // basic case
        if ( p.length != s.length || s.length == 0 )
            return false;

        Map<String, Character> seen = new HashMap<>();

        char start = 'a';

        if ( !p[0].equals("a") )
            start = p[0].charAt(0);

        for (int i = 0; i < s.length; i++) {
            if ( seen.get(s[i]) == null ) {
                seen.put(s[i], start);
                start++;
            }
        }

        System.out.println(seen);

        for ( String key : seen.keySet() ) {
            for (int i = 0; i < s.length; i++) {
                System.out.println(key + " vs " + s[i]);
                if ( s[i].equals(key) )
                    s2[i] = String.valueOf(seen.get(key));
                System.out.println(" -> " + s2[i]);
            }
        }
        System.out.println(Arrays.toString(s2));

        return Arrays.equals(p, s2);
    }

    static boolean wordPattern1(String pattern, String str) {
        Map<Character, String> mc = new HashMap<>();
        Map<String, Character> ms = new HashMap<>();

        String[] words = str.split("\\s");

        if ( words.length != pattern.length() )
            return false;

        for (int i = 0; i < words.length; i++) {
            char c = pattern.charAt(i);
            String word = words[i];
            if ( mc.get(c) == null ) {
                if ( ms.get(word) != null )
                    return false;
                else {
                    mc.put(c, word);
                    ms.put(word, c);
                }
            }
            else {
                if ( !mc.get(c).equals(word) )
                    return false;
            }
        }
        return true;
    }

    static boolean wordPattern2(String pattern, String str) {
        Map<Object, Integer> mc = new HashMap<>();

        String[] words = str.split("\\s");

        if ( words.length != pattern.length() )
            return false;

        for (int i = 0; i < words.length; i++) {
            char c = pattern.charAt(i);
            String word = words[i];

            mc.putIfAbsent(c, i);
            mc.putIfAbsent(word, i);

            if ( !mc.get(c).equals(mc.get(word)) )
                return false;
        }
        return true;
    }

    static boolean wordPattern(String pattern, String str) {
        Map<Character, String> map = new HashMap<>();
        String[] words = str.split("\\s");

        if ( words.length != pattern.length() )
            return false;

        for (int i = 0; i < pattern.length(); i++) {
            if ( !map.containsKey(pattern.charAt(i)) ) {
                if ( map.containsValue(words[i]) )
                    return false;
                else
                    map.put(pattern.charAt(i), words[i]);
            }
            else {
                if ( !map.get(pattern.charAt(i)).equals(words[i]) )
                    return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(wordPattern("abba", "dog cat cat dog"));
        System.out.println(wordPattern("abcba", "dog cat tom cat dog"));
        System.out.println(wordPattern("abc", "b c a"));
        System.out.println(wordPattern("e", "eukera"));
        System.out.println(wordPattern("deadbeef", "d e a d b e e f"));
    }

}
