package LeetCode;

import java.util.Arrays;

/**
 * 26. Remove Duplicates from Sorted Array
 * Easy
 *
 * Given a sorted array nums, remove the duplicates in-place such that each element appear only once and return the new length.
 *
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
 *
 * Example 1:
 *
 * Given nums = [1,1,2],
 *
 * Your function should return length = 2, with the first two elements of nums being 1 and 2 respectively.
 *
 * It doesn't matter what you leave beyond the returned length.
 * Example 2:
 *
 * Given nums = [0,0,1,1,1,2,2,3,3,4],
 *
 * Your function should return length = 5, with the first five elements of nums being modified to 0, 1, 2, 3, and 4 respectively.
 *
 * It doesn't matter what values are set beyond the returned length.
 * Clarification:
 *
 * Confused why the returned value is an integer but your answer is an array?
 *
 * Note that the input array is passed in by reference, which means modification to the input array will be known to the caller as well.
 *
 * Internally you can think of this:
 *
 * // nums is passed in by reference. (i.e., without making a copy)
 * int len = removeDuplicates(nums);
 *
 * // any modification to nums in your function would be known by the caller.
 * // using the length returned by your function, it prints the first len elements.
 * for (int i = 0; i < len; i++) {
 *     print(nums[i]);
 * }
 */
public class Easy_26_RemoveDuplicatesFromSortedArray {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void shift(int[] arr, int start, int end) {
        while ( start < end )
            swap(arr, start, end--);
    }

    static int removeDuplicates(int[] nums) {
        int len = nums.length;
        int moved = 0;
        for ( int i = 0; i < len; i++ ) {
            for ( int j = 0; j < len - moved; j++ ) {
                System.out.println(i + ":" + j);
                if ( i < j && nums[i] == nums[j] ) {
                    shift(nums, j, len - 1 );
                    moved++;
                    System.out.println(moved + " => " + Arrays.toString(nums));
                    j--;
                }
            }
        }
        return len - moved;
    }

    // fast version from the web site
    static int removeDuplicates2(int[] nums) {
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if (nums[i] != nums[j]){
                i++;
                nums[i] = nums[j];
            }
        }
        return i + 1;
    }






    static int removeDuplicates3(int[] nums) {
        int i = 0;
        for ( int j = 1; j < nums.length; j++ ) {
            // not a duplicate
            if ( nums[i] != nums[j] ) {
                // increase pointer
                i++;
                nums[i] = nums[j];
                System.out.println(Arrays.toString(nums));
            }
        }
        return ++i;
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{1,1,2,2,3,4,5,6};
//        shift(nums1, 1, nums1.length - 1);
//        shift(nums1, 2, nums1.length - 1);
        int[] nums2 = new int[]{0,0,1,1,1,2,2,3,3,4};
        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        int len1 = removeDuplicates3(nums1);
        int len2 = removeDuplicates3(nums2);
        for (int i=0;i<len1;i++)
            System.out.print(nums1[i]);
        System.out.println();
        for (int i=0;i<len2;i++)
            System.out.print(nums2[i]);
        System.out.println();
    }

}
