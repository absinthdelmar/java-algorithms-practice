package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * 125. Valid Palindrome
 * Easy
 *
 * Share
 * Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
 *
 * Note: For the purpose of this problem, we define empty string as valid palindrome.
 *
 * Example 1:
 *
 * Input: "A man, a plan, a canal: Panama"
 * Output: true
 * Example 2:
 *
 * Input: "race a car"
 * Output: false
 *
 *
 * Constraints:
 *
 * s consists only of printable ASCII characters.
 *
 */
public class Easy_125_ValidPalindrome {

    static void swap(char[] arr, int i, int j) {
        char temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    // my version 27ms very slow, fast version would be without RegEx, arrayCopy, and on the same character array
    static boolean isPalindrome0(String s) {
        if ( s == null || s.isEmpty() )
            return true;

        Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");

        s = s.toLowerCase();
        s = pattern.matcher(s).replaceAll("");

        int n = s.length();
        char[] result = new char[n];

        for (int i = 0; i < n; i++) {
            result[i] = s.charAt(n - i - 1);
            System.out.println( i + " vs " + (n - i - 1) + " => " + result[i] );
        }

        System.out.println(s);
        System.out.println(result);

        return Arrays.equals(result, s.toCharArray());
    }

    // 6ms version, better be only direct ASCII char codes comparison
    static boolean isPalindrome(String s) {
        // no string reference passed
        if (s == null)
            return false;
        // consider empty string a palindrome
        if (s.isEmpty())
            return true;

        int i = 0, j = s.length() - 1;

        while ( i < j ) {
            char ci = s.charAt(i);
            char cj = s.charAt(j);
            // skip if not letter or digit
            if ( !Character.isLetterOrDigit(ci) )
                i++;
            // skip if not letter or digit
            else if ( !Character.isLetterOrDigit(cj) )
                j--;
            // exit immediately to save time
            else if ( Character.toLowerCase(ci) != Character.toLowerCase(cj) )
                return false;
            // move forward
            else {
                i++;
                j--;
            }
        }

        return true;
    }

    @Test
    public void testIsPalindrome() {
        Assertions.assertTrue(isPalindrome("A man, a plan, a canal: Panama"));
        Assertions.assertTrue(isPalindrome("Mr. Owl ate my metal worm"));
        Assertions.assertTrue(isPalindrome("Do geese see God?"));
        Assertions.assertTrue(isPalindrome("Was it a car or a cat I saw?"));
        Assertions.assertTrue(isPalindrome("Murder for a jar of red rum"));
        Assertions.assertTrue(isPalindrome("Go hang a salami, I'm a lasagna hog"));
        Assertions.assertTrue(isPalindrome("Eva, can I see bees in a cave?"));
    }

    @Test
    public void testIsNotPalindrome() {
        Assertions.assertFalse(isPalindrome("race a car"));
    }

    @Test
    public void testDummy() {

    }

    public static void main(String[] args) {
        System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
    }

}
