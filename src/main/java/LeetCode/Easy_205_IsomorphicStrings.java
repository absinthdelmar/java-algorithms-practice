package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 205. Isomorphic Strings
 * Easy
 *
 * Given two strings s and t, determine if they are isomorphic.
 *
 * Two strings are isomorphic if the characters in s can be replaced to get t.
 *
 * All occurrences of a character must be replaced with another character while preserving the order of characters.
 * No two characters may map to the same character but a character may map to itself.
 *
 * Example 1:
 *
 * Input: s = "egg", t = "add"
 * Output: true
 * Example 2:
 *
 * Input: s = "foo", t = "bar"
 * Output: false
 * Example 3:
 *
 * Input: s = "paper", t = "title"
 * Output: true
 *
 * Note:
 *
 * You may assume both s and t have the same length.
 */
public class Easy_205_IsomorphicStrings {

    static char[] replaceAll(char[] chars, char a, char b) {
        char[] result = Arrays.copyOf(chars, chars.length);
        for (int i = 0; i < result.length; i++) {
            if ( result[i] == a )
                result[i] = b;
        }
        return result;
    }

    static boolean isIsomorphic0(String s, String t) {
        char[] cs = s.toCharArray();
        char[] ct = t.toCharArray();

        int matches = 0;
        int i = 0, j = 0;

        while (i < cs.length) {
            // try replacement
            char[] test = replaceAll(cs, cs[i], ct[j]);
            // if replacement current char matches replacement char
            if ( test[matches] == ct[j] ) {
                System.out.println(test);
                matches++;
                j++;
                if ( matches == ct.length )
                    return true;
            }
            else {
                i++;
            }
        }

        return false;
    }

    static boolean check(Map<Character,Character> map, String s, String t) {
        for (int i = 0; i < s.length(); i++) {
            System.out.println(map);
            if ( !map.containsKey(s.charAt(i)) ) {
                map.put(s.charAt(i), t.charAt(i));
            }
            else if ( map.get(s.charAt(i)) != map.get(t.charAt(i)) ) {
                return false;
            }
        }
        return true;
    }

    static boolean isIsomorphic2(String s, String t) {
        Map<Character,Character> map = new HashMap<>();
        return check(map, s, t) && check(map, t, s);
    }

    static boolean isIsomorphic3(String s, String t) {
        char[] map = new char[127];
        Set<Character> set = new HashSet<>();

        for(int i = 0; i < s.length(); i++){
            System.out.println(set);
            System.out.println(Arrays.toString(map));
            if (map[s.charAt(i)] == 0){

                if ( set.contains(t.charAt(i)) )
                    return false;

                map[s.charAt(i)] = t.charAt(i);
                set.add(t.charAt(i));
            }
            else if (map[s.charAt(i)] != t.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    // slow
    static boolean isIsomorphic4(String s, String t) {
        Map<Character, Character> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            System.out.println("map: " + map);
            if ( map.get(s.charAt(i)) == null ) {
                if ( map.containsValue(t.charAt(i)) ) {
                    System.out.println("tcharati: " + map.get(t.charAt(i)));
                    return false;
                }
                map.put(s.charAt(i), t.charAt(i));
            }
            else if ( map.get(s.charAt(i)) != t.charAt(i) ) {
                return false;
            }
        }

        return true;
    }

    // 3 ms using mutual mappings (char arrays)
    static boolean isIsomorphic(String s, String t) {
        char[] smap = new char[127];
        char[] tmap = new char[127];

        for ( int i=0; i < s.length(); i++ ){
            System.out.println("smap: " + Arrays.toString(smap));
            System.out.println("tmap: " + Arrays.toString(tmap));
            char sc = s.charAt(i);
            char tc = t.charAt(i);
            // if both characters do not exist in mapping, add them
            if ( smap[sc] == 0 && tmap[tc] == 0 ){
                smap[sc] = tc;
                tmap[tc] = sc;
            }
            // either of the mappings are different mutually, not isomorphic then
            else if( smap[sc] != tc || tmap[tc] != sc ){
                return false;
            }
        }

        return true;
    }

    @Test
    public void testIsIsomorphic() {
        Assertions.assertTrue(isIsomorphic("egg", "add"));
    }

    @Test
    public void testIsIsomorphic2() {
        Assertions.assertFalse(isIsomorphic("foo", "bar"));
    }

    @Test
    public void testIsIsomorphic3() {
        Assertions.assertTrue(isIsomorphic("paper", "title"));
    }

    @Test
    public void testIsIsomorphic4() {
        Assertions.assertTrue(isIsomorphic("13", "42"));
    }

    public static void main(String[] args) {
//        System.out.println(isIsomorphic("egg", "add")); // e -> a, g -> d
//        System.out.println(isIsomorphic("foo", "bar"));
        System.out.println(isIsomorphic("paper", "title"));
    }

}
