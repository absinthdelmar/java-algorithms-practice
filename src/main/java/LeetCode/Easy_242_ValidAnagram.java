package LeetCode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 242. Valid Anagram
 * Easy
 *
 * Given two strings s and t , write a function to determine if t is an anagram of s.
 *
 * Example 1:
 *
 * Input: s = "anagram", t = "nagaram"
 * Output: true
 * Example 2:
 *
 * Input: s = "rat", t = "car"
 * Output: false
 * Note:
 * You may assume the string contains only lowercase alphabets.
 *
 * Follow up:
 * What if the inputs contain unicode characters? How would you adapt your solution to such case?
 */
public class Easy_242_ValidAnagram {

    static void swap(char[] chars, int i, int j) {
        char temp = chars[i];
        chars[i] = chars[j];
        chars[j] = temp;
    }

    static void permutate(String check, char[] chars, int start, int end) throws Exception {
        if ( start < end - 1 ) {
            for (int i = start; i < end; i++) {
                swap(chars, start, i);
                permutate(check, chars, start + 1, end);
                swap(chars, start, i);
            }
        }
        else {
            String actual = String.valueOf(chars);
//            System.out.println(actual);
            if ( actual.equals(check) )
                throw new Exception("valid anagram: " + check);
        }
    }

    // way too slow - time limit exceeded
    // tests include very long strings
    // so full permutation checking is unacceptable
    static boolean isAnagram0(String s, String t) {
        if ( s.length() != t.length() )
            return false;
        try {
            permutate(t, s.toCharArray(), 0, s.length());
        }
        catch ( Exception e ) {
            System.out.println(e.getMessage());
            return true;
        }
        return false;
    }

    // we just need to make sure that:
    // 1) strings are the same length
    // 2) characters of s are the same characters of t
    // so for example: s contains 3 letters a, then t has to also contain 3 letters a and so on
    // 7 ms
    static boolean isAnagram1(String s, String t) {
        // lengths are different - false
        if ( s.length() != t.length() )
            return false;
        // get character arrays
        char[] sc = s.toCharArray();
        char[] tc = t.toCharArray();
        // sort them
        Arrays.sort(sc);
        Arrays.sort(tc);
        System.out.println(Arrays.toString(sc));
        System.out.println(Arrays.toString(tc));
        // check if sorted arrays are exactly equal
        return Arrays.equals(sc, tc);
    }

    // use hashmap, 18 ms, very slow
    static boolean isAnagram2(String s, String t) {
        if ( s.length() != t.length() )
            return false;
        Map<Character, Integer> sm = new HashMap<>(26);
        Map<Character, Integer> tm = new HashMap<>(26);
        for (int i = 0; i < s.length(); i++) {
            char sc = s.charAt(i);
            char tc = t.charAt(i);
            Integer c1 = sm.getOrDefault(sc, 0);
            Integer c2 = tm.getOrDefault(tc, 0);
            sm.put(sc, c1 + 1);
            tm.put(tc, c2 + 1);
        }
        System.out.println(sm);
        System.out.println(tm);
        return sm.equals(tm);
    }

    // using counters array of size 26 (alphabet)
    // 3 ms
    static boolean isAnagram3(String s, String t) {
        if (s.length() != t.length())
            return false;
        int[] count = new int[26];
        for (int i = 0; i < s.length(); i++)
            count[s.charAt(i)-97]++;
        for (int i = 0; i < t.length(); i++)
            count[t.charAt(i)-97]--;
        for (int i = 0; i < count.length; i++)
            if ( count[i] != 0 )
                return false;
        return true;
    }

    // same but using iterators
    // 2 ms
    static boolean isAnagram(String s, String t) {
        if (s.length() != t.length())
            return false;
        int[] count = new int[26];
        for (char c: s.toCharArray())
            count[c-97]++;
        for (char c: t.toCharArray())
            count[c-97]--;
        for (int i: count)
            if ( i != 0 )
                return false;
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isAnagram("anagram", "nagaram"));
        System.out.println(isAnagram("anagran", "nagaram"));
        System.out.println(isAnagram("aaaaaabbbbbb", "aabb"));
    }

}
