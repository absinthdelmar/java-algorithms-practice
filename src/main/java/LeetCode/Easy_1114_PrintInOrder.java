package LeetCode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.StampedLock;

/**
 * 1114. Print in Order
 * Easy
 *
 * Suppose we have a class:
 *
 * public class Foo {
 *   public void first() { print("first"); }
 *   public void second() { print("second"); }
 *   public void third() { print("third"); }
 * }
 * The same instance of Foo will be passed to three different threads.
 * Thread A will call first(), thread B will call second(), and thread C will call third().
 * Design a mechanism and modify the program to ensure that second() is executed after first(), and third() is executed after second().
 *
 *
 *
 * Example 1:
 *
 * Input: [1,2,3]
 * Output: "firstsecondthird"
 * Explanation: There are three threads being fired asynchronously.
 * The input [1,2,3] means thread A calls first(), thread B calls second(), and thread C calls third().
 * "firstsecondthird" is the correct output.
 *
 * Example 2:
 *
 * Input: [1,3,2]
 * Output: "firstsecondthird"
 * Explanation: The input [1,3,2] means thread A calls first(), thread B calls third(), and thread C calls second().
 * "firstsecondthird" is the correct output.
 *
 *
 * Note:
 *
 * We do not know how the threads will be scheduled in the operating system,
 * even though the numbers in the input seems to imply the ordering.
 * The input format you see is mainly to ensure our tests' comprehensiveness.
 *
 */
public class Easy_1114_PrintInOrder {

    // we can only modify Foo class

    static class Foo {

        Semaphore s1;
        Semaphore s2;
        Semaphore s3;

        public Foo() {
            s1 = new Semaphore(1);
            s2 = new Semaphore(0);
            s3 = new Semaphore(0);
        }

        public void first(Runnable printFirst) throws InterruptedException {
            s1.acquire();
            // printFirst.run() outputs "first". Do not change or remove this line.
            printFirst.run();
            s2.release();
        }

        public void second(Runnable printSecond) throws InterruptedException {
            s2.acquire();
            // printSecond.run() outputs "second". Do not change or remove this line.
            printSecond.run();
            s3.release();
        }

        public void third(Runnable printThird) throws InterruptedException {
            s3.acquire();
            // printThird.run() outputs "third". Do not change or remove this line.
            printThird.run();
            s1.release();
        }
    }

    // helper code below this line

    static class T implements Runnable {
        Foo foo;
        String data;

        T(Foo foo, String data) {
            this.foo = foo;
            this.data = data;
        }

        public void run() {
            try {
                Method method = foo.getClass().getMethod(data, Runnable.class);
                method.invoke(foo, (Runnable)() -> System.out.println(data));
            }
            catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Foo foo = new Foo();
//        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ExecutorService executorService = Executors.newWorkStealingPool();
        executorService.submit(new T(foo,"first"));
        executorService.submit(new T(foo, "second"));
        executorService.submit(new T(foo, "third"));
        executorService.shutdown();
        if ( !executorService.awaitTermination(5, TimeUnit.SECONDS ) ) {
            System.out.println("Threads didn't finish in 2 seconds!");
        }
    }

}
