package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * 1013. Partition Array Into Three Parts With Equal Sum
 * Easy
 *
 * Given an array A of integers, return true if and only
 * if we can partition the array into three non-empty parts with equal sums.
 *
 * Formally, we can partition the array if we can find indexes i+1 < j with
 * (A[0] + A[1] + ... + A[i] == A[i+1] + A[i+2] + ... + A[j-1] == A[j] + A[j-1] + ... + A[A.length - 1])
 *
 * Example 1:
 *
 * Input: A = [0,2,1,-6,6,-7,9,1,2,0,1]
 * Output: true
 * Explanation: 0 + 2 + 1 = -6 + 6 - 7 + 9 + 1 = 2 + 0 + 1
 * Example 2:
 *
 * Input: A = [0,2,1,-6,6,7,9,-1,2,0,1]
 * Output: false
 * Example 3:
 *
 * Input: A = [3,3,6,5,-2,2,5,1,-9,4]
 * Output: true
 * Explanation: 3 + 3 = 6 = 5 - 2 + 2 + 5 + 1 - 9 + 4
 *
 *
 * Constraints:
 *
 * 3 <= A.length <= 50000
 * -10^4 <= A[i] <= 10^4
 *
 */
public class Easy_1013_PartitionArrayThreePartsEqualSum {

    static long sum(int[] a, int start, int end) {
//        System.out.println(start + " vs " + end);
//        if ( start > end )
//            return -1;
        // assume sum is 0
//        if (start > end)
//            return 0;
        System.out.println("start: " + start + " end: " + end);
        long sum = 0;
        // start inclusive, end exclusive
        for (int i = start; i < end; i++) {
            sum += (long) a[i];
        }
        System.out.println(Arrays.toString(Arrays.copyOfRange(a, start, end)) + " -> sum = " + sum);
        return sum;
    }

    //        [0, b1), [b1, b2), [b2, n)
    //        ( b1 < b2 )
    //        ( b2 < n )
    //        long s1 = sum(a, 0, b1);
    //        long s2 = sum(a, b1, b2);
    //        long s3 = sum(a, b2, n);
    //        s1 == s2 == s3;
    // 1,2,3,4 into 3 consecutive parts
    // [1] [2] [34]
    // [1] [23] [4]
    // [12] [3] [4]

    static boolean checkSum(int[] a, int b1, int b2) {
        long s1 = sum(a, 0, b1);
        long s2 = sum(a, b1, b2);
        long s3 = sum(a, b2, a.length);
        return s1 == s2 && s2 == s3;
    }

    static class Solution {
        int[] array;
        int arrayLen;
        int[] arrayLeft;
        int arrayLeftLen;
        int[] arrayRight;
        int arrayRightLen;
        long sum;
        long sumLeft;
        long sumRight;
        int start;
        int end;

        public boolean check(int n) {
//            return array.length > 0 && array.length < n - 1 &&
//                    arrayLeft.length > 0 && arrayRight.length > 0 &&
//                    sumLeft == sum && sumRight == sum;
            return arrayLen > 0 && arrayLen < n - 1 &&
                    arrayLeftLen > 0 && arrayRightLen > 0 &&
                    sumLeft == sum && sumRight == sum;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                    .append("sum = " )
                    .append(sum)
                    .append(" -> " )
//                    .append(Arrays.toString(array))
                    .append(arrayLen)
                    .append(" L: " )
//                    .append(Arrays.toString(arrayLeft))
                    .append(arrayLeftLen)
                    .append(" R: " )
//                    .append(Arrays.toString(arrayRight))
                    .append(arrayRightLen)
//                    .append(" [ ")
//                    .append(start)
//                    .append(", ")
//                    .append(end)
//                    .append(" ]")
                    .toString();
        }
    }

    // very very very SLOW!
    static boolean solutionThreeParts0(int[] a) {
        if (a == null || a.length < 3)
            return false;

        int n = a.length;

        Map<Long, List<Solution>> solutions = new HashMap<>();

        // all sub arrays
        for (int i = 0; i < n; i++) {
            for (int j = i; j <= n; j++) {
                Solution solution = new Solution();
//                solution.array = Arrays.copyOfRange(a, i, j);
                solution.arrayLen = j - i;
                solution.start = i;
                solution.end = j;
//                solution.arrayLeft = Arrays.copyOfRange(a, 0, i);
                solution.arrayLeftLen = i;
//                solution.arrayRight = Arrays.copyOfRange(a, j, n);
                solution.arrayRightLen = n - j;
                solution.sum = sum(a, i, j);
                solution.sumLeft = sum(a, 0, i);
                solution.sumRight = sum(a, j, n);

//                if ( solution.array.length > 0 && solution.array.length < n - 1 &&
//                     solution.arrayLeft.length > 0 && solution.arrayRight.length > 0 &&
//                     solution.sumLeft == solution.sum && solution.sumRight == solution.sum ) {
                if ( solution.check( n ) ) {
                    if ( !solutions.containsKey(solution.sum) )
                        solutions.put(solution.sum, new ArrayList<>());
                    solutions.get(solution.sum).add(solution);
                }
            }
        }

        for (Long sum: solutions.keySet()) {
            for ( Solution solution: solutions.get(sum) )
                System.out.println(solution);
        }

//        System.out.println("solutions: " + solutions);

        return !solutions.isEmpty();
    }

    // very different approach using total sum, sum / 3 and sum / 3 * 2 assumption
    // does not work for corner case [1, -1, 1, -1] !!!
    static boolean solutionThreeParts1(int[] a) {
        if (a == null || a.length < 3)
            return false;

        int n = a.length;

        // sum of all elements
        long total = sum(a, 0, n);

        System.out.println(total);

        // if sum of all elements is not divisible by 3 then solution is impossible
        // as there cannot be 3 sub arrays with equal sum
        if ( total % 3 != 0 )
            return false;

        // sum of one part
        long s1 = total / 3;

        // sum of two parts
        long s2 = s1 * 2;

        // final indices
        int b1 = -1, b2 = -1;

        long prefixSum = 0;

        for (int i = 0; i < n; i++) {
            prefixSum += a[i];

            if ( prefixSum == s1 && b1 == -1 ) {
                b1 = i;
            }
            else if ( prefixSum == s2 && b1 != -1 ) {
                b2 = i;
                break;
            }
        }

        if ( b1 != -1 && b2 != -1 ) {
            long check1 = sum(a, 0, b1);
            long check2 = sum(a, b1, b2);
            long check3 = sum(a, b2, n);
            System.out.println("b1 " + b1);
            System.out.println("b2 " + b2);
            System.out.println("sum1 " + check1);
            System.out.println("sum2 " + check2);
            System.out.println("sum3 " + check3);
            return check1 == check2 && check2 == check3;
        }

        return false;
    }

    // blazing fast
    // checks how many times (totalSum / 3) parts we could have while traversing array
    // does work for all test cases even [ 1, -1, 1, -1 ]
    static boolean solutionThreeParts(int[] a) {
        if (a == null || a.length < 3)
            return false;

        // use long for sums instead of int to prevent potential overflow
        long total = 0;

        for (int i = 0; i < a.length; i++)
            total += a[i];

        // per part sum, assuming we must have 3 parts
        long sub = total / 3;
        long sum = 0;

        int count = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
            // got it, increase count, reset
            if ( sum == sub ) {
                sum = 0;
                count++;
            }
        }

        System.out.println(count);

        // we must have 3 parts of the same sum, check we had reset also (sum==0)
        return count >= 3 && sum == 0;
    }

    @Test
    public void testSolution() {
        Assertions.assertTrue(solutionThreeParts(new int[] { 0,2,1, -6,6,-7,9,1,  2,0,1 }));
    }

    @Test
    public void testSolution2() {
        Assertions.assertTrue(solutionThreeParts(new int[] { 1, 2, 3, 3 }));
    }

    @Test
    public void testSolution3() {
        Assertions.assertTrue(solutionThreeParts(new int[] { 1, 3, 4, 0, 4 }));
    }

    @Test
    public void testSolution4() {
        Assertions.assertFalse(solutionThreeParts(new int[] { 1, -1, 1, -1 }));
    }

    @Test
    public void testSolution5() {
        Assertions.assertTrue(solutionThreeParts(new int[] { 10,-10,10,-10,10,-10,10,-10 }));
    }

    public static void main(String[] args) {
//        int[] a = new int[] { 0,2,1, -6,6,-7,9,1,  2,0,1 };      // true
//        int[] a = new int[] { 1, 2, 3, 3 };     // true
//        int[] a = new int[] { 1, 3, 4, 0, 4 };     // true
        int[] a = new int[] { 1, -1, 1, -1 };     // false    (corner case)
//        int[] a = new int[] { 10,-10,10,-10,10,-10,10,-10 };        // true     (corner case)
        System.out.println(solutionThreeParts(a));
    }
}
