package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 101. Symmetric Tree
 * Easy
 *
 * Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 *
 * For example, this binary tree [1,2,2,3,4,4,3] is symmetric:
 *
 *     1
 *    / \
 *   2   2
 *  / \ / \
 * 3  4 4  3
 *
 *
 * But the following [1,2,2,null,3,null,3] is not:
 *
 *     1
 *    / \
 *   2   2
 *    \   \
 *    3    3
 *
 *
 * Follow up: Solve it both recursively and iteratively.
 *
 */
public class Easy_101_SymmetricTree {

    static boolean isSymmetric(TreeNode a, TreeNode b) {
        // both trees are empty
        if ( a == null && b == null )
            return true;

        if ( a == null || b == null || a.val != b.val )
            return false;

        // both trees are non empty
        // left subtree is a mirror of right subtree
        // right subtree is a mirror of left subtree
        return isSymmetric(a.left, b.right) &&
               isSymmetric(a.right, b.left);
    }

    static boolean isSymmetric(TreeNode root) {
        if ( root == null )
            return true;

        // left and right subtree are mirrors of each other
        return isSymmetric(root.left, root.right);
    }

    // [1,2,2,3,4,4,3]
    @Test
    public void testIsSymmetric() {
        TreeNode tree = new TreeNode(
                1,
                new TreeNode(2, new TreeNode(3), new TreeNode(4)),
                new TreeNode(2, new TreeNode(4), new TreeNode(3))
        );
        Assertions.assertTrue(isSymmetric(tree));
    }

    // []
    @Test
    public void testIsSymmetric2() {
        TreeNode tree = null;
        Assertions.assertTrue(isSymmetric(tree));
    }

    // [1,2,2,null,3,3]
    @Test
    public void testIsSymmetric3() {
        TreeNode tree = new TreeNode(1,
                new TreeNode(2, null, new TreeNode(3)),
                new TreeNode(2, new TreeNode(3), null)
        );
        System.out.println(tree);
        Assertions.assertTrue(isSymmetric(tree));
    }

    // [1,2,2,3,null,null,3]
    @Test
    public void testIsSymmetric4() {
        TreeNode tree = new TreeNode(1,
                new TreeNode(2, new TreeNode(3), null),
                new TreeNode(2, null, new TreeNode(3))
        );
        System.out.println(tree);
        Assertions.assertTrue(isSymmetric(tree));
    }

    // [1,2,2,3,4,4,3,5,6,6,5,5,6,6,5]
    @Test
    public void testIsSymmetric5() {
        TreeNode tree = new TreeNode(1,
                new TreeNode(2, new TreeNode(3, new TreeNode(5), new TreeNode(6)), new TreeNode(4, new TreeNode(6), new TreeNode(5))),
                new TreeNode(2, new TreeNode(4, new TreeNode(5), new TreeNode(6)), new TreeNode(3, new TreeNode(6), new TreeNode(5)))
        );
        System.out.println(tree);
        Assertions.assertTrue(isSymmetric(tree));
    }

    // [1,2,2,3,4,3,4]
    @Test
    public void testNotSymmetric() {
        TreeNode tree = new TreeNode(
                1,
                new TreeNode(2, new TreeNode(3), new TreeNode(4)),
                new TreeNode(2, new TreeNode(3), new TreeNode(4))
        );
        Assertions.assertFalse(isSymmetric(tree));
    }

    // [1,2,2,null,3,null,3]
    @Test
    public void testNotSymmetric2() {
        TreeNode tree = new TreeNode(
                1,
                new TreeNode(2, null, new TreeNode(3)),
                new TreeNode(2, null, new TreeNode(3))
        );
        Assertions.assertFalse(isSymmetric(tree));
    }

    // [2,3,3,4,5,5]
    @Test
    public void testNotSymmetric3() {
        TreeNode tree = new TreeNode(
                2,
                new TreeNode(3, new TreeNode(4), new TreeNode(5)),
                new TreeNode(3, new TreeNode(4), null)
        );
        Assertions.assertFalse(isSymmetric(tree));
    }

    // [2,3,3,4,5,5,4,null,null,8,9,null,null,9,8]
    @Test
    public void testNotSymmetric4() {
        TreeNode tree = new TreeNode(
                2,
                new TreeNode(3, new TreeNode(4), new TreeNode(5, new TreeNode(8), new TreeNode(9))),
                new TreeNode(3, new TreeNode(5), new TreeNode(4, new TreeNode(9), new TreeNode(8)))
        );
        Assertions.assertFalse(isSymmetric(tree));
    }

    public static void main(String[] args) {
        TreeNode treeNode1 = new TreeNode(
            1,
            new TreeNode(2, new TreeNode(3), new TreeNode(4)),
            new TreeNode(2, new TreeNode(4), new TreeNode(3))
        );
        System.out.println(isSymmetric(treeNode1));
    }

}
