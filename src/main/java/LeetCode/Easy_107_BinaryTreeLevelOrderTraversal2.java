package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 107. Binary Tree Level Order Traversal II
 * Easy
 *
 * Given a binary tree, return the bottom-up level order traversal of its nodes' values.
 * (ie, from left to right, level by level from leaf to root).
 *
 * For example:
 * Given binary tree [3,9,20,null,null,15,7],
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * return its bottom-up level order traversal as:
 * [
 *   [15,7],
 *   [9,20],
 *   [3]
 * ]
 *
 */
public class Easy_107_BinaryTreeLevelOrderTraversal2 {

    // calculate tree height
    static int height(TreeNode root) {
        // no tree - no height
        if ( root == null ) {
            return 0;
        }
        else {
            // calculate max height along the left and the right edges
            int lh = height(root.left);
            int rh = height(root.right);
            return lh > rh ? lh + 1 : rh + 1;
        }
    }

    // return a list of values at given level
    static List<Integer> atGivenLevel(TreeNode root, int level) {
        assert( level > 0 );
        // no tree - empty list
        if ( root == null ) {
            return Collections.emptyList();
        }
        // any other level - make a list consisting of all left and right branches
        if ( level > 1 ) {
            List<Integer> list = new ArrayList<>();
            list.addAll(atGivenLevel(root.left, level - 1));
            list.addAll(atGivenLevel(root.right, level - 1));
            return list;
        }
        // level = 1, just root value in a list
        else {
            System.out.println(root.val);
            return Collections.singletonList(root.val);
        }
    }

    // it is an implementation of Breadth-First-Search ( BFS )
    // my version is 18 ms - very slow
    static List<List<Integer>> levelOrderBottom0(TreeNode root) {
        LinkedList<List<Integer>> list = new LinkedList<>();
        // calculate height
        int h = height(root);
        // iterate through all levels up to specific height
        // add every result as a first to the list
        for (int i = 1; i <= h; i++) {
            System.out.println("level: " + i);
            list.addFirst(atGivenLevel(root, i));
        }
        return list;
    }

    // fast 0 ms version from the website using depth

    // result list represents a list of node/vertex values per depth
    static List<List<Integer>> result;
    static int depth;

    // determine max depth for current node and current depth
    static void calculateMaxDepth(TreeNode root, int currentDepth) {
        if ( root == null ) {
            depth = Math.max(currentDepth, depth);
            return;
        }
        // proceed to left and right nodes/vertices also with the next depth
        calculateMaxDepth(root.left, currentDepth + 1);
        calculateMaxDepth(root.right, currentDepth + 1);
    }

    // traverse through the tree starting from current node and depth
    static void traverse(TreeNode root, int currentDepth) {
        // stop on leaf or empty tree
        if ( root == null )
            return;

        // traverse to left and right nodes/vertices also with the next depth
        traverse(root.left, currentDepth + 1);
        traverse(root.right, currentDepth + 1);

        // if node exists, add its value at current depth
        if ( root.left != null )
            result.get( depth - ( currentDepth + 1 ) ).add( root.left.val );
        if ( root.right != null )
            result.get( depth - ( currentDepth + 1 ) ).add( root.right.val );
    }

    // level order (per level) search or breadth first search (BFS)
    static List<List<Integer>> levelOrderBottom1(TreeNode root) {
        result = new ArrayList<>();
        // we have empty tree - return empty result list
        if ( root == null )
            return result;

        depth = 0;

        // obtain a maximum depth for current tree
        calculateMaxDepth(root, -1);

        // initialize lists for each depth available
        for (int i = 0; i <= depth ; i++)
            result.add(new ArrayList<>());

        // traverse the tree to populate lists per depth, starting from depth = 0
        traverse(root, 0);

        // add root value last at maximum depth
        result.get(depth).add(root.val);

        return result;
    }

    // 1 ms version from the website using Queue
    static List<List<Integer>> levelOrderBottom(TreeNode root) {
        LinkedList<List<Integer>> result = new LinkedList<>();
        if ( root == null )
            return result;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while ( !queue.isEmpty() ) {
            List<Integer> list = new ArrayList<>();
            // important! save size because it changes as we poll elements
            // because we will need pull specific number of elements per level
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                if ( node != null ) {
                    list.add(node.val);
                    if (node.left != null)
                        queue.add(node.left);
                    if (node.right != null)
                        queue.add(node.right);
                }
            }
            result.addFirst(list);
        }

        System.out.println(result);

        return result;
    }

    // input [3,9,20,null,null,15,7]
    // output expected [
    //  [15,7],
    //  [9,20],
    //  [3]
    // ]
    @Test
    public void testBottomUpTraversal() {
        TreeNode tree = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        List<List<Integer>> treeList = Arrays.asList(
            Arrays.asList(15, 7),
            Arrays.asList(9, 20),
            Arrays.asList(3)
        );
        Assertions.assertArrayEquals(treeList.toArray(), levelOrderBottom(tree).toArray());
    }

    // input [1,2]
    // output expected [
    //  [2],
    //  [1]
    // ]
    @Test
    public void testBottomUpTraversal2() {
        TreeNode tree = new TreeNode(1, new TreeNode(2), null);
        List<List<Integer>> treeList = Arrays.asList(
            Arrays.asList(2),
            Arrays.asList(1)
        );
        Assertions.assertArrayEquals(treeList.toArray(), levelOrderBottom(tree).toArray());
    }

    // input [1,2,3,4,null,null,5]
    // output expected [
    //  [4,5],
    //  [2,3],
    //  [1]
    // ]
    @Test
    public void testBottomUpTraversal3() {
        TreeNode tree = new TreeNode(1,
                new TreeNode(2, new TreeNode(4), null),
                new TreeNode(3, null, new TreeNode(5)));
        List<List<Integer>> treeList = Arrays.asList(
            Arrays.asList(4, 5),
            Arrays.asList(2, 3),
            Arrays.asList(1)
        );
        Assertions.assertArrayEquals(treeList.toArray(), levelOrderBottom(tree).toArray());
    }

    public static void main(String[] args) {
        TreeNode tree = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
//        System.out.println(levelOrderBottom(tree));
//        TreeNode tree = new TreeNode(1,
//                new TreeNode(2, new TreeNode(4), null),
//                new TreeNode(3, null, new TreeNode(5)));
        System.out.println(TreeNode.isBalanced(tree));
        System.out.println(levelOrderBottom(tree));
    }

}
