package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class Easy_69_Sqrt {

    static boolean isPrime( int x ) {
        if ( x <= 3 )
            return x > 1;
        else if ( x % 2 == 0 || x % 3 == 0 )
            return false;
        int i = 5;
        while ( i * i <= x ) {
            if ( x % i == 0 || x % ( i + 2 ) == 0 )
                return false;
            i += 6;
        }
        return true;
    }

    static List<Integer> numberToDigits( int x ) {
        List<Integer> result = new ArrayList<>();
        int temp = x;
        do {
            result.add(temp % 10);
            temp /= 10;
        } while ( temp > 0 );
        return result;
    }

    static int sumDigits(List<Integer> digits) {
        return digits.stream().reduce(0, Integer::sum);
    }

    static long multiply(List<Integer> mults) {
        return mults.stream().reduce(1, (a, b) -> a * b);
    }

    static int chooseDivider(int x) {
        System.out.println("choose: " + x);
        if (isPrime(x))
            return x;
//        else if (x % 7 == 0)
//            return 7;
//        else if (x % 5 == 0)
//            return 5;
//        else if (x % 3 == 0)
//            return 3;
        else if (x % 2 == 0)
            return 2;
        return chooseDivider(sumDigits(numberToDigits(x)));
    }

    static int div(List<Integer> mults, int x) {
        if ( x > 1 ) {
            int divider = chooseDivider(x);
            mults.add(divider);
            return div(mults, x / divider);
        }
        System.out.println(" x = " + x );
        return x;
    }

    static int mySqrt0(int x) {
        List<Integer> mults = new ArrayList<>();
        System.out.println(div(mults, x));
        System.out.println(mults);
        System.out.println(multiply(mults));



        return 0;
    }

    // bit shift approximation version 1ms
    // uses long to not overflow
    static int mySqrt1( int x ) {
        long test = 0;
        long bit = 1 << 15;      // 2^15
        do {
            test ^= bit;
            if ( test * test > x ) {
                test ^= bit;
            }
        } while ( ( bit >>= 1 ) != 0 );
        return (int)test;
    }

    // division by 2 version 1ms
    static int mySqrt2( int x ) {
        if ( x == 0 )
            return 0;
        int start = 1;
        int end = x;
        while ( start < end ) {
            int mid = start + ( end - start ) / 2;
            System.out.println("mid: " + mid);
            // return mid value if:
            // 1) mid value is less or equal x divided by mid value
            // 2) mid+1 value is greater than x / (mid+1)
            if ( mid <= x / mid && ( mid + 1 ) > x / ( mid + 1 ) ) {
                return mid;
            }
            // cut range, set end to mid on the next iteration
            else if ( mid > x / mid ) {
                System.out.println(mid + " > " + (x/mid) + ", end = " + mid);
                end = mid;
            }
            // cut range, start from mid+1 on the next iteration
            else {
                start = mid + 1;
            }
        }
        return start;
    }

    // another bitshift approximation version 1 ms
    // uses long to not overflow
    static int mySqrt( int x ) {
        if ( x < 2 )
            return x;
        int small = mySqrt( x >> 2 ) << 1;
        int large = small + 1;
        if ( (long)large * (long)large > x )
            return small;
        else
            return large;
    }

    @Test
    public void testDummy() {

    }

    @Test
    public void testMySqrt1() {
        Assertions.assertEquals(3, mySqrt1(9));
        Assertions.assertEquals(2, mySqrt1(4));
        Assertions.assertEquals(4, mySqrt1(16));
        Assertions.assertEquals(32, mySqrt1(1024));
        Assertions.assertEquals(105, mySqrt1(11025));
        Assertions.assertEquals(46340, mySqrt1(2147395600));
    }

    @Test
    public void testMySqrt2() {
        Assertions.assertEquals(3, mySqrt2(9));
        Assertions.assertEquals(2, mySqrt2(4));
        Assertions.assertEquals(4, mySqrt2(16));
        Assertions.assertEquals(32, mySqrt2(1024));
        Assertions.assertEquals(105, mySqrt2(11025));
        Assertions.assertEquals(46340, mySqrt2(2147395600));
    }

    @Test
    public void testMySqrt() {
        Assertions.assertEquals(3, mySqrt(9));
        Assertions.assertEquals(2, mySqrt(4));
        Assertions.assertEquals(4, mySqrt(16));
        Assertions.assertEquals(32, mySqrt(1024));
        Assertions.assertEquals(105, mySqrt(11025));
        Assertions.assertEquals(46340, mySqrt(2147395600));
    }

    public static void main(String[] args) {
//        System.out.println(mySqrt2(2147395600));        // 46340
        System.out.println(mySqrt(2147395600));         // 46340
//        System.out.println(mySqrt(0,46340));         // 215
//        System.out.println(mySqrt(0,215));         // 14
//        System.out.println(mySqrt(4));
//        System.out.println(mySqrt(8));
//        System.out.println(mySqrt(11025));
//        System.out.println(isPrime(37));
//        System.out.println(isPrime(149));
//        System.out.println(isPrime(7));
//        System.out.println(isPrime(13));
//        System.out.println(isPrime(29));
//        System.out.println(isPrime(41));
//        System.out.println(numberToDigits(11026));
//        System.out.println(sumDigits(numberToDigits(11026)));
//        System.out.println(sqrt(11025));
//        List<Integer> mults = new ArrayList<>();
//        System.out.println(div(mults, 11025));
//        System.out.println(mults);
    }

}
