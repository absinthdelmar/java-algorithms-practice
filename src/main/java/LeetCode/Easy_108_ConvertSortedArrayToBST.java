package LeetCode;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * 108. Convert Sorted Array to Binary Search Tree
 * Easy
 *
 * Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
 *
 * For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the
 * two subtrees of every node never differ by more than 1.
 *
 * Example:
 *
 * Given the sorted array: [-10,-3,0,5,9],
 *
 * One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:
 *
 *       0
 *      / \
 *    -3   9
 *    /   /
 *  -10  5
 *
 */
public class Easy_108_ConvertSortedArrayToBST {

    // bst - left is less than root, right is greater than root
    //       0
    //     /   \
    //    -3    5
    //   /       \
    // -10        9
    // height balanced binary search tree

    static TreeNode sortedArrayToBST0(int[] nums) {
        TreeNode result = new TreeNode(0,
                new TreeNode(-3, new TreeNode(-10), null),
                new TreeNode(9, new TreeNode(5), null));
        System.out.println("height: " + TreeNode.height(result));
//        TreeNode result = new TreeNode(1,
//                new TreeNode(2, new TreeNode(3, new TreeNode(5), new TreeNode(6)), new TreeNode(4, new TreeNode(6), new TreeNode(5))),
//                new TreeNode(2, new TreeNode(4, new TreeNode(5), new TreeNode(6)), new TreeNode(3, new TreeNode(6), new TreeNode(5)))
//        );
        List<Integer> list = new ArrayList<>();
        List<Integer> list1 = new ArrayList<>();
        TreeNode.dfs(result, list);
        TreeNode.bfs(result, list1);
        System.out.println(list);
        System.out.println(list1);
        return result;
    }

    // raw version with lists, very slow 31 ms
    static TreeNode sortedArrayToBST1(int[] nums) {
        if ( nums == null || nums.length == 0 )
            return null;

        int m = nums.length / 2;
        int mid = nums[m];

        System.out.println("mid: " + mid + " nums: " + Arrays.toString(nums));

        List<Integer> left = new ArrayList<>();
        List<Integer> right = new ArrayList<>();

        for (int i = 0; i < m; i++)
            left.add(nums[i]);

        for (int i = m + 1; i < nums.length; i++)
            right.add(nums[i]);

//        System.out.println("left: " + Arrays.toString(left));
//        System.out.println("right: " + Arrays.toString(right));

        TreeNode root = new TreeNode(mid);

        // we have no way to convert List<Integer> to int[] directly to match output, so let's use streams
        root.left = sortedArrayToBST1( left.stream().mapToInt(i->i).toArray() );
        root.right = sortedArrayToBST1( right.stream().mapToInt(i->i).toArray() );

        return root;
    }

    // 1 ms version using exact sized arrays
    static TreeNode sortedArrayToBST2(int[] nums) {
        if ( nums == null || nums.length == 0 )
            return null;

        int m = nums.length / 2;
        int mid = nums[m];

//        System.out.println("mid: " + mid + " nums: " + Arrays.toString(nums));

        int[] left = new int[m];
        int[] right = new int[nums.length - m - 1];

        for (int i = 0; i < m; i++)
            left[i] = nums[i];

        for (int i = m + 1; i < nums.length; i++)
            right[i - m - 1] = nums[i];

//        System.out.println("left: " + Arrays.toString(left));
//        System.out.println("right: " + Arrays.toString(right));

        TreeNode root = new TreeNode(mid);

        root.left = sortedArrayToBST( left );
        root.right = sortedArrayToBST( right );

        return root;
    }

    // 0ms version from the website
    static TreeNode sortedArrayToBST(int[] nums) {
        if ( nums == null || nums.length == 0 )
            return null;
        return produce(nums, 0, nums.length - 1);
    }

    static TreeNode produce(int[] nums, int start, int end) {
        if ( start > end )
            return null;

        int mid = start + ( end - start ) / 2;

        TreeNode root = new TreeNode(nums[mid]);
        root.left = produce(nums, start, mid - 1);
        root.right = produce(nums, mid + 1, end);
        return root;
    }

    // input [-10,-3,0,5,9]
    // output [0,-3,9,-10,null,5]
    @Test
    public void testSortedArrayToBST() {
        int[] nums = new int[]{-10,-3,0,5,9};
        List<Integer> list = new ArrayList<>();
        TreeNode.bfs(sortedArrayToBST(nums), list);
        Assertions.assertEquals(Arrays.asList(0,-3,9,-10,null,5), list);
    }

    // input [-10,-3,0,5,9]
    // output [0,-3,9,-10,null,5]
    public static void main(String[] args) {
        int[] nums = new int[]{-10,-3,0,5,9};
        List<Integer> bfsList = new ArrayList<>();
        List<Integer> dfsList = new ArrayList<>();
        List<Integer> dfsListInorder = new ArrayList<>();
        List<Integer> dfsListPostorder = new ArrayList<>();
        TreeNode.bfs(sortedArrayToBST(nums), bfsList);
        TreeNode.dfsPreorder(sortedArrayToBST(nums), dfsList);
        TreeNode.dfsInorder(sortedArrayToBST(nums), dfsListInorder);
        TreeNode.dfsPostOrder(sortedArrayToBST(nums), dfsListPostorder);
        System.out.println(bfsList);
        System.out.println(dfsList);
        System.out.println(dfsListInorder);
        System.out.println(dfsListPostorder);
    }

}
