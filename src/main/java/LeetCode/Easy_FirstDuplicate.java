package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

// Not on LeetCode, taken from CodeSignal
public class Easy_FirstDuplicate {

    // slow using minIndex of duplicate
    // time O(n**2) space O(n)
    static int firstDuplicate0(int[] nums) {
        int minIndex = nums.length - 1;
        for (int i = 0; i < nums.length - 1; i++) {
            for (int j = 1; j < nums.length; j++) {
//                System.out.println(nums[i] + " vs " + nums[j]);
                if ( i < j && nums[i] == nums[j] ) {
                    minIndex = Math.min(minIndex, j);
//                    System.out.println("gotcha! minIndex = " + minIndex);
                }
            }
        }
        return minIndex;
    }

    // using set
    // O(n) time, O(n) space
    static int firstDuplicate1(int[] nums) {
        Set<Integer> seen = new HashSet<>();
        for (int i = 0; i < nums.length; i++)
            if ( !seen.add(nums[i]) )
                return i;
        return -1;
    }

    // no extra space, no extra loop
    // O(n) time, O(1) space
    static int firstDuplicate(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
//            System.out.println(Arrays.toString(nums));
            if ( nums[Math.abs(nums[i])-1] < 0 )
                return i;
            else
                nums[Math.abs(nums[i])-1] = -nums[Math.abs(nums[i])-1];
        }
        return -1;
    }

    @Test
    public void testFirstDuplicate() {
        int[] nums = new int[]{2,1,3,5,3,2};
        Assertions.assertEquals(4, firstDuplicate0(nums));
        Assertions.assertEquals(4, firstDuplicate1(nums));
        Assertions.assertEquals(4, firstDuplicate(nums));
    }

    // values in the array has to be equal or less than the length of the array
    public static void main(String[] args) {
        System.out.println(firstDuplicate0(new int[]{2,1,3,5,3,2}));
        System.out.println(firstDuplicate1(new int[]{2,1,3,5,3,2}));
        System.out.println(firstDuplicate(new int[]{2,1,3,5,3,2}));
    }

}
