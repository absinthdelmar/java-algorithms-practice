package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

/**
 * 226. Invert Binary Tree
 * Easy
 *
 * Invert a binary tree.
 *
 * Example:
 *
 * Input:
 *
 *      4
 *    /   \
 *   2     7
 *  / \   / \
 * 1   3 6   9
 * Output:
 *
 *      4
 *    /   \
 *   7     2
 *  / \   / \
 * 9   6 3   1
 *
 * Trivia:
 * This problem was inspired by this original tweet by Max Howell:
 *
 * Google: 90% of our engineers use the software you wrote (Homebrew),
 * but you can’t invert a binary tree on a whiteboard so f*** off.
 */
public class Easy_226_InvertBinaryTree {

    static void swap(TreeNode root) {
        if ( root == null )
            return;
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        swap(root.left);
        swap(root.right);
    }

    // better memory usage
    static TreeNode invertTree0(TreeNode root) {
        swap(root);
        return root;
    }

    // same speed
    static TreeNode invertTree(TreeNode root) {
        return root == null ? null : new TreeNode( root.val, invertTree(root.right), invertTree(root.left) );
    }

    // input:   [4,2,1,3,7,6,9]
    // output:  [4,7,9,6,2,3,1]
    @Test
    public void testInvertBinaryTree() {
        TreeNode root = new TreeNode(4,
                new TreeNode(2, new TreeNode(1), new TreeNode(3)),
                new TreeNode(7, new TreeNode(6), new TreeNode(9)));
        List<Integer> expected = Arrays.asList(4,7,9,6,2,3,1);
        List<Integer> actual = new ArrayList<>();
        TreeNode.dfs(invertTree(root), actual);
        Assertions.assertArrayEquals(expected.toArray(), actual.toArray());
    }

    // input:   [1,2]
    // output:  [1,null,2]
    @Test
    public void testInvertBinaryTree2() {
        TreeNode root = new TreeNode(1, new TreeNode(2), null);
        List<Integer> expected = Arrays.asList(1, null, 2);
        List<Integer> actual = new ArrayList<>();
        TreeNode.dfs(invertTree(root), actual);
        System.out.println(actual);
        System.out.println(root);
        Assertions.assertArrayEquals(expected.toArray(), actual.toArray());
    }

    // input:   [4,2,1,3,7,6,9]
    // output:  [4,7,9,6,2,3,1]
    public static void main(String[] args) {
//        TreeNode root = new TreeNode(4,
//                new TreeNode(2, new TreeNode(1), new TreeNode(3)),
//                new TreeNode(7, new TreeNode(6), new TreeNode(9)));
        TreeNode root = new TreeNode(1,
            new TreeNode(2, new TreeNode(3, null, new TreeNode(6)), new TreeNode(7, new TreeNode(8), new TreeNode(9))),
            new TreeNode(10, new TreeNode(11, new TreeNode(12), new TreeNode(13)), new TreeNode(14, new TreeNode(15), new TreeNode(16)))
        );
        List<Integer> list = new ArrayList<>();
        TreeNode.dfs(root, list);
        System.out.println(list);

        list = new ArrayList<>();
        TreeNode.dfs(invertTree(root), list);
        System.out.println(list);
    }

}
