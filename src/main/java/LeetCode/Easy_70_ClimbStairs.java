package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 70. Climbing Stairs
 * Easy
 *
 * You are climbing a stair case. It takes n steps to reach to the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
 *
 * Example 1:
 *
 * Input: 2
 * Output: 2
 * Explanation: There are two ways to climb to the top.
 * 1. 1 step + 1 step
 * 2. 2 steps
 * Example 2:
 *
 * Input: 3
 * Output: 3
 * Explanation: There are three ways to climb to the top.
 * 1. 1 step + 1 step + 1 step
 * 2. 1 step + 2 steps
 * 3. 2 steps + 1 step
 *
 *
 * Constraints:
 *
 * 1 <= n <= 45
 */
public class Easy_70_ClimbStairs {

    // climb stairs is a fibonacci sequence:
    // next value in sequence is a sum of two previous values
    // e.g. 4th value is equal 2+3, 5th = 5+3, 6th = 8+5, etc.
    static int climbStairs(int n) {
        // 45 is a maximum because int gets overflowed
        if ( n < 1 || n > 45 )
            return 0;
        if ( n == 1 )
            return 1;
        if ( n == 2 )
            return 2;
        int a = 1, b = 2, c, i = 3;
        do {
            c = a + b;
            a = b;
            b = c;
            i++;
        } while ( i <= n );
        return b;
    }

    @Test
    public void testClimbStairs() {
        Assertions.assertEquals(2, climbStairs(2));
        Assertions.assertEquals(3, climbStairs(3));
        Assertions.assertEquals(5, climbStairs(4));
        Assertions.assertEquals(8, climbStairs(5));
        Assertions.assertEquals(13, climbStairs(6));
        Assertions.assertEquals(21, climbStairs(7));
        Assertions.assertEquals(34, climbStairs(8));
        Assertions.assertEquals(55, climbStairs(9));
        Assertions.assertEquals(89, climbStairs(10));
    }

    public static void main(String[] args) {
        System.out.println(climbStairs(45));
    }

}
