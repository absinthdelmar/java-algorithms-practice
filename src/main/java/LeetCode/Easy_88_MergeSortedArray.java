package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * 88. Merge Sorted Array
 * Easy
 *
 * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.
 *
 * Note:
 *
 * The number of elements initialized in nums1 and nums2 are m and n respectively.
 * You may assume that nums1 has enough space (size that is equal to m + n) to hold additional elements from nums2.
 * Example:
 *
 * Input:
 * nums1 = [1,2,3,0,0,0], m = 3
 * nums2 = [2,5,6],       n = 3
 *
 * Output: [1,2,2,3,5,6]
 *
 *
 * Constraints:
 *
 * -10^9 <= nums1[i], nums2[i] <= 10^9
 * nums1.length == m + n
 * nums2.length == n
 */
public class Easy_88_MergeSortedArray {

    static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
        System.out.println("\nswap: " + Arrays.toString(nums));
    }

    static void merge0(int[] nums1, int m, int[] nums2, int n) {
        assert(nums1.length == m + n);
        assert(nums2.length == n);
        int i = 0, j = 0;
        if ( m > n ) {
            int[] temp = nums1;
            nums1 = nums2;
            nums2 = temp;
            int t = m;
            m = n;
            n = t;
        }
        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(nums2));
        while ( j < n ) {
            System.out.println("nums1[" + i + "] = " + nums1[i] + " nums2[" + j + "] = " + nums2[j]);
            while ( i < m ) {
                if ( nums2[j] < nums1[i] ) {
                    swap(nums1, i, m + j);
                    nums1[i] = nums2[j];
                    j++;
                }
                System.out.println("i = " + i + ", " + Arrays.toString(nums1));
                i++;
            }
            // put in the rest
            if ( m + j < m + n ) {
                nums1[m + j] = nums2[j];
                if ( m + j - 1 >= 0 && nums1[m + j - 1] > nums1[m + j] )
                    swap(nums1, m + j - 1, m + j);
            }
            j++;
        }
    }


    static void merge1(int[] nums1, int m, int[] nums2, int n) {
        assert (nums1.length == m + n);
        assert (nums2.length == n);
        int i = 0, j = 0;
        if ( m < n ) {
            int[] temp = new int[m + n];
            for (int k = 0; k < n; k++)
                temp[k] = nums2[k];
            nums2 = new int[m];
            for (int k = 0; k < m; k++) {
                nums2[k] = nums1[k];
            }
            for (int k = 0; k < n; k++) {
                nums1[k] = temp[k];
            }
            int t = m;
            m = n;
            n = t;
        }
        while ( i < m ) {
            System.out.print("nums1[" + i + "] = " + nums1[i]);
            while ( j < n ) {
                System.out.print(" nums2[" + j + "] = " + nums2[j]);
                if ( nums2[j] > nums1[i] )
                    break;
                // shift by 1
                int k = m + n - 1;
                while ( k > i ) {
                    swap(nums1, k, k - 1);
                    k--;
                }
                nums1[i] = nums2[j];
                j++;
            }
            System.out.println();
            i++;
        }
        while ( m + j < m + n && j < n ) {
            System.out.println("writing rest: " + nums2[j] + " at " + (m+j));
            nums1[m + j] = nums2[j];
            j++;
        }
        Arrays.sort(nums1);
        System.out.println("end: " + Arrays.toString(nums1));
    }

    // i'm an idiot lol, it is too simple as always
    static void merge(int[] nums1, int m, int[] nums2, int n) {
        // new resulting array of size m + n
        int[] result = new int[m + n];
        int i = 0, j = 0, k = 0;
        while ( i < m && j < n ) {
            // write nums2 number first being less than nums1 number
            if ( nums1[i] > nums2[j] ) {
                result[k++] = nums2[j++];
                System.out.println("from nums2: " + Arrays.toString(result));
            }
            // write nums1 number first being less than nums2 number
            else {
                result[k++] = nums1[i++];
                System.out.println("from nums1: " + Arrays.toString(result));
            }
        }
        // write the rest from nums1
        System.out.println("rest from nums1:");
        while ( i < m ) {
            result[k++] = nums1[i++];
        }
        System.out.println(Arrays.toString(result));
        // write the rest from nums2
        System.out.println("rest from nums2:");
        while ( j < n ) {
            result[k++] = nums2[j++];
        }
        System.out.println(Arrays.toString(result));
        // copy result contents into nums1 as its content will be tested for the final result
        System.out.println("copy all to nums1:");
        for (int l = 0; l < nums1.length; l++) {
            nums1[l] = result[l];
        }
        System.out.println(Arrays.toString(result));
    }

    @Test
    public void testMerge() {
        int[] nums1 = new int[]{1,2,3,0,0,0};
        int[] nums2 = new int[]{2,5,6};
        int m = 3, n = 3;
        merge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
        Assertions.assertArrayEquals(new int[]{1,2,2,3,5,6}, nums1);
    }

    @Test
    public void testMerge1() {
        int[] nums1 = new int[]{1,2,3,0,0,0};
        int[] nums2 = new int[]{0,1,1};
        int m = 3, n = 3;
        merge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
        Assertions.assertArrayEquals(new int[]{0,1,1,1,2,3}, nums1);
    }

    @Test
    public void testMerge2() {
        int[] nums1 = new int[]{4,0,0,0,0,0};
        int[] nums2 = new int[]{1,2,3,5,6};
        int m = 1, n = 5;
        merge(nums1, m, nums2, n);
        System.out.println("final: " + Arrays.toString(nums1));
        Assertions.assertArrayEquals(new int[]{1,2,3,4,5,6}, nums1);
    }

    @Test
    public void testMerge3() {
        int[] nums1 = new int[]{0};
        int[] nums2 = new int[]{1};
        int m = 0, n = 1;
        merge(nums1, m, nums2, n);
        System.out.println("final: " + Arrays.toString(nums1));
        Assertions.assertArrayEquals(new int[]{1}, nums1);
    }

    @Test
    public void testMerge4() {
        int[] nums1 = new int[]{1,2,4,5,6,0};
        int[] nums2 = new int[]{3};
        int m = 5, n = 1;
        merge(nums1, m, nums2, n);
        Assertions.assertArrayEquals(new int[]{1,2,3,4,5,6}, nums1);
    }

    @Test
    public void testMerge5() {
        int[] nums1 = new int[]{-1,2,2,4,6,0,0,0};
        int[] nums2 = new int[]{1,5,8};
        int m = 5, n = 3;
        merge(nums1, m, nums2, n);
        Assertions.assertArrayEquals(new int[]{-1,1,2,2,4,5,6,8}, nums1);
    }

    @Test
    public void testMerge6() {
        int[] nums1 = new int[]{1,5,8,0,0,0,0,0};
        int[] nums2 = new int[]{-1,2,2,4,6};
        int m = 3, n = 5;
        merge(nums1, m, nums2, n);
        Assertions.assertArrayEquals(new int[]{-1,1,2,2,4,5,6,8}, nums1);
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{1,2,5,6,0,0};
        int[] nums2 = new int[]{3,4};
        int m = 4, n = 2;
        merge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));
    }

}
