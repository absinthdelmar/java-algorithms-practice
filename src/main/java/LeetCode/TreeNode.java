package LeetCode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class TreeNode {

    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        return " [ " + this.val + " " + ( this.left != null ? " L: " + this.left.toString() : "-" ) +
                " " + ( this.right != null ? " R: " + this.right.toString() : "-" ) + " ] ";
    }

    // just for repeating sake to implement basic tree algos
    // depth first search (PREORDER, default)
    static void dfs(TreeNode root, List<Integer> list) {
        if ( root == null )
            return;

        list.add(root.val);

        dfs(root.left, list);
        dfs(root.right, list);
    }

    // alias
    static void dfsPreorder(TreeNode root, List<Integer> list) {
        dfs(root, list);
    }

    // depth first search - INORDER (recursive)
    static void dfsInorder(TreeNode root, List<Integer> list) {
        if ( root == null )
            return;

        dfsInorder(root.left, list);
        list.add(root.val);
        dfsInorder(root.right, list);
    }

    // depth first search - INORDER (iterative) using Stack
    static void dfsInorderIterative(TreeNode root, List<Integer> list) {
        if (root != null) {
            Stack<TreeNode> stack = new Stack<>();

            TreeNode current = root;
            while ( current != null || !stack.isEmpty() ) {
                // go left until the end
                while ( current != null ) {
                    stack.push(current);
                    current = current.left;
                }
                // current is null at this point, so pick the last from stack
                current = stack.pop();
                // add popped value to resulting list
                list.add(current.val);
                // go right
                current = current.right;
            }
        }
    }

    // depth first search - POSTORDER
    static void dfsPostOrder(TreeNode root, List<Integer> list) {
        if ( root == null )
            return;

        dfsPostOrder(root.left, list);
        dfsPostOrder(root.right, list);
        list.add(root.val);
    }

    // breadth-first-search or level order search
    // list content matches LeetCode output for BFS
    static void bfs(TreeNode root, List<Integer> list) {
        if ( root == null )
            return;

        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        while ( !queue.isEmpty() ) {
            TreeNode node = queue.poll();

            if ( node == null ) {
                if ( !queue.isEmpty() )
                    list.add(null);
            }
            else {
                list.add(node.val);

                if (node.left != null && node.right != null) {
                    queue.add(node.left);
                    queue.add(node.right);
                } else if (node.left != null) {
                    queue.add(node.left);
                    queue.add(null);
                } else if (node.right != null) {
                    queue.add(null);
                    queue.add(node.right);
                }
            }
        }
    }

    // binary tree height
    static int height(TreeNode root) {
        if ( root == null )
            return 0;

        return Math.max(height(root.left), height(root.right)) + 1;
    }

    // check if tree is balanced starting from specific node
    static boolean isBalanced(TreeNode root) {
        if ( root == null )
            return true;

        int lh = height(root.left);
        int rh = height(root.right);

        return Math.abs(lh - rh) <= 1 && isBalanced(root.left) && isBalanced(root.right);
    }

    // depth of a node in a binary tree root
    // starts from level = 1
    static int depthOf(TreeNode root, TreeNode node, int level) {
        if ( root == null )
            return 0;
        if ( root == node )
            return level;
        int left = depthOf(root.left, node, level + 1);
        return left > 0 ? left : depthOf(root.right, node, level + 1);
    }

    static TreeNode parentOf(TreeNode root, TreeNode node) {
        // basic case anything is null
        if ( root == null || node == null || ( root.left == null && root.right == null ) )
            return null;
        // root is the parent of node
        if ( ( root.left  != null && root.left.val  == node.val ) ||
                ( root.right != null && root.right.val == node.val ) )
            return root;
        // look elsewhere - left and right
        TreeNode left = parentOf(root.left, node);
        return left != null ? left : parentOf(root.right, node);
    }

    // LCA
    static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        int dp = depthOf(root, p, 1);
        int dq = depthOf(root, q, 1);

//        System.out.println("depth of p=" + p.val + " is " + dp);
//        System.out.println("depth of q=" + q.val + " is " + dq);

        // climb up
        while ( dp != dq ) {
//            System.out.println(dp + " vs " + dq);
            // p is deeper than q, climb up p
            if ( dp > dq ) {
                p = parentOf(root, p);
                dp--;
            }
            // q is deeper than p, climb up q
            else {
                q = parentOf(root, q);
                dq--;
            }
        }

        // update parents of p and q until they meet
        while ( p != null && q != null && p.val != q.val ) {
            p = parentOf(root, p);
            q = parentOf(root, q);
        }

        return p;
    }

}
