package LeetCode;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 349. Intersection of Two Arrays
 * Easy
 *
 * Given two arrays, write a function to compute their intersection.
 *
 * Example 1:
 *
 * Input: nums1 = [1,2,2,1], nums2 = [2,2]
 * Output: [2]
 * Example 2:
 *
 * Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * Output: [9,4]
 * Note:
 *
 * Each element in the result must be unique.
 * The result can be in any order.
 */
public class Easy_349_IntersectionOfTwoArrays {

    // works for any int numbers
    // 14 ms very slow
    static int[] intersection0(int[] nums1, int[] nums2) {
        Set<Integer> s = new HashSet<>();

        int count = 0;
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length; j++) {
                if ( nums1[i] == nums2[j] ) {
                    count++;
                    s.add(nums1[i]);
                }
            }
        }

        System.out.println(count);
        System.out.println(s);

        return s.stream().mapToInt(Number::intValue).toArray();
    }

    static int[] add(int[] nums, int num){
        int n = nums.length;
        int[] result = new int[n + 1];
        System.arraycopy(nums, 0, result, 0 , n);
        result[n] = num;
        return result;
    }

    // works for numbers 0 <= n <= 1000 using counting array
    // 0 ms
    static int[] intersection(int[] nums1, int[] nums2) {
        int[] count = new int[1001];

        for (int i = 0; i < nums1.length; i++) {
            if (count[nums1[i]] == 0)
                count[nums1[i]]++;
        }

        int[] result = new int[0];

        for (int i = 0; i < nums2.length; i++) {
            if (count[nums2[i]] == 1) {
                result = add(result, nums2[i]);
                count[nums2[i]]--;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{1,2,2,1};
        int[] nums2 = new int[]{2,2};
        System.out.println(Arrays.toString(intersection(nums1, nums2)));
        nums1 = new int[]{4,9,5};
        nums2 = new int[]{9,4,9,8,4};
        System.out.println(Arrays.toString(intersection(nums1, nums2)));
    }

}
