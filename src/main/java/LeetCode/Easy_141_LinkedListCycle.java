package LeetCode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 141. Linked List Cycle
 * Easy
 *
 * Given a linked list, determine if it has a cycle in it.
 *
 * To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.
 *
 *
 *
 * Example 1:
 *
 * Input: head = [3,2,0,-4], pos = 1
 * Output: true
 * Explanation: There is a cycle in the linked list, where tail connects to the second node.
 *
 *
 * Example 2:
 *
 * Input: head = [1,2], pos = 0
 * Output: true
 * Explanation: There is a cycle in the linked list, where tail connects to the first node.
 *
 *
 * Example 3:
 *
 * Input: head = [1], pos = -1
 * Output: false
 * Explanation: There is no cycle in the linked list.
 *
 *
 *
 *
 * Follow up:
 *
 * Can you solve it using O(1) (i.e. constant) memory?
 *
 *
 *
 * Definition for singly-linked list.
 * class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) {
 *         val = x;
 *         next = null;
 *     }
 * }
 *
 */
public class Easy_141_LinkedListCycle {

    static class ListNode {
       int val;
       ListNode next;
       ListNode(int x) {
           val = x;
           next = null;
       }
    }

    // my slow 4 ms version using set
    static boolean hasCycle0(ListNode head) {
        Set<ListNode> nodes = new HashSet<>();
        ListNode temp = head;
        while( temp != null ) {
            if ( !nodes.add(temp) )
                return true;
            temp = temp.next;
        }
        return false;
    }

    // my very slow 9 ms version
    static boolean hasCycle1(ListNode head) {
        Map<ListNode, Integer> map = new HashMap<>();
        ListNode temp = head;
        while( temp != null ) {
            Integer count = map.get( temp );
            if ( count != null && count > 1 )
                return true;
            map.put( temp, count != null ? count + 1 : 0 );
            temp = temp.next;
        }
        return false;
    }

    /**
     * Floyd's Cycle Detection Algorithm (The Tortoise and the Hare)
     *
     * http://www.siafoo.net/algorithm/10
     *
     * How do you determine if your singly-linked list has a cycle? In the late 1960s, Robert W. Floyd invented an
     * algorithm that worked in linear (O(N)) time. It is also called Floyd's cycle detection algorithm.
     *
     * The easiest solution to the cycle detection problem is to run through the list, keeping track of which nodes
     * you visit, and on each node check to see if it is the same as any of the previous nodes. It's pretty obvious
     * that this runs in quadratic (O(N^2)) time... not very efficient, and actually more complicated than this one.
     *
     * The Tortoise and the Hare is possibly the most famous cycle detection algorithm, and is surprisingly
     * straightforward. The Tortoise and the Hare are both pointers, and both start at the top of the list.
     * For each iteration, the Tortoise takes one step and the Hare takes two. If there is a loop, the hare will go
     * around that loop (possibly more than once) and eventually meet up with the turtle when the turtle gets into the
     * loop. If there is no loop, the hare will get to the end of the list without meeting up with the turtle.
     *
     * Why can't you just let the hare go by itself? If there was a loop, it would just go forever; the turtle
     * ensues you will only take n steps at most.
     *
     */
    static boolean hasCycle2(ListNode head) {
        if ( head == null )
            return false;
        ListNode turtle = head;     // slow moving pointer
        ListNode hare = head;       // fast moving pointer (slow * 2)
        while( turtle != null && hare != null && hare.next != null ) {
            turtle = turtle.next;
            hare = hare.next.next;
            if ( turtle == hare )
                return true;
        }
        return false;
    }

    /**
     * Brent's Cycle Finding Algorithm
     *
     * http://www.siafoo.net/algorithm/11
     *
     * In 1980, Richard Brent invented an algorithm that not only worked in linear time, but required less stepping
     * than Floyd's Tortoise and the Hare algorithm (however it is slightly more complex).
     * Although stepping through a 'regular' linked list is computationally easy, these algorithms are also used
     * for factorization and pseudorandom number generators, linked lists are implicit and finding the next member
     * is computationally difficult.
     *
     * Brent's algorithm features a moving rabbit and a stationary, then teleporting, turtle.
     * Both turtle and rabbit start at the top of the list. The rabbit takes one step per iteration.
     * If it is then at the same position as the stationary turtle, there is obviously a loop.
     * If it reaches the end of the list, there is no loop.
     *
     * Of course, this by itself will take infinite time if there is a loop. So every once in a while, we
     * teleport the turtle to the rabbit's position, and let the rabbit continue moving.
     * We start out waiting just 2 steps before teleportation, and we double that each time we move the turtle.
     *
     * Why move the turtle at all? Well, the loop might not include the entire list; if a rabbit gets stuck in a loop
     * further down, without the turtle, it will go forever. Why take twice as long each time?
     * Eventually, the length of time between teleportations will become longer than the size of the loop, and the
     * turtle will be there waiting for the rabbit when it gets back.
     *
     * Note that like Floyd's Tortoise and Hare algorithm, this one runs in O(N).
     * However you're doing less stepping than with Floyd's (in fact the upper bound for steps is the number you
     * would do with Floyd's algorithm). According to Brent's research, his algorithm is 24-36% faster on average
     * for implicit linked list algorithms.
     *
     */
    static boolean hasCycle(ListNode head) {
        if (head == null)
            return false;
        ListNode turtle = head;
        ListNode rabbit = head;
        int steps_taken = 0;
        int step_limit = 2;
        while ( rabbit != null ) {
            // move rabbit to the next position
            rabbit = rabbit.next;

            // increase steps taken counter
            steps_taken++;

            // if rabbit meets turtle a cycle is found
            if ( rabbit == turtle )
                return true;

            // teleport turtle, increased step limit by a factor of 2 -> step_limit = Math.pow(step_limit, 2)
            if ( steps_taken == step_limit ) {
                steps_taken = 0;
                step_limit <<= 1;
                turtle = rabbit;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(3);
        head.next = new ListNode(2);
        head.next.next = new ListNode(0);
        head.next.next.next = new ListNode(-4);
        head.next.next.next.next = head.next;
        System.out.println(hasCycle(head));
    }

}
