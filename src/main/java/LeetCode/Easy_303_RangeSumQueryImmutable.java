package LeetCode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 * 303. Range Sum Query - Immutable
 * Easy
 *
 * Given an integer array nums, find the sum of the elements between indices i and j (i ≤ j), inclusive.
 *
 * Example:
 *
 * Given nums = [-2, 0, 3, -5, 2, -1]
 *
 * sumRange(0, 2) -> 1
 * sumRange(2, 5) -> -1
 * sumRange(0, 5) -> -3
 *
 *
 * Constraints:
 *
 * You may assume that the array does not change.
 * There are many calls to sumRange function.
 * 0 <= nums.length <= 10^4
 * -10^5 <= nums[i] <= 10^5
 * 0 <= i <= j < nums.length
 *
 */
public class Easy_303_RangeSumQueryImmutable {

    // slow 156 ms
    static class NumArray0 {

        private final int[] nums;
        private Map<Integer, Integer> cache;

        public NumArray0(int[] nums) {
            this.nums = nums;
            this.cache = new HashMap<>();
        }

        public int sumRange(int i, int j) {
            System.out.println(cache);
            Integer key = key(i, j);
            Integer cached = cache.get(key);
            if ( cached != null )
                return cached;
            long sum = 0;
            for (int k = i; k <= j; k++) {
                sum += nums[k];
            }
            cache.put(key, (int)sum);
            return (int)sum;
        }

        // cantor pairing
        static int key(int i, int j) {
            return ( (i + j) * (i + j + 1) / 2 ) + j;
        }

    }

    // we need to cache sum somehow not to iterate over and over again
    // we have to use previously calculated sums
    // unacceptable on LeetCode due to security barrier on threads
    static class NumArray1 {

        private final int[] nums;
        private ForkJoinPool fjp;

        static class Sum extends RecursiveTask<Integer> {

            private final static int THRESHOLD = 10;
            private int hi, lo;
            private int[] arr;

            Sum(int[] arr, int lo, int hi) {
                this.arr = arr;
                this.lo = lo;
                this.hi = hi;
            }

            protected Integer compute() {
                if ( hi - lo <= THRESHOLD ) {
                    int result = 0;
                    for (int i = lo; i <= hi; i++) {
                        result += arr[i];
                    }
                    return result;
                }
                else {
                    int mid = (lo + hi) / 2;
                    Sum left = new Sum(arr, lo, mid);
                    Sum right = new Sum(arr, mid, hi);
                    left.fork();
                    int rs = right.compute();
                    int ls = left.join();
                    return rs + ls;
                }
            }
        }

        public NumArray1(int[] nums) {
            this.nums = nums;
            this.fjp = new ForkJoinPool();
        }

        public int sumRange(int i, int j) {
            return fjp.invoke(new Sum(nums, i, j));
        }

    }

    // using pre-calculated sums of current and previous elements
    static class NumArray {

        private int[] cache;

        // populate sum cache array on init
        public NumArray(int[] nums) {
            int n = nums.length;
            cache = new int[n + 1];
            // this is by default on init, all elements set to zero
            //cache[0] = 0;
            for (int i = 1; i <= n; i++) {
                cache[i] = nums[i - 1] + cache[i - 1];
            }
            System.out.println(Arrays.toString(cache));
        }

        public int sumRange(int i, int j) {
            return cache[j + 1] - cache[i];
        }

    }

    public static void main(String[] args) {
        NumArray obj = new NumArray(new int[]{-2, 0, 3, -5, 2, -1});
        System.out.println(obj.sumRange(0, 2));
        System.out.println(obj.sumRange(2, 5));
        System.out.println(obj.sumRange(0, 5));
    }

}
