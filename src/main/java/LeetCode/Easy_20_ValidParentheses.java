package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * 20. Valid Parentheses
 * Easy
 *
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 *
 * An input string is valid if:
 * - Open brackets must be closed by the same type of brackets.
 * - Open brackets must be closed in the correct order.
 *
 * Note that an empty string is also considered valid.
 */
public class Easy_20_ValidParentheses {

    // dirty

    static boolean isValid(String s) {
        if ( s == null || s.length() == 0 )
            return true;
        if ( s.length() % 2 != 0 )
            return false;

        System.out.println("\nString is < " + s + " >");

        // opening element and its corresponding closing one
        Map<Character, Character> allowed = new HashMap<>();
        allowed.put('(', ')');
        allowed.put('[', ']');
        allowed.put('{', '}');

        Stack<Character> next = new Stack<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            // opening
            if (allowed.keySet().contains(c)) {
                System.out.println("got opening: " + c);
                //                if (!allowed.keySet().contains(allowed.get(c)))
                next.push(allowed.get(c));
            }
            // closing
            else if (allowed.values().contains(c)) {
                System.out.print("got closing: " + c);
                if (next.empty())
                    return false;
                char expected = next.peek();
                System.out.println(" vs expected: " + expected);
                if (expected == c)
                    next.pop();
                else
                    return false;
            }
            // none of the above
            else {
                return false;
            }
            System.out.println("expected " + allowed.keySet() + " or next closing: " + (next.empty() ? "empty" : next));
        }
        return next.isEmpty();
    }

    // clean version

    static Map<Character, Character> allowed = new HashMap<>();

    static {
        allowed.put('(', ')');
        allowed.put('[', ']');
        allowed.put('{', '}');
    }

    static boolean isValidClean(String s) {
        if ( s == null || s.length() == 0 )
            return true;

        Stack<Character> next = new Stack<>();

        for ( char c : s.toCharArray() ) {
            Character temp = allowed.get(c);
            if ( temp != null ) {
                next.push( temp );
            } else {
                if ( next.empty() || next.pop() != c )
                    return false;
            }
        }

        return next.isEmpty();
    }

    // version 3 blazing fast, no map, simple array stack
    static boolean isValidFast(String s) {
        if ( s == null )
            return true;
        char[] st = new char[s.length()];
        int i = 0;
        for (char c: s.toCharArray()) {
            switch (c) {
                case '(':
                case '[':
                case '{':
                    st[i++] = c;
                    break;
                case ')':
                    if ( i == 0 || st[--i] != '(' )
                        return false;
                    break;
                case ']':
                    if ( i == 0 || st[--i] != '[' )
                        return false;
                    break;
                case '}':
                    if ( i == 0 || st[--i] != '{' )
                        return false;
                    break;
            }
        }
        return i == 0;
    }

    @Test
    public void testIsValid() {
        Assertions.assertTrue(isValid("()"));
        Assertions.assertTrue(isValid("()[]{}"));
        Assertions.assertFalse(isValid("(]"));
        Assertions.assertFalse(isValid("([)]"));
        Assertions.assertTrue(isValid("{[]}"));
        Assertions.assertFalse(isValid("({[]}}}"));
        Assertions.assertFalse(isValid("["));
        Assertions.assertFalse(isValid("[["));
        Assertions.assertFalse(isValid("[[["));
        Assertions.assertFalse(isValid("]"));
        Assertions.assertFalse(isValid("]]"));
        Assertions.assertFalse(isValid("]]]"));
    }

    @Test
    public void testIsValidClean() {
        Assertions.assertTrue(isValidClean("()"));
        Assertions.assertTrue(isValidClean("()[]{}"));
        Assertions.assertFalse(isValidClean("(]"));
        Assertions.assertFalse(isValidClean("([)]"));
        Assertions.assertTrue(isValidClean("{[]}"));
        Assertions.assertFalse(isValidClean("({[]}{}{}{}{}}}}[[[[((((()))))}}"));
        Assertions.assertFalse(isValidClean("["));
        Assertions.assertFalse(isValidClean("[["));
        Assertions.assertFalse(isValidClean("[[["));
        Assertions.assertFalse(isValidClean("]"));
        Assertions.assertFalse(isValidClean("]]"));
        Assertions.assertFalse(isValidClean("]]]"));
    }

    @Test
    public void testIsValidFast() {
        Assertions.assertTrue(isValidFast("()"));
        Assertions.assertTrue(isValidFast("()[]{}"));
        Assertions.assertFalse(isValidFast("(]"));
        Assertions.assertFalse(isValidFast("([)]"));
        Assertions.assertTrue(isValidFast("{[]}"));
        Assertions.assertFalse(isValidFast("({[]}{}{}{}{}}}}[[[[((((()))))}}"));
        Assertions.assertFalse(isValidFast("["));
        Assertions.assertFalse(isValidFast("[["));
        Assertions.assertFalse(isValidFast("[[["));
        Assertions.assertFalse(isValidFast("]"));
        Assertions.assertFalse(isValidFast("]]"));
        Assertions.assertFalse(isValidFast("]]]"));
        Assertions.assertFalse(isValidFast("]]]<>"));
    }

    @Test
    public void testDummy() {

    }

    public static void main(String[] args) {
        System.out.println(isValid("()"));      // true
        System.out.println(isValid("()[]{}"));  // true
        System.out.println(isValid("(]"));      // false
        System.out.println(isValid("([)]"));    // false
        System.out.println(isValid("{[]}"));    // true
        System.out.println(isValid("{{[]}}}"));    // false
    }
}
