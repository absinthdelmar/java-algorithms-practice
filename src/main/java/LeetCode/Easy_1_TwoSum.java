package LeetCode;

import java.util.Arrays;

/**
 * 1. Easy - TwoSum
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 *
 * Example:
 *
 * Given nums = [2, 7, 11, 15], target = 9,
 *
 * Because nums[0] + nums[1] = 2 + 7 = 9,
 * return [0, 1].
 */
public class Easy_1_TwoSum {

    static int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];

        if (nums.length < 2)
            return result;

        for (int i=0; i<nums.length - 1; i++) {
            for (int j=nums.length - 1; j > 0; j--) {
                if ( i != j ) {
                    System.out.print(i + ", " + j);
                    if ( nums[i] + nums[j] == target ) {
                        System.out.println(" hit! ");
                        result[0] = i;
                        result[1] = j;
                        return result;
                    }
                }
                System.out.println();
            }
        }

        return result;
    }

    public static void main(String[] args) {
        int[] input = new int[]{1, 3, 4, 2};
        int[] answer = twoSum(input, 6);
        System.out.println(Arrays.toString(answer));
    }

}
