package LeetCode;

import java.util.*;

/**
 * 299. Bulls and Cows
 * Easy
 *
 * You are playing the following Bulls and Cows game with your friend: You write down a number and ask your friend
 * to guess what the number is. Each time your friend makes a guess, you provide a hint that indicates how many digits
 * in said guess match your secret number exactly in both digit and position (called "bulls") and how many digits
 * match the secret number but locate in the wrong position (called "cows"). Your friend will use successive guesses
 * and hints to eventually derive the secret number.
 *
 * Write a function to return a hint according to the secret number and friend's guess, use A to indicate the bulls
 * and B to indicate the cows.
 *
 * Please note that both secret number and friend's guess may contain duplicate digits.
 *
 * Example 1:
 *
 * Input: secret = "1807", guess = "7810"
 *
 * Output: "1A3B"
 *
 * Explanation: 1 bull and 3 cows. The bull is 8, the cows are 0, 1 and 7.
 * Example 2:
 *
 * Input: secret = "1123", guess = "0111"
 *
 * Output: "1A1B"
 *
 * Explanation: The 1st 1 in friend's guess is a bull, the 2nd or 3rd 1 is a cow.
 * Note: You may assume that the secret number and your friend's guess only contain digits, and their lengths are
 * always equal.
 */
public class Easy_299_BullsAndCows {

    // bulls - how many digits and their position matched
    // cows - how many digits matched but in wrong position
    // 12 ms slow
    static String getHint(String secret, String guess) {
        if ( secret.length() != guess.length() )
            return "0A0B";

        int bulls = 0, cows = 0;
        // counters of digits for both secret and guess
        int[] s = new int[10];
        int[] g = new int[10];

        for (int i = 0; i < secret.length(); i++) {
            // increase bulls if both secret and guess digits and their positions match
            if ( secret.charAt(i) == guess.charAt(i) ) {
                bulls++;
            }
            // increase counters for both otherwise
            // index is the character code - character code for '0' making it simply 0-9
            else {
                s[secret.charAt(i) - '0']++;
                g[guess.charAt(i) - '0']++;
            }
        }

        // increase cows counter by choosing minimal counter between secret and guess appearing digits
        for (int i = 0; i < g.length; i++) {
            cows += Math.min( g[i], s[i] );
        }

        return String.format("%dA%dB", bulls, cows);
    }

    public static void main(String[] args) {
        System.out.println(getHint("1807", "7810"));    // 1A3B
        System.out.println(getHint("1123", "0111"));    // 1A1B
        System.out.println(getHint("11", "10"));        // 1A0B
        System.out.println(getHint("1122", "1222"));    // 3A0B
    }

}
