package LeetCode;

import java.util.Arrays;

/**
 * 268. Missing Number
 * Easy
 *
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.
 *
 * Example 1:
 *
 * Input: [3,0,1]
 * Output: 2
 * Example 2:
 *
 * Input: [9,6,4,2,3,5,7,0,1]
 * Output: 8
 * Note:
 * Your algorithm should run in linear runtime complexity.
 * Could you implement it using only constant extra space complexity?
 */
public class Easy_268_MissingNumber {

    // 5 ms, extra time
    static int missingNumber0(int[] nums) {
        if ( nums == null || nums.length == 0 ) {
            return 0;
        }
        if ( nums.length == 1 ) {
            int min = Math.min(nums[0], nums[0] - 1);
            int max = Math.max(nums[0], nums[0] + 1);
            System.out.println(min + " vs " + max);
            return min >= 0 ? min : max;
        }

        System.out.println(nums.length);

        Arrays.sort(nums);

        for (int i = 0; i < nums.length; i++) {
            System.out.println(i + " vs " + nums[i]);
            if ( nums[i] != i )
                return i;
        }

        return nums.length;
    }

    // 1 ms using boolean array marking
    static int missingNumber1(int[] nums) {
        if ( nums == null || nums.length == 0 ) {
            return 0;
        }
        if ( nums.length == 1 ) {
            int left = nums[0] - 1;
            int right = nums[0] + 1;
            return left < nums[0] && left >= 0 ? left : right;
        }
        boolean[] b = new boolean[nums.length + 1];
        for (int i = 0; i < nums.length; i++) {
            b[nums[i]] = true;
        }
//        System.out.println(Arrays.toString(b));
        for (int i = 0; i < b.length; i++) {
            if ( !b[i] )
                return i;
        }

        return nums.length;
    }

    // Another interesting way is to ^ every index with the number and the size of array, it is a unique solution
    // 0 ms
    static int missingNumber2(int[] nums) {
        int result = nums.length;
        for (int i = 0; i < nums.length; i++)
            result ^= i ^ nums[i];
        return result;
    }

    // lol
    static int missingNumber(int[] nums) {
        int n = nums.length;
        int expected = n*(n+1)/2;
        int actual = 0;
        for(int i=0; i < n; i++)
            actual += nums[i];
        return expected - actual;
    }

    public static void main(String[] args) {
        System.out.println(missingNumber(new int[]{0}));
        System.out.println(missingNumber(new int[]{1}));
        System.out.println(missingNumber(new int[]{0,1}));
        System.out.println(missingNumber(new int[]{3,0,1}));
        System.out.println(missingNumber(new int[]{9,6,4,2,3,5,7,0,1}));
    }

}
