package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 111. Minimum Depth of Binary Tree
 * Easy
 *
 * Given a binary tree, find its minimum depth.
 *
 * The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Given binary tree [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * return its minimum depth = 2.
 */
public class Easy_111_MinimumDepthOfBinaryTree {

    static int minDepth(TreeNode root) {
        if ( root == null )
            return 0;

        if ( root.left == null && root.right == null )
            return 1;

        if ( root.left == null )
            return minDepth(root.right) + 1;

        if ( root.right == null )
            return minDepth(root.left) + 1;

        return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
    }

    @Test
    public void testMinDepth() {
        TreeNode tree = new TreeNode(
                3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7))
        );
        Assertions.assertEquals(2, minDepth(tree));
    }

    @Test
    public void testMinDepth2() {
        TreeNode tree = new TreeNode(1, new TreeNode(2), null);
        Assertions.assertEquals(2, minDepth(tree));
    }

    public static void main(String[] args) {
        TreeNode tree = new TreeNode(
            3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7))
        );
//        TreeNode tree = new TreeNode(1, null, new TreeNode(2));
        System.out.println(minDepth(tree));
    }

}
