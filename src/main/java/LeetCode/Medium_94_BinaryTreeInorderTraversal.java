package LeetCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 94. Binary Tree Inorder Traversal
 * Medium
 *
 * Given a binary tree, return the inorder traversal of its nodes' values.
 *
 * Example:
 *
 * Input: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * Output: [1,3,2]
 * Follow up: Recursive solution is trivial, could you do it iteratively?
 */
public class Medium_94_BinaryTreeInorderTraversal {

    static void dfsRecursive(TreeNode root, List<Integer> list) {
        if (root == null)
            return;
        dfsRecursive(root.left, list);
        list.add(root.val);
        dfsRecursive(root.right, list);
    }

    static void dfsIterative(TreeNode root, List<Integer> list) {
        if (root != null) {
            Stack<TreeNode> stack = new Stack<>();

            TreeNode current = root;
            while ( current != null || !stack.isEmpty() ) {
                while ( current != null ) {
                    stack.push(current);
                    current = current.left;
                }
                current = stack.pop();
                list.add(current.val);
                current = current.right;
            }
        }
    }

    // recursive
    static List<Integer> inorderTraversalR(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        dfsRecursive(root, result);
        return result;
    }

    // iterative
    static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        dfsIterative(root, result);
        return result;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null));
//        TreeNode root = new TreeNode(1,
//                new TreeNode(2, new TreeNode(4), new TreeNode(5)),
//                new TreeNode(3));
        System.out.println(inorderTraversalR(root));
        System.out.println(inorderTraversal(root));
    }

}
