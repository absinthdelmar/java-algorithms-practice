package LeetCode;

import java.util.Arrays;

/**
 * 344. Reverse String
 * Easy
 *
 * Write a function that reverses a string. The input string is given as an array of characters char[].
 *
 * Do not allocate extra space for another array, you must do this by modifying the input array in-place
 * with O(1) extra memory.
 *
 * You may assume all the characters consist of printable ascii characters.
 */
public class Easy_344_ReverseString {

    // 0 ms
    static void reverseString(char[] s) {
        int n = s.length;
        int i = 0, j = n - 1;
        while ( i < j ) {
//            System.out.println(s[i] + " vs " + s[j]);
            char c = s[i];
            s[i] = s[j];
            s[j] = c;
            System.out.println(s[i] + " vs " + s[j]);
            i++;
            j--;
        }
    }

    public static void main(String[] args) {
        char[] s = "hello world!".toCharArray();
        reverseString(s);
        System.out.println(Arrays.toString(s));
        System.out.println(String.valueOf(s));
    }

}
