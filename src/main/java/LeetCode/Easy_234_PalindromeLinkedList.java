package LeetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 234. Palindrome Linked List
 * Easy
 *
 * Given a singly linked list, determine if it is a palindrome.
 *
 * Example 1:
 *
 * Input: 1->2
 * Output: false
 * Example 2:
 *
 * Input: 1->2->2->1
 * Output: true
 *
 * Follow up:
 * Could you do it in O(n) time and O(1) space?
 */
public class Easy_234_PalindromeLinkedList {

    static ListNode reverseList(ListNode head) {
        if ( head == null )
            return null;
        ListNode next, prev = null;
        ListNode current = head;
        while ( current != null ) {
            // save current node next reference
            next = current.next;
            // update it to previous reference (null on first iteration)
            current.next = prev;
            // update previous to current
            prev = current;
            // update current, make it saved next
            current = next;
        }
        return prev;
    }

    // 4 ms very slow
    static boolean isPalindrome0(ListNode head) {
        List<Integer> first = new ArrayList<>();
        List<Integer> second = new ArrayList<>();
        ListNode temp = head;
        while ( temp != null ) {
            first.add(temp.val);
            temp = temp.next;
        }
        head = reverseList(head);
        temp = head;
        while ( temp != null ) {
            second.add(temp.val);
            temp = temp.next;
        }
        return first.equals(second);
    }

    // 0 ms with reverse in place
    static boolean isPalindrome1(ListNode head) {
        ListNode current = head;
        ListNode fast = head;
        ListNode prev = null;
        while ( fast != null && fast.next != null ) {
            // move fast
            fast = fast.next.next;

            // reverse
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        System.out.println("fast: " + fast);
        System.out.println("current: " + current);
        System.out.println("prev: " + prev);
        // too fast, set fast to last current and current to previous
        if ( fast == null ) {
            System.out.println("too fast! no middle :)");
            fast = current;
            current = prev;
        }
        else {
            System.out.println("there is middle, who cares about it, skipping it :)");
            fast = current.next;
            current = prev;
        }
        System.out.println("fast: " + fast);
        System.out.println("current: " + current);

        // go straight and reversed, checking each value
        while ( current != null && fast != null ) {
            System.out.println(current.val + " vs " + fast.val);
            if ( current.val != fast.val )
                return false;
            current = current.next;
            fast = fast.next;
        }

        return true;
    }

    static ListNode reverse(ListNode head) {
        ListNode prev = null;
        while ( head != null ) {
            ListNode next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }
        return prev;
    }

    static boolean isPalindrome(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;
        while ( fast != null && fast.next != null ) {
            slow = slow.next;
            fast = fast.next.next;
        }
        fast = head;
        slow = reverse(slow);
        while ( slow != null && fast != null ) {
            if ( slow.val != fast.val )
                return false;
            slow = slow.next;
            fast = fast.next;
        }
        return true;
    }

    public static void main(String[] args) {
        ListNode head = ListNode.addNode(null, 1);
        head = ListNode.addNode(head, 2);
        head = ListNode.addNode(head, 3);
        head = ListNode.addNode(head, 5);
        head = ListNode.addNode(head, 2);
        head = ListNode.addNode(head, 3);
        head = ListNode.addNode(head, 1);
        System.out.println(isPalindrome(head));
    }

}
