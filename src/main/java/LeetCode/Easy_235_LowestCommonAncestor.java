package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * ALSO: 236. Lowest Common Ancestor of a Binary Tree is a general, more complex version
 *
 * 235. Lowest Common Ancestor of a Binary Search Tree (BST) is a simplier version
 * Easy
 *
 * Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.
 *
 * According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between
 * two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node
 * to be a descendant of itself).”
 *
 * Given binary search tree:  root = [6,2,8,0,4,7,9,null,null,3,5]
 *
 *
 *
 *
 * Example 1:
 *
 * Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 8
 * Output: 6
 * Explanation: The LCA of nodes 2 and 8 is 6.
 * Example 2:
 *
 * Input: root = [6,2,8,0,4,7,9,null,null,3,5], p = 2, q = 4
 * Output: 2
 * Explanation: The LCA of nodes 2 and 4 is 2, since a node can be a descendant of itself according
 * to the LCA definition.
 *
 *
 * Constraints:
 *
 * All of the nodes' values will be unique.
 * p and q are different and both values will exist in the BST.
 */
public class Easy_235_LowestCommonAncestor {

    // initial level is 1
    static int depthOf(TreeNode root, TreeNode node, int level) {
        if ( root == null )
            return 0;
        if ( root == node )
            return level;
        int left = depthOf(root.left, node, level + 1);
        return left > 0 ? left :
                depthOf(root.right, node, level + 1);
    }

    static TreeNode parentOf(TreeNode root, TreeNode node) {
        // basic case anything is null
        if ( root == null || node == null || ( root.left == null && root.right == null ) )
            return null;
        // root is the parent of node
        if ( ( root.left  != null && root.left.val  == node.val ) ||
             ( root.right != null && root.right.val == node.val ) )
            return root;
        // look elsewhere - left and right
        TreeNode left = parentOf(root.left, node);
        return left != null ? left :
                parentOf(root.right, node);
    }

    // general algorithm for a binary tree (not just bst)
    // 7 ms
    static TreeNode lowestCommonAncestor0(TreeNode root, TreeNode p, TreeNode q) {
        int dp = depthOf(root, p, 1);
        int dq = depthOf(root, q, 1);

        System.out.println("depth of p=" + p.val + " is " + dp);
        System.out.println("depth of q=" + q.val + " is " + dq);

//        TreeNode parent = parentOf(root, p);
//        System.out.println("parentOf '" + p.val +"' is '" + parent.val + "'");

        // climb up
        while ( dp != dq ) {
            System.out.println(dp + " vs " + dq);
            if ( dp > dq ) {
                System.out.println("p is deeper than q, climb up p");
                p = parentOf(root, p);
                dp--;
            }
            else {
                System.out.println("q is deeper than p, climb up q");
                q = parentOf(root, q);
                dq--;
            }
        }

        // update parents of p and q until they meet
        while ( p != null && q != null && p.val != q.val ) {
            p = parentOf(root, p);
            q = parentOf(root, q);
        }

        return p;
    }

    // tree is a BST
    // 4 ms
    static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        // p value is less than q value, swap them
        if ( p.val < q.val )
            return lowestCommonAncestor(root, q, p);
        // p & q values are higher than current, then go right
        if ( p.val > root.val && q.val > root.val )
            return lowestCommonAncestor(root.right, p, q);
        // p & q values are lower than current, then go left
        else if ( p.val < root.val && q.val < root.val  )
            return lowestCommonAncestor(root.left, p, q);
        else
            return root;
    }

    @Test
    public void testLCA() {
        TreeNode p = new TreeNode(2, new TreeNode(0), new TreeNode(4, new TreeNode(3), new TreeNode(5)));
        TreeNode q = new TreeNode(8, new TreeNode(7), new TreeNode(9));
        TreeNode root = new TreeNode(6, p, q );
        Assertions.assertEquals( root, lowestCommonAncestor(root, p, q) );
    }

    @Test
    public void testLCA2() {
        TreeNode q = new TreeNode(4, new TreeNode(3), new TreeNode(5));
        TreeNode p = new TreeNode(2, new TreeNode(0), q);
        TreeNode root = new TreeNode(6, p, new TreeNode(8, new TreeNode(7), new TreeNode(9)) );
        Assertions.assertEquals( p, lowestCommonAncestor(root, p, q) );
    }

    @Test
    public void testLCA3() {
        TreeNode q = new TreeNode(1);
        TreeNode root = new TreeNode(2, q, null );
        Assertions.assertEquals( root, lowestCommonAncestor(root, root, q) );
    }

    public static void main(String[] args) {
//        TreeNode p = new TreeNode(2, new TreeNode(0), new TreeNode(4, new TreeNode(3), new TreeNode(5)));
//        TreeNode q = new TreeNode(8, new TreeNode(7), new TreeNode(9));
//        TreeNode root = new TreeNode(6, p, q );
//        System.out.println(lowestCommonAncestor(root, root, q));
        TreeNode q = new TreeNode(1);
        TreeNode root = new TreeNode(2, q, null );
        System.out.println(root);
        System.out.println(lowestCommonAncestor(root, root, q));
    }

}
