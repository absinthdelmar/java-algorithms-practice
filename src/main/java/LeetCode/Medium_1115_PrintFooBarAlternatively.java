package LeetCode;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 1115. Print FooBar Alternately
 * Medium
 *
 * Suppose you are given the following code:
 *
 * class FooBar {
 *   public void fooBar() {
 *     for (int i = 0; i < n; i++) {
 *       print("fooBar");
 *     }
 *   }
 *
 *   public void bar() {
 *     for (int i = 0; i < n; i++) {
 *       print("bar");
 *     }
 *   }
 * }
 *
 * The same instance of FooBar will be passed to two different threads.
 * Thread A will call fooBar() while thread B will call bar(). Modify the given program to output "foobar" n times.
 *
 *
 *
 * Example 1:
 *
 * Input: n = 1
 * Output: "foobar"
 * Explanation: There are two threads being fired asynchronously.
 * One of them calls fooBar(), while the other calls bar(). "foobar" is being output 1 time.
 *
 * Example 2:
 *
 * Input: n = 2
 * Output: "foobarfoobar"
 * Explanation: "foobar" is being output 2 times.
 *
 */
public class Medium_1115_PrintFooBarAlternatively {

    // we can only modify FooBar class

    static class FooBar {
        private int n;
        private Semaphore s1 = new Semaphore(1);
        private Semaphore s2 = new Semaphore(0);

        public FooBar(int n) {
            this.n = n;
        }

        public void foo(Runnable printFoo) throws InterruptedException {

            for (int i = 0; i < n; i++) {
                s1.acquire();
                // printFoo.run() outputs "foo". Do not change or remove this line.
                printFoo.run();
                s2.release();
            }
        }

        public void bar(Runnable printBar) throws InterruptedException {

            for (int i = 0; i < n; i++) {
                s2.acquire();
                // printBar.run() outputs "bar". Do not change or remove this line.
                printBar.run();
                s1.release();
            }
        }
    }

    // helper code below this line

    static class T implements Runnable {
        FooBar fooBar;
        String data;

        T(FooBar fooBar, String data) {
            this.fooBar = fooBar;
            this.data = data;
        }

        public void run() {
            try {
                Method method = fooBar.getClass().getMethod(data, Runnable.class);
                method.invoke(fooBar, (Runnable)() -> System.out.print(data));
            }
            catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        FooBar fooBar = new FooBar(3);
//        ExecutorService executorService = Executors.newFixedThreadPool(3);
        ExecutorService executorService = Executors.newWorkStealingPool();
        executorService.submit(new T(fooBar,"foo"));
        executorService.submit(new T(fooBar, "bar"));
        executorService.shutdown();
        if ( !executorService.awaitTermination(5, TimeUnit.SECONDS ) ) {
            System.out.println("Threads didn't finish in 2 seconds!");
        }
    }

}
