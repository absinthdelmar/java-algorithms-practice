package LeetCode;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 136. Single Number
 * Easy
 *
 * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
 *
 * Note:
 *
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 *
 * Example 1:
 *
 * Input: [2,2,1]
 * Output: 1
 * Example 2:
 *
 * Input: [4,1,2,1,2]
 * Output: 4
 *
 */
public class Easy_136_SingleNumber {

    // raw version, very slow, hangs on large input
    static int singleNumber0(int[] nums) {
        List<Integer> list = Arrays.stream(nums).boxed().collect(Collectors.toList());
        return list.stream().filter(i -> Collections.frequency(list, i) == 1).min(Integer::compareTo).orElse(-1);
    }

    // second version, still very slow 431 ms, double n iteration
    static int frequency(int[] nums, int a) {
        int result = 0;
        for (int i = 0; i < nums.length; i++)
            result += nums[i] == a ? 1 : 0;
        return result;
    }

    static int singleNumber1(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int f = frequency(nums, nums[i]);
            System.out.println(nums[i] + " " + f);
            if ( f == 1 )
                return nums[i];
        }
        return -1;
    }

    // 12 ms, still very slow, using HashMap
    static int singleNumber2(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if ( map.containsKey(nums[i]) )
                map.replace(nums[i], map.get(nums[i]) + 1);
            else
                map.put(nums[i], 1);
        }
        System.out.println(map);
        List<Integer> list = map.entrySet().stream().filter(e -> e.getValue() == 1).map(Map.Entry::getKey).collect(Collectors.toList());
        return list.size() == 1 ? list.get(0) : -1;
    }

    // using xor
    // does not work for example for [4,4,1,4]
    // so it depends on what number is last, if it is a duplicate, it will not work
    // and based on the problem description, which says "TWICE", we have to assume we can use XOR to solve it
    static int singleNumber(int[] nums) {
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            result ^= nums[i];
        }
        return result;
    }

    // concept : 2 * sum([a+b+c]) - sum(a+a+b+b+c)=c
    static int singleNumber4(int[] nums) {
        int setSum = Arrays.stream(nums).boxed().collect(Collectors.toSet()).stream().reduce(Integer::sum).orElse(0);
        int sum = Arrays.stream(nums).boxed().reduce(Integer::sum).orElse(0);
        return 2 * setSum - sum;
    }

    public static void main(String[] args) {
        System.out.println(singleNumber(new int[]{ 2,2,1 }));
        System.out.println(singleNumber(new int[]{ 4,1,2,1,2 }));
//        System.out.println(singleNumber(new int[]{ 4,4,1,4 }));
    }

}
