package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * 121. Best Time to Buy and Sell Stock
 * Easy
 *
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 *
 * Note that you cannot sell a stock before you buy one.
 *
 * Example 1:
 *
 * Input: [7,1,5,3,6,4]
 * Output: 5
 * Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
 *              Not 7-1 = 6, as selling price needs to be larger than buying price.
 * Example 2:
 *
 * Input: [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 */
public class Easy_121_BestTimeToBuyAndSellStock {

    // max profit summarized throughout all days
    static int maxProfit0(int[] prices, int start, int end) {
        if ( end <= start )
            return 0;

        int profit = 0;

        // buy day
        for (int i = start; i < end; i++) {

            // sell day
            for (int j = i+1; j <= end; j++) {

                if ( prices[j] > prices[i] ) {
                    // current profit
                    int p = prices[j] - prices[i] +
                            maxProfit0(prices, start, i - 1) +
                            maxProfit0(prices,j + 1, end);

                    // update max profit
                    profit = Math.max(profit, p);
                }

            }

        }

        return profit;
    }

    static int maxProfit0(int[] prices) {
        System.out.println(Arrays.toString(prices));

        if (prices == null || prices.length < 2)
            return 0;

        return maxProfit0(prices, 0, prices.length - 1);
    }

    static class TradeIndex {
        int buy, sell;

        @Override
        public String toString() {
            return "buy at: " + buy + " sell at: " + sell;
        }
    }

    static int maxProfit1(int[] prices) {
        if (prices == null || prices.length < 2)
            return 0;

        List<TradeIndex> trades = new ArrayList<>();

        int n = prices.length;
        int i = 0;

        while ( i < n - 1 ) {
            // min
            while ( ( i < n - 1 ) && prices[i + 1] <= prices[i] )
                i++;

            if ( i == n - 1 )
                break;

            TradeIndex trade = new TradeIndex();
            trade.buy = i++;

            // FIXME
            while ( ( i < n ) && prices[i] >= prices[i - 1] ) {
                System.out.println(prices[i-1] + " vs " + prices[i]);
                i++;
            }

            trade.sell = i - 1;

            trades.add(trade);
        }

        System.out.println(trades);

        int maxProfit = 0;
        for (int j = 0; j < trades.size(); j++) {
            int profit = prices[trades.get(j).sell] - prices[trades.get(j).buy];
            if ( profit > maxProfit )
                maxProfit = profit;
        }

        return maxProfit;
    }

    // TODO change this to single trade only
    static int maxProfit(int[] prices) {
        int best_without_stock = 0, best_with_stock = -Integer.MAX_VALUE;
        for(int x : prices) {
            System.out.println("x = " + x);
            best_with_stock = Math.max(best_with_stock, best_without_stock - x);
            System.out.println("with = " + best_with_stock);
            best_without_stock = Math.max(best_without_stock, best_with_stock + x);
            System.out.println("without = " + best_without_stock);
        }
        return best_without_stock;
    }

    // Day 1 price 7
    // Day 2 price 1 buy
    // Day 3 price 5
    // Day 4 price 3
    // Day 5 price 6 sell
    // Day 6 price 4
    @Test
    public void testMaxProfit() {
        Assertions.assertEquals(5, maxProfit(new int[]{7,1,5,3,6,4}));
    }

    // Day 1 price 7
    // Day 2 price 6
    // Day 3 price 4
    // Day 4 price 3
    // Day 5 price 1
    @Test
    public void testMaxProfit2() {
        Assertions.assertEquals(0, maxProfit(new int[]{7,6,4,3,1}));
    }

    // Day 1 price 3
    // Day 2 price 2
    // Day 3 price 6
    // Day 4 price 5
    // Day 5 price 0
    // Day 6 price 3
    @Test
    public void testMaxProfit3() {
        Assertions.assertEquals(4, maxProfit(new int[]{3,2,6,5,0,3}));
    }

    @Test
    public void testMaxProfit4() {
        Assertions.assertEquals(2, maxProfit(new int[]{2,1,2,1,0,1,2}));
    }

    @Test
    public void testMaxProfit5() {
        Assertions.assertEquals(3, maxProfit(new int[]{4,7,2,1}));
    }

    public static void main(String[] args) {
//        System.out.println(maxProfit(new int[]{7,1,5,3,6,4}));
//        System.out.println(maxProfit(new int[]{7,6,4,3,1}));
        System.out.println(maxProfit(new int[]{3,2,6,5,0,3}));
    }

}
