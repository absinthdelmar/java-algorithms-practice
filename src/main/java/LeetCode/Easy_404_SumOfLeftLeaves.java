package LeetCode;

/**
 * 404. Sum of Left Leaves
 * Easy
 *
 * Find the sum of all left leaves in a given binary tree.
 *
 * Example:
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 *
 * There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.
 */
public class Easy_404_SumOfLeftLeaves {

    static int dfs(TreeNode root) {
        // is empty
        if ( root == null )
            return 0;
        int result = 0;
        // has left
        if ( root.left != null )
            // is a leaf
            if ( root.left.left == null && root.left.right == null )
                result += root.left.val;
            else
                result += dfs(root.left);
        // has right
        if ( root.right != null )
            result += dfs(root.right);
        return result;
    }
    static int sumOfLeftLeaves(TreeNode root) {
        return dfs(root);
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        System.out.println(sumOfLeftLeaves(root));
        root = new TreeNode(1,
                new TreeNode(2, new TreeNode(4), new TreeNode(5)),
                new TreeNode(3));
        System.out.println(sumOfLeftLeaves(root));
    }

}
