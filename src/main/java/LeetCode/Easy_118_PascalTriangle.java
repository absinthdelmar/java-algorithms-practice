package LeetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 118. Pascal's Triangle
 * Easy
 *
 * Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 *
 * Example:
 *
 * Input: 5
 * Output:
 * [
 *      [1],
 *     [1,1],
 *    [1,2,1],
 *   [1,3,3,1],
 *  [1,4,6,4,1]
 * ]
 *
 */
public class Easy_118_PascalTriangle {

    static List<Integer> gen(List<Integer> prev) {
        assert prev.size() >= 2;
        List<Integer> result = new ArrayList<>();
        result.add(1);
        for (int i = 0; i < prev.size() - 1; i++)
            result.add( prev.get(i) + prev.get(i + 1) );
        result.add(1);
        return result;
    }

    // TODO replace spaghetti result.add for 1 and 2 rows with something more sophisticated :)
    static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        if ( numRows <= 0 ) {
            return result;
        }
        if ( numRows == 1 ) {
            result.add(Arrays.asList(1));
        }
        else if ( numRows == 2 ) {
            result.add(Arrays.asList(1));
            result.add(Arrays.asList(1, 1));
        }
        else {  // numRows > 2
            result.add(Arrays.asList(1));
            result.add(Arrays.asList(1, 1));
            for (int i = 1; i < numRows - 1; i++) {
                result.add( gen( result.get(i) ) );
            }
        }
        return result;
    }

    public static void printer(List<List<Integer>> input) {
        for (int i = 0; i < input.size(); i++)
            System.out.println(input.get(i));
    }

    public static void main(String[] args) {
        List<List<Integer>> input1 = new ArrayList<>();
        input1.add(Arrays.asList(1));
        input1.add(Arrays.asList(1,1));
        input1.add(Arrays.asList(1,2,1));
        input1.add(Arrays.asList(1,3,3,1));
        input1.add(Arrays.asList(1,4,6,4,1));
//        printer(input1);
        printer(generate(15));
    }

}
