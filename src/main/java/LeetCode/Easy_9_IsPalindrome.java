package LeetCode;

public class Easy_9_IsPalindrome {

    // solution #1, using String
    static boolean isPalindrome(int x) {
        String s = Integer.toString(x);
        char[] c = s.toCharArray();
        char[] reverse = new char[c.length];
        for ( int i = 0; i < c.length; i++ )
            reverse[i] = c[c.length - i - 1];
        return s.equals(new String(reverse));
    }

    // solution #2, reverse integer
    static boolean isPalindrome2(int x) {
        int reversed = reverse2(x);
        return Integer.compare(reversed, x) == 0;
    }

    // overflow check addition
    static int add(int x, int y) throws ArithmeticException {
        long result = (long)x + (long)y;
        if ( (int)result != result )
            throw new ArithmeticException("integer overflow");
        return (int)result;
    }

    // overflow check multiplication
    static int mult(int x, int y) throws ArithmeticException {
        long result = (long)x * (long)y;
        if ( (int)result != result )
            throw new ArithmeticException("integer overflow");
        return (int)result;
    }

    static int reverse2(int x) {
        try {
            boolean negative = x < 0;

            if ( negative )
                x = mult(x, -1);

            int lastDigit = x % 10, remainder = x / 10, reverse = 0;

            while (remainder > 0) {
                reverse = mult(reverse, 10);
                reverse = add(reverse, lastDigit);

                lastDigit = remainder % 10;
                remainder = remainder / 10;
            }
            reverse = mult(reverse, 10);
            reverse = add(reverse, lastDigit);

            return negative ? -reverse : reverse;
        }
        catch(ArithmeticException e){
            System.out.println(e.getMessage());
            return 0;
        }
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(-121));
        System.out.println(isPalindrome(121));
        System.out.println(isPalindrome(12121));
        System.out.println(isPalindrome(10));
        System.out.println(isPalindrome2(-121));
        System.out.println(isPalindrome2(121));
        System.out.println(isPalindrome2(12121));
        System.out.println(isPalindrome2(10));
    }

}
