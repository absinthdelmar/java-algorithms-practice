package LeetCode;

public class Easy_231_PowerOf2 {

    static boolean isPowerOfTwo(int n) {
        if ( n < 0 )
            return false;
        return (n != 0) && ((n & (n - 1)) == 0);
    }

    public static void main(String[] args) {
        int n = -2147483648;
        System.out.println(isPowerOfTwo(n));
        System.out.println(isPowerOfTwo(1024));
    }
}
