package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 28. Implement strStr()
 * Easy
 *
 * Implement strStr().
 *
 * Return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
 *
 * Example 1:
 *
 * Input: haystack = "hello", needle = "ll"
 * Output: 2
 * Example 2:
 *
 * Input: haystack = "aaaaa", needle = "bba"
 * Output: -1
 * Clarification:
 *
 * What should we return when needle is an empty string? This is a great question to ask during an interview.
 *
 * For the purpose of this problem, we will return 0 when needle is an empty string. This is consistent to C's strstr() and Java's indexOf().
 *
 */
public class Easy_28_StrStr {

    // working!!!! WOW !!!
    static int strStr(String haystack, String needle) {
        if ( needle == null || needle.length() == 0 )
            return 0;

        int i = 0;
        int j = 0;
        int m = 0;

        while ( i < haystack.length() ) {
//            System.out.print(haystack.charAt(i));
            if ( haystack.charAt(i) != needle.charAt(j) ) {
                if ( m > 0 ) {
                    m = 0;
                    j = 0;
                    i--;
//                    System.out.println("reset");
                }
                else {
                    i++;
                }
            }
            else {
//                System.out.println("got match: " + haystack.charAt(i) + "=" + needle.charAt(j));
                m++;
                i++;
                j++;
                if ( m == needle.length() && j == m )
                    return i - m;
            }
        }

        return -1;
    }

    // working!!!! WOW !!! but too slow for leetcode :((
    static int strStr2(String haystack, String needle) {
        if ( needle == null || needle.length() == 0 )
            return 0;

        char[] h = haystack.toCharArray();
        char[] n = needle.toCharArray();

        int i = 0, j = 0;

        while ( i < h.length ) {
            if ( h[i] == n[j] ) {
                i++;
                j++;
                if ( j == n.length )
                    return i - j;
            }
            else {
                if ( j > 0 ) {
                    j = 0;
                    i--;
                }
                else {
                    i++;
                }
            }
        }

        return -1;
    }

    // working, java String.indexOf version, worked through with comments on every action
    static int strStr3(String haystack, String needle) {
        if ( needle == null || needle.length() == 0 )
            return 0;

        char[] h = haystack.toCharArray();
        char[] n = needle.toCharArray();

        // first needle char
        char first = n[0];

        // max haystack pointer position
        int max = h.length - n.length;
        int i = 0;

        // start from haystack position 0 until maximum possible position
        while ( i <= max ) {
            // current haystack pointed char not equal to the first needle char
            if ( h[i] != first ) {
                // continue to increase haystack pointer until max is reached and not equal to the first needle char
                while ( ++i <= max && h[i] != first );
            }
            // current haystack pointer is less or equal to max pointer position
            // matched first needle char
            if ( i <= max ) {
                // next to the current haystack char pointer
                // j is an additional variable to keep i in its place (not to change i)
                int j = i + 1;
                // final position of the haystack char pointer
                int end = j + n.length - 1;

                // k is an additional variable to navigate through needle
                // increase both j and k while j does not come to a haystack end
                // and every haystack char left equals every needle char left
//                for ( int k = 1; j < end && h[j] == n[k]; j++, k++ );
                int k = 1;
                while ( j < end && h[j] == n[k] ) {
                    j++;
                    k++;
                }

                // found the whole match, return current haystack position
                if ( j == end ) {
                    return i;
                }
            }
            i++;
        }

        return -1;
    }

    @Test
    public void testDummy() {

    }

    @Test
    public void testStrStr3() {
        Assertions.assertEquals(1, strStr3("helloworld", "ell"));
        Assertions.assertEquals(-1, strStr3("helloworld", "ellb"));
        Assertions.assertEquals(2, strStr3("hello", "ll"));
        Assertions.assertEquals(-1, strStr3("aaaaa", "bba"));
        Assertions.assertEquals(4, strStr3("hebbohello", "ohe"));
        Assertions.assertEquals(2, strStr3("hellohello", "ll"));
        Assertions.assertEquals(0, strStr3("hellohello", ""));
        Assertions.assertEquals(7, strStr3("hellzohello", "ello"));
        Assertions.assertEquals(4, strStr3("mississippi", "issip"));
    }

    public static void main(String[] args) {
        System.out.println(strStr3("helloworld", "world"));
        System.out.println(strStr3("helloworld", "ell"));
        System.out.println(strStr3("helloworld", "ellb"));
        System.out.println(strStr3("hello", "ll"));
        System.out.println(strStr3("aaaaa", "bba"));
        System.out.println(strStr3("hebbohello", "ohe"));
        System.out.println(strStr3("hellohello", "ll"));
        System.out.println(strStr3("helloohello", ""));
        System.out.println(strStr3("hellzohello", "ello"));
        System.out.println(strStr3("mississippi", "issip"));
    }

}
