package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 122. Best Time to Buy and Sell Stock II
 * Easy
 *
 * 2593
 *
 * 1778
 *
 * Add to List
 *
 * Share
 * Say you have an array prices for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete as many transactions as you like
 * (i.e., buy one and sell one share of the stock multiple times).
 *
 * Note: You may not engage in multiple transactions at the same time (i.e., you must sell the stock
 * before you buy again).
 *
 * Example 1:
 *
 * Input: [7,1,5,3,6,4]
 * Output: 7
 * Explanation: Buy on day 2 (price = 1) and sell on day 3 (price = 5), profit = 5-1 = 4.
 *              Then buy on day 4 (price = 3) and sell on day 5 (price = 6), profit = 6-3 = 3.
 * Example 2:
 *
 * Input: [1,2,3,4,5]
 * Output: 4
 * Explanation: Buy on day 1 (price = 1) and sell on day 5 (price = 5), profit = 5-1 = 4.
 *              Note that you cannot buy on day 1, buy on day 2 and sell them later, as you are
 *              engaging multiple transactions at the same time. You must sell before buying again.
 * Example 3:
 *
 * Input: [7,6,4,3,1]
 * Output: 0
 * Explanation: In this case, no transaction is done, i.e. max profit = 0.
 *
 *
 * Constraints:
 *
 * 1 <= prices.length <= 3 * 10 ^ 4
 * 0 <= prices[i] <= 10 ^ 4
 */
public class Easy_122_BestTimeToBuyAndSellStock2 {

    static int maxProfit(int[] prices) {
        int best_without_stock = 0, best_with_stock = -Integer.MAX_VALUE;
        for(int x : prices) {
            System.out.println("x = " + x);
            best_with_stock = Math.max(best_with_stock, best_without_stock - x);
            System.out.println("with = " + best_with_stock);
            best_without_stock = Math.max(best_without_stock, best_with_stock + x);
            System.out.println("without = " + best_without_stock);
        }
        return best_without_stock;
    }

    // Day 1 price 7
    // Day 2 price 1 buy
    // Day 3 price 5
    // Day 4 price 3
    // Day 5 price 6 sell
    // Day 6 price 4
    @Test
    public void testMaxProfit() {
        Assertions.assertEquals(7, maxProfit(new int[]{7,1,5,3,6,4}));
    }

    // Day 1 price 7
    // Day 2 price 6
    // Day 3 price 4
    // Day 4 price 3
    // Day 5 price 1
    @Test
    public void testMaxProfit2() {
        Assertions.assertEquals(0, maxProfit(new int[]{7,6,4,3,1}));
    }

    // Day 1 price 3
    // Day 2 price 2
    // Day 3 price 6
    // Day 4 price 5
    // Day 5 price 0
    // Day 6 price 3
    @Test
    public void testMaxProfit3() {
        Assertions.assertEquals(4, maxProfit(new int[]{3,2,6,5,0,3}));
    }

    @Test
    public void testMaxProfit4() {
        Assertions.assertEquals(2, maxProfit(new int[]{2,1,2,1,0,1,2}));
    }

    @Test
    public void testMaxProfit5() {
        Assertions.assertEquals(3, maxProfit(new int[]{4,7,2,1}));
    }

    @Test
    public void testMaxProfit6() {
        Assertions.assertEquals(4, maxProfit(new int[]{1,2,3,4,5}));
    }

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7,1,5,3,6,4}));
//        System.out.println(maxProfit(new int[]{7,6,4,3,1}));
//        System.out.println(maxProfit(new int[]{3,2,6,5,0,3}));
    }

}
