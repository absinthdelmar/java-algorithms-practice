package LeetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 257. Binary Tree Paths
 * Easy
 *
 * Given a binary tree, return all root-to-leaf paths.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Input:
 *
 *    1
 *  /   \
 * 2     3
 *  \
 *   5
 *
 * Output: ["1->2->5", "1->3"]
 *
 * Explanation: All root-to-leaf paths are: 1->2->5, 1->3
 */
public class Easy_256_BinaryTreePaths {

    static void dfs(TreeNode root, List<String> list, String s) {
        if ( root != null ) {
            s += root.val;
            // add to resulting list if leaf
            if (root.left == null && root.right == null)
                list.add(s);
            else {
                s += "->";
                dfs(root.left, list, s);
                dfs(root.right, list, s);
            }
        }
    }

    static List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<>();
        dfs(root, result, "");
        return result;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1, new TreeNode(2, null, new TreeNode(5)), new TreeNode(3));
        System.out.println(binaryTreePaths(root));
    }

}
