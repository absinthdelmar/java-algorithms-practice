package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * 119. Pascal's Triangle II
 * Easy
 *
 * Given a non-negative index k where k ≤ 33, return the kth index row of the Pascal's triangle.
 *
 * Note that the row index starts from 0.
 *
 *
 * In Pascal's triangle, each number is the sum of the two numbers directly above it.
 *
 * Example:
 *
 * Input: 3
 * Output: [1,3,3,1]
 * Follow up:
 *
 * Could you optimize your algorithm to use only O(k) extra space?
 *
 */
public class Easy_119_PascalTriangle2 {

    static List<Integer> gen(List<Integer> prev) {
        List<Integer> result = new ArrayList<>();
        result.add(1);
        for (int i = 0; i < prev.size() - 1; i++)
            result.add( prev.get(i) + prev.get(i + 1) );
        result.add(1);
        return result;
    }

    // my version 1 ms
    static List<Integer> getRow0(int rowIndex) {
        List<List<Integer>> result = new ArrayList<>();
        result.add(Collections.singletonList(1));
        result.add(Arrays.asList(1, 1));
        if ( rowIndex == 0 || rowIndex == 1 )
            return result.get(rowIndex);
        if ( rowIndex > 1 )
            for (int i = 1; i < rowIndex; i++)
                result.add( gen( result.get(i) ) );
        return result.get(rowIndex);
    }

    // crazy 0ms version from the website
    static List<Integer> getRow(int rowIndex) {
        List<Integer> result = new ArrayList<>();
        result.add(1);
        for (int i = 1; i <= rowIndex; i++) {
            int a = result.get(result.size() - 1);
            int b = rowIndex - i + 1;
            int c = (int)((a * (long)b) / i);
            result.add(c);
        }
        return result;
    }

    @Test
    public void testGetRow0() {
        Assertions.assertEquals( Arrays.asList(1), getRow(0) );
    }

    @Test
    public void testGetRow1() {
        Assertions.assertEquals( Arrays.asList(1, 1), getRow(1) );
    }

    @Test
    public void testGetRow2() {
        Assertions.assertEquals( Arrays.asList(1, 2, 1), getRow(2) );
    }

    @Test
    public void testGetRow3() {
        Assertions.assertEquals( Arrays.asList(1, 3, 3, 1), getRow(3) );
    }

    @Test
    public void testGetRow14() {
        Assertions.assertEquals(
            Arrays.asList(1, 14, 91, 364, 1001, 2002, 3003, 3432, 3003, 2002, 1001, 364, 91, 14, 1),
            getRow(14)
        );
    }

    public static void printer(List<List<Integer>> input) {
        for (int i = 0; i < input.size(); i++)
            System.out.println(input.get(i));
    }

    public static void main(String[] args) {
        System.out.println(getRow(14));
    }

}
