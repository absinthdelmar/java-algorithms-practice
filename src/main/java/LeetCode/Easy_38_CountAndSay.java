package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 38. Count and Say
 * Easy
 *
 * The count-and-say sequence is the sequence of integers with the first five terms as following:
 *
 * 1.     1
 * 2.     11
 * 3.     21
 * 4.     1211
 * 5.     111221
 * 1 is read off as "one 1" or 11.
 * 11 is read off as "two 1s" or 21.
 * 21 is read off as "one 2, then one 1" or 1211.
 *
 * Given an integer n where 1 ≤ n ≤ 30, generate the nth term of the count-and-say sequence.
 * You can do so recursively, in other words from the previous member read off the digits,
 * counting the number of digits in groups of the same digit.
 *
 * Note: Each term of the sequence of integers will be represented as a string.
 *
 * Example 1:
 *
 * Input: 1
 * Output: "1"
 * Explanation: This is the base case.
 * Example 2:
 *
 * Input: 4
 * Output: "1211"
 * Explanation: For n = 3 the term was "21" in which we have two groups "2" and "1", "2" can be
 * read as "12" which means frequency = 1 and value = 2, the same way "1" is read as "11",
 * so the answer is the concatenation of "12" and "11" which is "1211".
 */
public class Easy_38_CountAndSay {

    // looks like working
    static String countAndSay(int n) {
        if ( n == 1 )
            return "1";
        // repeat with decreasing n
        return next(countAndSay(n-1));
    }

    static String next(String prev) {
        StringBuilder result = new StringBuilder();
        // initial counter
        int count = 1;
        // go through the previous string
        for ( int i = 0; i < prev.length(); i++ ) {
            // range check to pick current and next character
            // increase counter if they are available and the same
            // proceed to the next character in previous string
            if ( i + 1 < prev.length() && prev.charAt(i) == prev.charAt(i+1) ) {
                count++;
                continue;
            }
            // append counter and current char to result
            result.append(count).append(prev.charAt(i));
            // reset counter
            count = 1;
        }
        return result.toString();
    }

    @Test
    public void testCountAndSay() {
        Assertions.assertEquals("1", countAndSay(1));
        Assertions.assertEquals("11", countAndSay(2));
        Assertions.assertEquals("21", countAndSay(3));
        Assertions.assertEquals("1211", countAndSay(4));
        Assertions.assertEquals("111221", countAndSay(5));
        Assertions.assertEquals("312211", countAndSay(6));
        Assertions.assertEquals("13112221", countAndSay(7));
        Assertions.assertEquals("1113213211", countAndSay(8));
        Assertions.assertEquals("31131211131221", countAndSay(9));
        Assertions.assertEquals("13211311123113112211", countAndSay(10));
    }

    public static void main(String[] args) {
        for ( int n = 1; n <= 10; n++ )
            System.out.println(countAndSay(n));
    }

}
