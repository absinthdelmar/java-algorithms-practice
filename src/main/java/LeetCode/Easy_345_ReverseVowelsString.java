package LeetCode;

/**
 * 345. Reverse Vowels of a String
 * Easy
 *
 * Write a function that takes a string as input and reverse only the vowels of a string.
 *
 * Example 1:
 *
 * Input: "hello"
 * Output: "holle"
 * Example 2:
 *
 * Input: "leetcode"
 * Output: "leotcede"
 * Note:
 * The vowels does not include the letter "y".
 */
public class Easy_345_ReverseVowelsString {

    static boolean isVowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' ||
               c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U';
    }

    // 2 ms
    static String reverseVowels(String str) {
        int n = str.length();
        char[] s = str.toCharArray();
        int i = 0, j = n - 1;
        while ( i < j ) {
            if ( !isVowel(s[j]) ) {
                j--;
                continue;
            }
            else if ( !isVowel(s[i]) ) {
                i++;
                continue;
            }
            char c = s[i];
            s[i] = s[j];
            s[j] = c;
//            System.out.println(s[i] + " vs " + s[j]);
            i++;
            j--;
        }
        return String.valueOf(s);
    }

    public static void main(String[] args) {
        System.out.println(reverseVowels("hello"));
        System.out.println(reverseVowels("leetcode"));
        System.out.println(reverseVowels("aA"));
    }

}
