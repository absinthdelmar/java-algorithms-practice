package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Easy_67_AddBinary {

    static String padLeftZeros(String input, int len) {
        if ( input.length() >= len )
            return input;
        StringBuilder sb = new StringBuilder();
        while ( sb.length() < len - input.length() )
            sb.append("0");
        sb.append(input);
        return sb.toString();
    }

    // working version, but very slow
    static String addBinary(String a, String b) {
        StringBuilder result = new StringBuilder();

        int len = Math.max(a.length(), b.length());
        a = padLeftZeros(a, len);
        b = padLeftZeros(b, len);

        int i = len;
        boolean carry = false;
        while ( --i >= 0 ) {
            String sum;
            if (a.charAt(i) == '1' && b.charAt(i) == '1') {
                if ( carry ) {
                    sum = "1";
                }
                else {
                    sum = "0";
                    carry = true;
                }
            }
            else if ( a.charAt(i) == '0' && b.charAt(i) == '0' ) {
                if ( carry ) {
                    sum = "1";
                    carry = false;
                }
                else {
                    sum = "0";
                }
            }
            else {
                sum = carry ? "0" : "1";
            }
            System.out.println(a.charAt(i) + "+" + b.charAt(i) + "=" + sum + " (" + carry + ")");
            result.insert(0, sum);
            System.out.println(result);
        }

        if ( carry )
            result.insert(0, "1");

        return result.toString();
    }

    @Test
    public void testAddBinary() {
        Assertions.assertEquals("100", addBinary("11", "1"));
        Assertions.assertEquals("10101", addBinary("1010", "1011"));
        Assertions.assertEquals("11110", addBinary("1111", "1111"));
        Assertions.assertEquals(
            "110111101100010011000101110110100000011101000101011001000011011000001100011110011010010011000000000",
            addBinary(
                "10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101",
                "110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"
            )
        );
    }

    public static void main(String[] args) {
//        System.out.println(addBinary("11", "1"));
//        System.out.println(addBinary("1010", "1011"));
//        System.out.println(addBinary("1111", "1111"));
        System.out.println(addBinary(
            "10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101",
            "110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"
        ));
    }

}
