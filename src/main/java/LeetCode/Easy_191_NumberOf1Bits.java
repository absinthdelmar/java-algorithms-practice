package LeetCode;

/**
 * 191. Number of 1 Bits
 * Easy
 *
 * Write a function that takes an unsigned integer and return the number of '1' bits it has
 * (also known as the Hamming weight).
 *
 *
 *
 * Example 1:
 *
 * Input: 00000000000000000000000000001011
 * Output: 3
 * Explanation: The input binary string 00000000000000000000000000001011 has a total of three '1' bits.
 * Example 2:
 *
 * Input: 00000000000000000000000010000000
 * Output: 1
 * Explanation: The input binary string 00000000000000000000000010000000 has a total of one '1' bit.
 * Example 3:
 *
 * Input: 11111111111111111111111111111101
 * Output: 31
 * Explanation: The input binary string 11111111111111111111111111111101 has a total of thirty one '1' bits.
 *
 *
 * Note:
 *
 * Note that in some languages such as Java, there is no unsigned integer type. In this case, the input will be given
 * as signed integer type and should not affect your implementation, as the internal binary representation of the
 * integer is the same whether it is signed or unsigned.
 *
 * In Java, the compiler represents the signed integers using 2's complement notation.
 * Therefore, in Example 3 above the input represents the signed integer -3.
 */
public class Easy_191_NumberOf1Bits {

    // you need to treat n as an unsigned value
    static int hammingWeight0(int n) {
        // technique here is to put a count of each part of bits into this part
        n = ( n & 0x5555_5555 ) + ( ( n >> 1 ) & 0x5555_5555 );    // 2 bits part
        n = ( n & 0x3333_3333 ) + ( ( n >> 2 ) & 0x3333_3333 );    // 4 bits part
        n = ( n & 0x0f0f_0f0f ) + ( ( n >> 4 ) & 0x0f0f_0f0f );    // 8 bits part
        n = ( n & 0x00ff_00ff ) + ( ( n >> 8 ) & 0x00ff_00ff );    // 16 bits part
        n = ( n & 0x0000_ffff ) + ( ( n >> 16 ) & 0x0000_ffff );    // 32 bits part
        return n;
    }

    // basically the same meaning as above, but done using iteration
    // with check and mask increase on each step: 1, 2, 4, 8, 16, 32, ... , 2147483648
    static int hammingWeight(int n) {
        int count = 0;
        int mask = 1;
        for (int i = 0; i < 32; i++) {
            // increase counter when (n & mask) != 0 using ternary operator
            count += (n & mask) != 0 ? 1 : 0;
            // multiply mask by 2
            mask <<= 1;
            System.out.println(signedIntToUnsignedLong(mask));
        }
        return count;
    }

    // helper function to check results
    static long signedIntToUnsignedLong(int x) {
        return ((long)x) & 0xFFFF_FFFFL;
    }

    // helper function to check results
    static int unsignedLongToSignedInt(long x) {
        return (int)(( x << 1 ) >> 1);
    }

    public static void main(String[] args) {
        // 00000000000000000000000000001011
        System.out.println(hammingWeight(11));
        // 00000000000000000000000010000000
        System.out.println(hammingWeight(128));
        // 11111111111111111111111111111101
        System.out.println(hammingWeight(unsignedLongToSignedInt(4294967293L)));
    }

}
