package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 171. Excel Sheet Column Number
 * Easy
 *
 * Given a column title as appear in an Excel sheet, return its corresponding column number.
 *
 * For example:
 *
 *     A -> 1
 *     B -> 2
 *     C -> 3
 *     ...
 *     Z -> 26
 *     AA -> 27
 *     AB -> 28
 *     ...
 * Example 1:
 *
 * Input: "A"
 * Output: 1
 * Example 2:
 *
 * Input: "AB"
 * Output: 28
 * Example 3:
 *
 * Input: "ZY"
 * Output: 701
 *
 *
 * Constraints:
 *
 * 1 <= s.length <= 7
 * s consists only of uppercase English letters.
 * s is between "A" and "FXSHRXW".
 */
public class Easy_171_ExcelSheetColumnNumber {

    static String convertToTitle(int n) {
        StringBuilder result = new StringBuilder();
        while ( n > 0 ) {
            int remainder = n % 26;
            // just add Z
            // 90 is the char code for Z
            if (remainder % 26 == 0) {
                result.append((char) 90);
                n = n / 26 - 1;
            // add other letters
            // 65 is the char code for A
            } else {
                result.append((char) ((remainder - 1) + 65));
                n /= 26;
            }
        }
        return result.reverse().toString();
    }

    static int titleToNumber(String s) {
        if ( s == null || s.length() == 0 || s.length() > 7 )
            return 0;

        int result = 0;

        for (int i = 0; i < s.length(); i++)
            result = result * 26 + s.charAt(i) - 65 + 1;

        return result;
    }

    @Test
    public void testTitleToNumber() {
        Assertions.assertEquals(28, titleToNumber("AB"));
        Assertions.assertEquals(701, titleToNumber("ZY"));
        Assertions.assertEquals(7000, titleToNumber("JIF"));
    }

    public static void main(String[] args) {
//        System.out.println((int)'A');   // 65
//        System.out.println((int)'Z');   // 90
//        System.out.println(convertToTitle(27));
//        System.out.println(convertToTitle(52));
//        System.out.println(convertToTitle(53));
//        System.out.println(convertToTitle(700));
//        System.out.println(convertToTitle(701));
//        System.out.println(convertToTitle(7000));
//        System.out.println(titleToNumber("A"));
//        System.out.println(titleToNumber("AB"));
//        System.out.println(titleToNumber("ZY"));
        System.out.println(titleToNumber("JIFJIF"));
    }

}
