package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 104. Maximum Depth of Binary Tree
 * Easy
 *
 * Given a binary tree, find its maximum depth.
 *
 * The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Given binary tree [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * return its depth = 3.
 *
 */
public class Easy_104_MaxDepthOfBinaryTree {

    static int calculateDepth(TreeNode root, int depth) {
        if ( root == null )
            return depth;
        depth++;
        int ld = calculateDepth(root.left, depth);
        int rd = calculateDepth(root.right, depth);
        return ld > rd ? ld : rd;
    }

    static int maxDepth(TreeNode root) {
        if ( root == null )
            return 0;
        if ( root.left == null && root.right == null )
            return 1;
        return calculateDepth(root, 0);
    }

    @Test
    public void testMaxDepth() {
        TreeNode treeNode1 = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        Assertions.assertEquals(3, maxDepth(treeNode1));
    }

    @Test
    public void testMaxDepth2() {
        TreeNode treeNode1 = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20));
        Assertions.assertEquals(2, maxDepth(treeNode1));
    }

    public static void main(String[] args) {
        TreeNode treeNode1 = new TreeNode(3,
                new TreeNode(9),
                new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        System.out.println(maxDepth(treeNode1));
    }

}
