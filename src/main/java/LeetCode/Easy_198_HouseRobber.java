package LeetCode;

import java.util.ArrayList;
import java.util.List;

/**
 * 198. House Robber
 * Easy
 *
 * You are a professional robber planning to rob houses along a street. Each house has a certain amount of
 * money stashed, the only constraint stopping you from robbing each of them is that adjacent houses have
 * security system connected and it will automatically contact the police if two adjacent houses were broken
 * into on the same night.
 *
 * Given a list of non-negative integers representing the amount of money of each house, determine the maximum
 * amount of money you can rob tonight without alerting the police.
 *
 *
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,1]
 * Output: 4
 * Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
 *              Total amount you can rob = 1 + 3 = 4.
 * Example 2:
 *
 * Input: nums = [2,7,9,3,1]
 * Output: 12
 * Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
 *              Total amount you can rob = 2 + 9 + 1 = 12.
 *
 *
 * Constraints:
 *
 * 0 <= nums.length <= 100
 * 0 <= nums[i] <= 400
 */
public class Easy_198_HouseRobber {

    static int rob(int[] nums) {
        // no houses, no money, just return 0
        if ( nums == null || nums.length == 0 )
            return 0;
        // just one house, return its stash value
        if ( nums.length == 1 )
            return nums[0];
        List<Integer> dp = new ArrayList<>();
        // first stash value
        dp.add(0, nums[0]);
        // second is the max between the first and the second
        dp.add(1, Math.max(nums[0], nums[1]));
        // every next is the max between: the current + one before previous AND the previous
        for (int i = 2; i < nums.length; i++) {
            dp.add( i, Math.max( nums[i] + dp.get( i - 2 ), dp.get(i - 1) ) );
        }
        // return the last one, being the max
        return dp.get(nums.length - 1);
    }

    public static void main(String[] args) {
        System.out.println(rob(new int[]{1,2,3,1}));        // 4 = 1 + 3
        System.out.println(rob(new int[]{2,7,9,3,1}));      // 12 = 2 + 9 + 1
        System.out.println(rob(new int[]{2,1,1,2}));        // 4 = 2 + 2
    }
}
