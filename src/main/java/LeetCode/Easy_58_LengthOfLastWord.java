package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 58. Length of Last Word
 * Easy
 *
 * Given a string s consists of upper/lower-case alphabets and empty space characters ' ',
 * return the length of last word (last word means the last appearing word if we loop from left to right) in the string.
 *
 * If the last word does not exist, return 0.
 *
 * Note: A word is defined as a maximal substring consisting of non-space characters only.
 *
 * Example:
 *
 * Input: "Hello World"
 * Output: 5
 *
 */
public class Easy_58_LengthOfLastWord {

    // funny
    static int lengthOfLastWord0(String s) {
        if ( s == null || s.length() == 0 )
            return 0;
        String[] words = s.split(" ");
        return words.length > 0 ? words[words.length-1].length() : 0;
    }

    static int lengthOfLastWord(String s) {
        if ( s == null || s.length() == 0 )
            return 0;
        List<String> words = new ArrayList<>();
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
//            System.out.println(c);
            if ( c != ' ' ) {
                word.append(c);
                System.out.println(word);
                if ( i == s.length() - 1 )
                    words.add(word.toString());
            }
            else {
                if ( word.toString().trim().length() > 0 ) {
                    System.out.println(word);
                    words.add(word.toString());
                    word = new StringBuilder();
                }
            }
        }
        System.out.println(words);
        return words.size() > 0 ? words.get(words.size()-1).length() : 0;
    }

    @Test
    public void testLengthOfLastWord() {
        Assertions.assertEquals(5, lengthOfLastWord("Hello World"));
        Assertions.assertEquals(7, lengthOfLastWord("Hello World Ausweis"));
        Assertions.assertEquals(1, lengthOfLastWord("b   a    "));
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLastWord("Hello World"));
        System.out.println(lengthOfLastWord("b   a    "));
    }

}
