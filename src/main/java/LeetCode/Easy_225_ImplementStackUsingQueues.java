package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 225. Implement Stack using Queues
 * Easy
 *
 * Implement the following operations of a stack using queues.
 *
 * push(x) -- Push element x onto stack.
 * pop() -- Removes the element on top of the stack.
 * top() -- Get the top element.
 * empty() -- Return whether the stack is empty.
 * Example:
 *
 * MyStack stack = new MyStack();
 *
 * stack.push(1);
 * stack.push(2);
 * stack.top();   // returns 2
 * stack.pop();   // returns 2
 * stack.empty(); // returns false
 * Notes:
 *
 * You must use only standard operations of a queue -- which means only push to back, peek/pop from front, size,
 * and is empty operations are valid.
 *
 * Depending on your language, queue may not be supported natively. You may simulate a queue by using a list or
 * deque (double-ended queue), as long as you use only standard operations of a queue.
 *
 * You may assume that all operations are valid (for example, no pop or top operations will be called on an empty stack).
 */
public class Easy_225_ImplementStackUsingQueues {

    // ugly using a copy
    static class MyStack0 {

        Queue<Integer> queue = new LinkedList<>();

        /** Initialize your data structure here. */
        public MyStack0() {

        }

        /** Push element x onto stack. */
        public void push(int x) {
            queue.add(x);
        }

        /** Removes the element on top of the stack and returns that element. */
        public int pop() {
            Queue<Integer> copy = new LinkedList<>(queue);
            queue.clear();
            Integer value = null;
            while ( !copy.isEmpty() ) {
                value = copy.poll();
                if ( !copy.isEmpty() ) {
                    queue.offer(value);
                }
            }
            return value;
        }

        /** Get the top element. */
        public int top() {
            Queue<Integer> copy = new LinkedList<>(queue);
            Integer value = null;
            while ( !copy.isEmpty() )
                value = copy.poll();
            return value;
        }

        /** Returns whether the stack is empty. */
        public boolean empty() {
            return queue.isEmpty();
        }

        @Override
        public String toString() {
            return queue.toString();
        }
    }

    // same queue with reinsert
    static class MyStack {

        Queue<Integer> queue = new LinkedList<>();

        /** Initialize your data structure here. */
        public MyStack() {

        }

        /** Push element x onto stack. */
        public void push(int x) {
            queue.offer(x);
            int size = queue.size();
            while ( size > 1 ) {
                queue.offer(queue.remove());
                size--;
            }
        }

        /** Removes the element on top of the stack and returns that element. */
        public int pop() {
            return queue.remove();
        }

        /** Get the top element. */
        public int top() {
            return queue.peek();
        }

        /** Returns whether the stack is empty. */
        public boolean empty() {
            return queue.isEmpty();
        }

        @Override
        public String toString() {
            return queue.toString();
        }
    }

    @Test
    public void testStack() {
        MyStack obj = new MyStack();
        obj.push(1);
        obj.push(2);
        Assertions.assertEquals(2, obj.top());
        Assertions.assertEquals(2, obj.pop());
        Assertions.assertFalse(obj.empty());
    }

    public static void main(String[] args) {
        MyStack obj = new MyStack();
        obj.push(1);
        obj.push(2);
        System.out.println(obj);
        System.out.println(obj.top());    // returns 2
        System.out.println(obj.pop());    // returns 2
        System.out.println(obj);
        System.out.println(obj.empty());  // returns false
    }

}
