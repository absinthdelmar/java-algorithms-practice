package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * 168. Excel Sheet Column Title
 * Easy
 *
 * Given a positive integer, return its corresponding column title as appear in an Excel sheet.
 *
 * For example:
 *
 *     1 -> A
 *     2 -> B
 *     3 -> C
 *     ...
 *     26 -> Z
 *     27 -> AA
 *     28 -> AB
 *     ...
 * Example 1:
 *
 * Input: 1
 * Output: "A"
 * Example 2:
 *
 * Input: 28
 * Output: "AB"
 * Example 3:
 *
 * Input: 701
 * Output: "ZY"
 */
public class Easy_168_ExcelSheetColumnTitle {

    static String convertToTitle(int n) {
        StringBuilder result = new StringBuilder();
        while ( n > 0 ) {
            int remainder = n % 26;
            // just add Z
            // 90 is the char code for Z
            if (remainder % 26 == 0) {
                result.append((char) 90);
                n = n / 26 - 1;
            // add other letters
            // 65 is the char code for A
            } else {
                result.append((char) ((remainder - 1) + 65));
                n /= 26;
            }
        }
        return result.reverse().toString();
    }

    @Test
    public void testTitleToNumber() {
        Assertions.assertEquals("AB", convertToTitle(28));
        Assertions.assertEquals("ZY", convertToTitle(701));
        Assertions.assertEquals("JIF", convertToTitle(7000));
    }


    public static void main(String[] args) {
//        System.out.println((int)'A');   // 65
//        System.out.println((int)'Z');   // 90
        System.out.println(convertToTitle(27));
        System.out.println(convertToTitle(52));
        System.out.println(convertToTitle(53));
        System.out.println(convertToTitle(700));
        System.out.println(convertToTitle(7000));
    }

}
