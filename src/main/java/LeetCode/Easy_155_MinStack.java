package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

/**
 * 155. Min Stack
 * Easy
 *
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.
 *
 * push(x) -- Push element x onto stack.
 * pop() -- Removes the element on top of the stack.
 * top() -- Get the top element.
 * getMin() -- Retrieve the minimum element in the stack.
 *
 *
 * Example 1:
 *
 * Input
 * ["MinStack","push","push","push","getMin","pop","top","getMin"]
 * [[],[-2],[0],[-3],[],[],[],[]]
 *
 * Output
 * [null,null,null,null,-3,null,0,-2]
 *
 * Explanation
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.getMin(); // return -3
 * minStack.pop();
 * minStack.top();    // return 0
 * minStack.getMin(); // return -2
 *
 *
 * Constraints:
 *
 * Methods pop, top and getMin operations will always be called on non-empty stacks.
 *
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
public class Easy_155_MinStack {

    // my initial version - 87 ms very very slow!
    static class MinStack0 {

        int[] elements;
        int elementsCount = 0;

        int defaultCapacity = 10;
        float fillFactor = 0.75f;

        /** initialize your data structure here. */
        public MinStack0() {
            elements = new int[defaultCapacity];
        }

        void grow() {
            int oldCapacity = elements.length;
            int newCapacity = oldCapacity + (oldCapacity >> 1);
//            System.out.println(newCapacity);
            elements = Arrays.copyOf(elements, newCapacity);
        }

        public void push(int x) {
            // 6 >= 6 * 0.75
            System.out.println(elementsCount + " >= " + elements.length * fillFactor);
            if ( elementsCount >= elements.length * fillFactor )
                grow();
            elements[elementsCount] = x;
            elementsCount++;
        }

        @Override
        public String toString() {
            return Arrays.toString(elements);
        }

        public void pop() {
            if ( elementsCount > 0 ) {
                int removed = elements[--elementsCount];
                System.out.println("removed " + removed);
            }
        }

        public int top() {
            return elements[elementsCount - 1];
        }

        public int getMin() {
            int[] copy = Arrays.copyOf( elements, elementsCount );
            Arrays.sort(copy);
            System.out.println(Arrays.toString(copy));
            return copy[0];
        }
    }

    // very fast version using LinkedList (ListNode, ListNode.next)

    static class ListNode {
        int val;
        int minValue;
        ListNode next;
        ListNode(int val, int minValue) {
            this.val = val;
            this.minValue = minValue;
        }
    }

    static class MinStack {

        private ListNode head;
        private int minValue = Integer.MAX_VALUE;

        MinStack() {

        }

        public void push(int x) {
            // choose current min
            minValue = Math.min(minValue, x);
            // create new item
            ListNode node = new ListNode(x, minValue);
            // add to the top
            node.next = head;
            head = node;
        }

        public void pop() {
            // remove element
            head = head.next;
            // end of list, update minValue
            if ( head == null )
                minValue = Integer.MAX_VALUE;
            else
                minValue = head.minValue;
        }

        public int top() {
            return head.val;
        }

        public int getMin() {
            return head.minValue;
        }
    }

    @Test
    public void testMinStack() {
        MinStack stack = new MinStack();
        stack.push(3);
        stack.push(2);
        stack.push(4);
        stack.push(6);
        stack.push(5);
        stack.push(1);
        stack.push(7);
        stack.pop();        // remove 7
        Assertions.assertEquals(1, stack.top());
        Assertions.assertEquals(1, stack.getMin());
    }

    @Test
    public void testMinStack2() {
        MinStack stack = new MinStack();
        stack.push(-2);
        stack.push(0);
        stack.push(-3);
        Assertions.assertEquals(-3, stack.getMin());
        stack.pop();        // remove -3
        System.out.println(stack.top());
        Assertions.assertEquals(0, stack.top());
        Assertions.assertEquals(-2, stack.getMin());
    }

    public static void main(String[] args) {
        Random random = new Random();
        MinStack stack = new MinStack();
        for (int i = 0; i < 20; i++) {
            stack.push(random.nextInt(50) + 25);
        }
        System.out.println(stack);
        stack.pop();    // delete top
        System.out.println("top: " + stack.top());
        System.out.println("min: " + stack.getMin());
    }

}
