package LeetCode;

import java.util.Stack;

/**
 * 405. Convert a Number to Hexadecimal
 * Easy
 *
 * Given an integer, write an algorithm to convert it to hexadecimal. For negative integer, two’s complement method is used.
 *
 * Note:
 *
 * - All letters in hexadecimal (a-f) must be in lowercase.
 * - The hexadecimal string must not contain extra leading 0s.
 * If the number is zero, it is represented by a single zero character '0'; otherwise, the first character in the
 * hexadecimal string will not be the zero character.
 * - The given number is guaranteed to fit within the range of a 32-bit signed integer.
 * - You must not use any method provided by the library which converts/formats the number to hex directly.
 *
 * Example 1:
 *
 * Input:
 * 26
 *
 * Output:
 * "1a"
 * Example 2:
 *
 * Input:
 * -1
 *
 * Output:
 * "ffffffff"
 */
public class Easy_405_ToHex {

    static String toHex(int num) {
        if ( num == 0 )
            return "0";

        char[] c = {
            '0','1','2','3','4','5','6','7','8','9',
            'a','b','c','d','e','f'
        };

        StringBuilder result = new StringBuilder();

        while ( num != 0 ) {
            // remainder ( num & 15 or num & 0xf or num & 0b1111 )
            // append it converted to hex using table above
            result.append(c[num & 0b1111]);
            // divide by 16 (signed right shift 4 bits)
            num >>>= 4;
        }

        return result.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(26 >>> 4);
        System.out.println(toHex(26));      // 1a
        System.out.println(toHex(333));     // 14d
        System.out.println(toHex(3333));    // d05
        System.out.println(toHex(-1));      // ffffffff
    }

}
