package LeetCode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 202. Happy Number
 * Easy
 *
 * Write an algorithm to determine if a number n is "happy".
 *
 * A happy number is a number defined by the following process: Starting with any positive integer,
 * replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1
 * (where it will stay), or it loops endlessly in a cycle which does not include 1.
 * Those numbers for which this process ends in 1 are happy numbers.
 *
 * Return True if n is a happy number, and False if not.
 *
 * Example:
 *
 * Input: 19
 * Output: true
 * Explanation:
 * 1**2 + 9**2 = 82
 * 8**2 + 2**2 = 68
 * 6**2 + 8**2 = 100
 * 1**2 + 0**2 + 0**2 = 1
 */
public class Easy_202_HappyNumber {

    static List<Integer> convert0(int n) {
        List<Integer> result = new ArrayList<>();
        while (n > 0) {
            result.add( n % 10 );
            n /= 10;
        }
        return result;
    }

    // very slow 5 ms
    static boolean isHappy0(int n) {
        Set<Integer> numbers = new HashSet<>();
        List<Integer> digits = convert0(n);
        while ( n != 1 || digits.get(0) != 1 ) {
            if ( !numbers.add(n) )
                return false;
            digits = convert0(n);
            n = digits.stream().map(i -> i * i).reduce(Integer::sum).orElse(n);
            System.out.println("n = " + n + " digit[0] = " + digits.get(0));
        }
        return true;
    }

    static int sumOfSquares(int n) {
        int sum = 0;
        while (n > 0) {
            sum += (n % 10) * (n % 10);
            n /= 10;
        }
        return sum;
    }

    // fast 1 ms
    static boolean isHappy1(int n) {
        if ( n == 1 )
            return true;
        Set<Integer> numbers = new HashSet<>();
        while ( n != 1 ) {
            // number already exists
            if ( !numbers.add(n) )
                return false;
            // get digit and sum their squares
            n = sumOfSquares(n);
            System.out.println("sum: " + n);
        }
        return true;
    }

    // blazing fast 0 ms, with magic (obtained through test runs) number 4 on which cycle is being detected
    static boolean isHappy(int n) {
        if ( n == 1 )
            return true;
        while ( n != 1 && n != 4 )
            n = sumOfSquares(n);
        return n == 1;
    }

    public static void main(String[] args) {
        System.out.println(isHappy(19));
        System.out.println(isHappy(3));
        System.out.println(isHappy(4));
    }

}
