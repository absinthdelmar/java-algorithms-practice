package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * 1386. Cinema Seat Allocation
 *
 * Medium
 *
 * [Medium_1386_CinemaSeatAllocation1.png]
 *
 * A cinema has n rows of seats, numbered from 1 to n and there are ten seats in each row, labelled from 1 to 10
 * as shown in the figure above.
 *
 * Given the array reservedSeats containing the numbers of seats already reserved, for example,
 * reservedSeats[i] = [3,8] means the seat located in row 3 and labelled with 8 is already reserved.
 *
 * Return the maximum number of four-person groups you can assign on the cinema seats.
 * A four-person group occupies four adjacent seats in one single row. Seats across an
 * aisle (such as [3,3] and [3,4]) are not considered to be adjacent, but there is an exceptional case on which
 * an aisle split a four-person group, in that case, the aisle split a four-person group in the middle, which means
 * to have two people on each side.
 *
 *
 *
 * Example 1:
 *
 * [Medium_1386_CinemaSeatAllocation2.png]
 *
 * Input: n = 3, reservedSeats = [[1,2],[1,3],[1,8],[2,6],[3,1],[3,10]]
 * Output: 4
 * Explanation: The figure above shows the optimal allocation for four groups, where seats mark with blue are
 * already reserved and contiguous seats mark with orange are for one group.
 * Example 2:
 *
 * Input: n = 2, reservedSeats = [[2,1],[1,8],[2,6]]
 * Output: 2
 * Example 3:
 *
 * Input: n = 4, reservedSeats = [[4,3],[1,4],[4,6],[1,7]]
 * Output: 4
 *
 *
 * Constraints:
 *
 * 1 <= n <= 10^9
 * 1 <= reservedSeats.length <= min(10*n, 10^4)
 * reservedSeats[i].length == 2
 * 1 <= reservedSeats[i][0] <= n
 * 1 <= reservedSeats[i][1] <= 10
 *
 * All reservedSeats[i] are distinct.
 *
 */
public class Medium_1386_CinemaSeatAllocation {

    // calculate total groups of four people can seat based on reservations per row
    static int totalGroupsOfFour(List<Integer> reservationRow){
        // default allocation
        int first = 1;
        int second = 1;
        int third = 0;

        // when only leftmost or rightmost or both seats are reserved,
        // so two families can fit when aisle split possible
        for (int i = 0; i < reservationRow.size(); i++) {
            int seat = reservationRow.get(i);
            if ( seat >= 2 && seat <= 5 )
                first = 0;
            if ( seat >= 6 && seat <= 9 )
                second = 0;
        }

        // check if third variant of allocation is still possible
        // set counters to zero if reserved seat is between two numbers, so family cannot fit
        if ( first == 0 && second == 0 ) {
            third = 1;
            for (int i = 0; i < reservationRow.size(); i++) {
                int seat = reservationRow.get(i);
                if ( seat >= 4 && seat <= 7 )
                    third = 0;
            }
        }

        // summarize obtained results from all cases
        return first + second + third;
    }

    static int maxNumberOfFamilies(int n, int[][] reservedSeats) {
        // basic case: 1 row, no allocated seats, so maximum two families
        if ( n == 1 && reservedSeats != null && reservedSeats.length == 0 )
            return 2;

        // default result is 2 multiplied by n rows, which is 2 families (groups of four) per row
        int result = 2 * n;

        // all available rows are not reserved
        if ( n > 1 && ( reservedSeats == null || reservedSeats.length == 0 ) )
            return result;

        Map<Integer, List<Integer>> reservations = new HashMap<>();

        // create a list per row with reserved seat numbers 1-10
        for ( int[] a : reservedSeats ) {
            List<Integer> list = reservations.getOrDefault( a[0], new ArrayList<>() );
            list.add( a[1] );
            reservations.put( a[0], list );
        }

        // so the final map could look like:
        // {1=[4, 7], 4=[3, 6]}
        // for input:
        // [[4,3],[1,4],[4,6],[1,7]]

        System.out.println("Reservations: " + reservations);

        for ( List<Integer> row : reservations.values() ) {
            result -= 2 - totalGroupsOfFour( row );
        }

        System.out.println("Can fit: " + result + " families");

        return result;
    }

    @Test
    public void testSolutionTwoRows() {
        int[][] reservations = new int[3][2];
        reservations[0] = new int[]{2,1};
        reservations[1] = new int[]{1,8};
        reservations[2] = new int[]{2,6};
        Assertions.assertEquals(2, maxNumberOfFamilies(2, reservations));
    }

    @Test
    public void testSolutionOneRowAisle() {
        int[][] reservations = new int[2][2];
        reservations[0] = new int[]{1,1};
        reservations[1] = new int[]{1,10};
        Assertions.assertEquals(2, maxNumberOfFamilies(1, reservations));
    }

    @Test
    public void testSolutionFourRows() {
        int[][] reservations = new int[4][2];
        reservations[0] = new int[]{4,3};
        reservations[1] = new int[]{1,4};
        reservations[2] = new int[]{4,6};
        reservations[3] = new int[]{1,7};
        Assertions.assertEquals(4, maxNumberOfFamilies(4, reservations));
    }

    public static void main(String[] args) {
        int[][] reservations = new int[3][2];
        reservations[0] = new int[]{2,1};
        reservations[1] = new int[]{1,8};
        reservations[2] = new int[]{2,6};
        System.out.println(maxNumberOfFamilies(2, reservations));
    }

}
