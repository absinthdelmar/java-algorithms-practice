package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Easy_203_RemoveLinkedListElement {

    static ListNode addNode(ListNode head, int value) {
        ListNode temp, p;
        temp = new ListNode(value);
        if ( head == null ) {
            head = temp;
        }
        else {
            p = head;
            while ( p.next != null ) {
                p = p.next;
            }
            p.next = temp;
        }
        return head;
    }

    // very slow 186 ms
    static ListNode removeElements0(ListNode head, int val) {
        List<Integer> list = new ArrayList<>();
        ListNode temp = head;
        while ( temp != null ) {
            if ( temp.val != val )
                list.add(temp.val);
            temp = temp.next;
        }
        head = null;
        for ( Integer item : list ) {
            System.out.println(item);
            head = addNode(head, item);
        }
        return head;
    }

    // still very slow even without a list
    static ListNode removeElements1(ListNode head, int val) {
        ListNode temp = head;
        ListNode result = null;
        while ( temp != null ) {
            if ( temp.val != val ) {
                System.out.println(temp.val);
                result = addNode(result, temp.val);
            }
            temp = temp.next;
        }
        return result;
    }

    static ListNode last(ListNode head) {
        ListNode last = head;
        ListNode temp = head;
        while ( temp != null ) {
            last = temp;
            temp = temp.next;
        }
        return last;
    }

    static ListNode after(ListNode head, int val) {
        if ( head == null )
            return null;
        if ( head.next == null )
            return head.val == val ? null : head;
        ListNode last = head;
        ListNode temp = head;
        while ( temp != null ) {
            if ( temp.val == val ) {
                last = temp;
                return last.next;
            }
            temp = temp.next;
        }
        return last;
    }

    static ListNode before(ListNode head, int val) {
        if ( head == null )
            return null;
        if ( head.next == null )
            return head.val == val ? null : head;
        ListNode prev = head;
        ListNode temp = head;
        while ( temp != null ) {
            if ( temp.val == val ) {
                prev.next = null;
            }
            prev = temp;
            temp = temp.next;
        }
        return head;
    }

    static ListNode merge(ListNode l1, ListNode l2) {
        if (l1 == null)
            return l2;
        if (l2 == null)
            return l1;
//        if (l1.val < l2.val) {
            l1.next = merge(l1.next, l2);
            return l1;
//        }
//        else {
//            l2.next = merge(l1, l2.next);
//            return l2;
//        }
    }

    // wrong for [1] or [1,1] with val=1
    static ListNode removeElements3(ListNode head, int val) {
        if ( head == null )
            return null;
        if ( head.next == null )
            return head.val == val ? null : head;
        return removeElements1(head, val);
//        ListNode after = after(head, val);
//        System.out.println("after:" + after);
//        ListNode before = before(head, val);
//        System.out.println("before:" + before);
//        return merge(before, after);
    }

    // fuck me :D so simple 0ms
    static ListNode removeElements(ListNode head, int val) {
        // if empty
        if ( head == null )
            return null;
        // new next is removeElements starting from current next (recursively)
        head.next = removeElements(head.next, val);
        // return current next node if (current value == value to remove) and current node otherwise
        return head.val == val ? head.next : head;
    }

    @Test
    public void testRemoveElements() {
        ListNode head = ListNode.addNode(null, 1);
        head = ListNode.addNode(head, 2);
        head = ListNode.addNode(head, 6);
        head = ListNode.addNode(head, 3);
        head = ListNode.addNode(head, 4);
        head = ListNode.addNode(head, 5);
        head = ListNode.addNode(head, 6);
        System.out.println(head);
        List<Integer> expected = Arrays.asList(1,2,3,4,5);
        List<Integer> actual = ListNode.toList(removeElements(head, 6));
        System.out.println(actual);
        Assertions.assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testRemoveElements2() {
        ListNode head = ListNode.addNode(null, 1);
        System.out.println(head);
        List<Integer> expected = Collections.emptyList();
        List<Integer> actual = ListNode.toList(removeElements(head, 1));
        Assertions.assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    public void testRemoveElements3() {
        ListNode head = ListNode.addNode(null, 1);
        head = ListNode.addNode(head, 1);
        System.out.println(head);
        List<Integer> expected = Collections.emptyList();
        List<Integer> actual = ListNode.toList(removeElements(head, 1));
        Assertions.assertArrayEquals(expected.toArray(), actual.toArray());
    }

    public static void main(String[] args) {
        ListNode head = ListNode.addNode(null, 1);
        head = ListNode.addNode(head, 2);
        head = ListNode.addNode(head, 6);
        head = ListNode.addNode(head, 3);
        head = ListNode.addNode(head, 4);
        head = ListNode.addNode(head, 5);
        head = ListNode.addNode(head, 6);
        System.out.println(head);
//        System.out.println(last(head));
//        System.out.println(after(head, 6));
//        System.out.println(before(head, 6));
        System.out.println(removeElements(head, 6));
    }

}
