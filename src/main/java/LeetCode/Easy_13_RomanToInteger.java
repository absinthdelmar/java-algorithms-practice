package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.
 *
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 * For example, two is written as II in Roman numeral, just two one's added together.
 * Twelve is written as, XII, which is simply X + II. The number twenty seven is written as XXVII, which is XX + V + II.
 *
 * Roman numerals are usually written largest to smallest from left to right.
 * However, the numeral for four is not IIII. Instead, the number four is written as IV.
 * Because the one is before the five we subtract it making four.
 * The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:
 *
 * I can be placed before V (5) and X (10) to make 4 and 9.
 * X can be placed before L (50) and C (100) to make 40 and 90.
 * C can be placed before D (500) and M (1000) to make 400 and 900.
 * Given a roman numeral, convert it to an integer. Input is guaranteed to be within the range from 1 to 3999.
 *
 * Example 1:
 *
 * Input: "III"
 * Output: 3
 * Example 2:
 *
 * Input: "IV"
 * Output: 4
 * Example 3:
 *
 * Input: "IX"
 * Output: 9
 * Example 4:
 *
 * Input: "LVIII"
 * Output: 58
 * Explanation: L = 50, V= 5, III = 3.
 * Example 5:
 *
 * Input: "MCMXCIV"
 * Output: 1994
 * Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 */
public class Easy_13_RomanToInteger {

    Map<String, Integer> dict;

    Easy_13_RomanToInteger() {
        dict = new HashMap<>();
        dict.put("I", 1);
        dict.put("IV", 4);
        dict.put("V", 5);
        dict.put("IX", 9);
        dict.put("X", 10);
        dict.put("XL", 40);
        dict.put("L", 50);
        dict.put("XC", 90);
        dict.put("C", 100);
        dict.put("CD", 400);
        dict.put("D", 500);
        dict.put("CM", 900);
        dict.put("M", 1000);
    }

    // 1994 = MCMXCIV
    // M-CM-XC-IV
    // IV = 4
    // XC = 100-10 = 90
    // CM = 1000-100 = 900
    // M = 1000
    int romanToInt(String s) {
        int result = 0;
        int i = 0;
        while ( i < s.length() ) {
            char c = s.charAt(i);
            char c2 = s.length() > i + 1 ? s.charAt(i + 1) : 0;
            String key = String.valueOf(c);
            String key2 = String.valueOf(c) + (c2 != 0 ? c2 : "");
            if ( dict.containsKey(key2) ) {
//                System.out.println(key2);
                result += dict.get(key2);
                i++;
            }
            else if (dict.containsKey(key)) {
//                System.out.println(key);
                result += dict.get(key);
            }
            i++;
        }
//        System.out.println(result);

        return result;
    }

    // solution # 2

    public int symbolToValue(char c) {
        switch (c) {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }

    public int romanToInt2(String s) {
        int result = 0;
        for (int i=0; i<s.length(); i++) {
            char c = s.charAt(i);
            if ( ( i < s.length() - 1) && (symbolToValue(c) < symbolToValue(s.charAt(i + 1))) ) {
                result -= symbolToValue(c);
            }
            else {
                result += symbolToValue(c);
            }
        }
        return result;
    }

    @Test
    void testRomanToInt() {
        Easy_13_RomanToInteger easy = new Easy_13_RomanToInteger();
        Assertions.assertEquals(3, easy.romanToInt("III"));
        Assertions.assertEquals(4, easy.romanToInt("IV"));
        Assertions.assertEquals(9, easy.romanToInt("IX"));
        Assertions.assertEquals(58, easy.romanToInt("LVIII"));
        Assertions.assertEquals(1994, easy.romanToInt("MCMXCIV"));
    }

    @Test
    void testRomanToInt2() {
        Easy_13_RomanToInteger easy = new Easy_13_RomanToInteger();
        Assertions.assertEquals(3, easy.romanToInt2("III"));
        Assertions.assertEquals(4, easy.romanToInt2("IV"));
        Assertions.assertEquals(9, easy.romanToInt2("IX"));
        Assertions.assertEquals(58, easy.romanToInt2("LVIII"));
        Assertions.assertEquals(1994, easy.romanToInt2("MCMXCIV"));
    }

    public static void main(String[] args) {
        Easy_13_RomanToInteger easy = new Easy_13_RomanToInteger();
//        System.out.println(easy.romanToInt("III"));
//        System.out.println(easy.romanToInt("IV"));
//        System.out.println(easy.romanToInt("IX"));
//        System.out.println(easy.romanToInt("LVIII"));
//        System.out.println(easy.romanToInt("MCMXCIV"));
//        System.out.println(easy.romanToInt2("III"));
//        System.out.println(easy.romanToInt2("IV"));
//        System.out.println(easy.romanToInt2("IX"));
//        System.out.println(easy.romanToInt2("LVIII"));
//        System.out.println(easy.romanToInt2("MCMXCIV"));
        System.out.println(easy.romanToInt2("MDCCCLXXXVIII"));
    }

}
