package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 100. Same Tree
 * Easy
 *
 * Given two binary trees, write a function to check if they are the same or not.
 *
 * Two binary trees are considered the same if they are structurally identical and the nodes have the same value.
 *
 * Example 1:
 *
 * Input:     1         1
 *           / \       / \
 *          2   3     2   3
 *
 *         [1,2,3],   [1,2,3]
 *
 * Output: true
 * Example 2:
 *
 * Input:     1         1
 *           /           \
 *          2             2
 *
 *         [1,2],     [1,null,2]
 *
 * Output: false
 * Example 3:
 *
 * Input:     1         1
 *           / \       / \
 *          2   1     1   2
 *
 *         [1,2,1],   [1,1,2]
 *
 * Output: false
 *
 */
public class Easy_100_SameTree {

    // walk tree to the left, then to the right, adding all values into list
    static void walk(List<Integer> values, TreeNode treeNode, int depth) {
        if ( treeNode == null ) {
            if ( depth >= 1 )
                values.add(null);
        }
        else {
            values.add(treeNode.val);
            depth++;
            walk(values, treeNode.left, depth);
            walk(values, treeNode.right, depth);
        }
    }

    static boolean isSameList(List<Integer> v1, List<Integer> v2) {
        if ( v1 == null && v2 == null )
            return true;
        if ( v1 == null || v2 == null )
            return false;
        if ( v1.size() != v2.size() )
            return false;
        for (int i = 0; i < v1.size(); i++) {
            System.out.println(i + " - " + v1.get(i) + " vs " + v2.get(i));
            if ( v1.get(i) != null && v2.get(i) != null &&
                 !v1.get(i).equals(v2.get(i)))
                return false;
            if ( v1.get(i) == null && v2.get(i) != null )
                return false;
            if ( v1.get(i) != null && v2.get(i) == null )
                return false;
        }
        return true;
    }

    // wow ! fast, 0 ms
    static boolean isSameTree0(TreeNode p, TreeNode q) {
        List<Integer> v1 = new ArrayList<>();
        List<Integer> v2 = new ArrayList<>();
        walk(v1, p, 0);
        walk(v2, q, 0);
        System.out.println(v1);
        System.out.println(v2);
        return isSameList(v1, v2);
    }

    // simplier 0ms version from website
    static boolean isSameTree(TreeNode p, TreeNode q) {
        System.out.println("p " + p);
        System.out.println("q " + q);
        if ( p == null && q == null )
            return true;
        if ( ( p == null && q != null ) || ( p != null && q == null ) || ( p.val != q.val ) )
            return false;
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    @Test
    public void testNotSameTree() {
        TreeNode treeNode1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        TreeNode treeNode2 = new TreeNode(1, new TreeNode(2), new TreeNode(3, new TreeNode(4), null));
        Assertions.assertFalse(isSameTree(treeNode1, treeNode2));
    }

    @Test
    public void testNotSameTree2() {
        TreeNode treeNode1 = new TreeNode(1, new TreeNode(2), null);
        TreeNode treeNode2 = new TreeNode(1, null, new TreeNode(2));
        Assertions.assertFalse(isSameTree(treeNode1, treeNode2));
    }

    @Test
    public void testNotSameTree3() {
        TreeNode treeNode1 = new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null));
        TreeNode treeNode2 = new TreeNode(1, null, new TreeNode(2, null, new TreeNode(3)));
        Assertions.assertFalse(isSameTree(treeNode1, treeNode2));
    }

    @Test
    public void testIsSameTree() {
        TreeNode treeNode1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        TreeNode treeNode2 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        Assertions.assertTrue(isSameTree(treeNode1, treeNode2));
    }

    @Test
    public void testIsSameTree2() {
        TreeNode treeNode1 = new TreeNode(0);
        TreeNode treeNode2 = new TreeNode(0);
        Assertions.assertTrue(isSameTree(treeNode1, treeNode2));
    }

    public static void main(String[] args) {
        TreeNode treeNode1 = new TreeNode(1, new TreeNode(2), new TreeNode(3));
        TreeNode treeNode2 = new TreeNode(1, new TreeNode(2), new TreeNode(3, new TreeNode(4), null));
        System.out.println(isSameTree(treeNode1, treeNode2));
    }

}
