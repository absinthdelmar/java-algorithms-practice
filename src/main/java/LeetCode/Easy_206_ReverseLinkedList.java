package LeetCode;

import java.util.*;

/**
 * 206. Reverse Linked List
 * Easy
 *
 * Reverse a singly linked list.
 *
 * Example:
 *
 * Input: 1->2->3->4->5->NULL
 * Output: 5->4->3->2->1->NULL
 * Follow up:
 *
 * A linked list can be reversed either iteratively or recursively. Could you implement both?
 */
public class Easy_206_ReverseLinkedList {

    // very slow 23 ms
    static ListNode reverseList0(ListNode head) {
        if ( head == null )
            return null;
        List<Integer> list = new ArrayList<>();
        ListNode temp = head;
        while ( temp != null ) {
            list.add(temp.val);
            temp = temp.next;
        }
        ListNode result = null;
        for (int i = list.size() - 1; i >= 0; i--) {
            result = ListNode.addNode(result, list.get(i));
        }
        return result;
    }

    // 0 ms
    static ListNode reverseList(ListNode head) {
        if ( head == null )
            return null;
        ListNode next, prev = null;
        ListNode current = head;
        while ( current != null ) {
            // save current node next reference
            next = current.next;
            // update it to previous reference (null on first iteration)
            current.next = prev;
            // update previous to current
            prev = current;
            // update current, make it saved next
            current = next;
        }
        return prev;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head = ListNode.addNode(head,2);
        head = ListNode.addNode(head,3);
        head = ListNode.addNode(head,4);
        head = ListNode.addNode(head,5);
        System.out.println(reverseList(head));
    }

}
