package LeetCode;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 83. Remove Duplicates from Sorted List
 * Easy
 *
 * Given a sorted linked list, delete all duplicates such that each element appear only once.
 *
 * Example 1:
 *
 * Input: 1->1->2
 * Output: 1->2
 * Example 2:
 *
 * Input: 1->1->2->3->3
 * Output: 1->2->3
 */
public class Easy_83_RemoveDuplicatesFromSortedList {

    static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            String dive = next != null ? "->" + next.toString() : "";
            return val + dive;
        }
    }

    static ListNode addNode(ListNode head, int value) {
        ListNode temp, p;
        // create a new listNode with specified value
        temp = new ListNode(value);
        // if head is not specified, our new listNode is the head
        if ( head == null ) {
            head = temp;
        }
        // otherwise, go to the end and add it
        else {
            p = head;
            while ( p.next != null ) {
                p = p.next;
            }
            p.next = temp;
        }
        return head;
    }

    // slow, using ArrayList to check for duplicates
    static ListNode deleteDuplicates0(ListNode head) {
        if ( head == null )
            return null;
        if ( head.next == null )
            return head;
        List<Integer> duplicates = new ArrayList<>();
        ListNode result = null;
        ListNode temp = head;
        do {
            if ( !duplicates.contains( temp.val ) ) {
                duplicates.add(temp.val);
                result = addNode(result, temp.val);
            }
        } while ( ( temp = temp.next ) != null );
        return result;
    }

    static boolean search( ListNode head, int val ) {
        if ( head == null )
            return false;
        ListNode temp = head;
        do {
            if ( temp.val == val )
                return true;
        } while ( ( temp = temp.next ) != null );
        return false;
    }

    // faster? 1ms
    static ListNode deleteDuplicates1(ListNode head) {
        if ( head == null )
            return null;
        if ( head.next == null )
            return head;
        ListNode result = null;
        ListNode temp = head;
        do {
            if ( !search( result, temp.val ) )
                result = addNode(result, temp.val);
        } while ( ( temp = temp.next ) != null );
        return result;
    }

    // even faster
    static ListNode deleteDuplicates(ListNode head) {
        if ( head == null )
            return null;
        if ( head.next == null )
            return head;
        ListNode result = head;
        ListNode temp = head;
        while ( temp != null ) {
            while ( temp.next != null && temp.next.val == temp.val )
                temp.next = temp.next.next;
            temp = temp.next;
        }
        return result;
    }

    @Test
    public void testDummy() {

    }

    @Test
    public void testDeleteDuplicates() {
        ListNode l1 = new ListNode(1, new ListNode(1, new ListNode(2)));
        ListNode l2 = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(3)))));
        ListNode l3 = new ListNode(1);
        Assertions.assertEquals("1->2", deleteDuplicates(l1).toString());
        Assertions.assertEquals("1->2->3", deleteDuplicates(l2).toString());
        Assertions.assertEquals("1", deleteDuplicates(l3).toString());
        Assertions.assertNull(deleteDuplicates(null));
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1, new ListNode(1, new ListNode(2)));
        ListNode l2 = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(3)))));
        System.out.println(deleteDuplicates(l1));
        System.out.println(deleteDuplicates(l2));
    }

}
