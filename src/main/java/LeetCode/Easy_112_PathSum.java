package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * 112. Path Sum
 * Easy
 *
 * Given a binary tree and a sum, determine if the tree has a root-to-leaf path
 * such that adding up all the values along the path equals the given sum.
 *
 * Note: A leaf is a node with no children.
 *
 * Example:
 *
 * Given the below binary tree and sum = 22,
 *
 *       5
 *      / \
 *     4   8
 *    /   / \
 *   11  13  4
 *  /  \      \
 * 7    2      1
 *
 * return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
 *
 */
public class Easy_112_PathSum {

    // TODO
    static boolean hasPathSum(TreeNode root, int sum) {
        // empty
        if ( root == null )
            return false;

        // is a leaf
        if ( root.left == null && root.right == null )
            return root.val == sum;

        return false;
    }

    //       5
    //      / \
    //     4   8
    //    /   / \
    //   11  13  4
    //  /  \      \
    // 7    2      1
    @Test
    public void testHasPathSum() {
        TreeNode tree = new TreeNode(
            5,
            new TreeNode(4, new TreeNode(11, new TreeNode(7), new TreeNode(2)), null),
            new TreeNode(8, new TreeNode(13), new TreeNode(4, null, new TreeNode(1)))
        );
        Assertions.assertTrue(hasPathSum(tree, 22));
    }

    @Test
    public void testHasPathSum2() {
        TreeNode tree = new TreeNode(22);
        Assertions.assertTrue(hasPathSum(tree, 22));
    }

    public static void main(String[] args) {
        TreeNode tree = new TreeNode(
            5,
            new TreeNode(4, new TreeNode(11, new TreeNode(7), new TreeNode(2)), null),
            new TreeNode(8, new TreeNode(13), new TreeNode(4, null, new TreeNode(1)))
        );
        System.out.println(hasPathSum(tree, 22));
    }

}
