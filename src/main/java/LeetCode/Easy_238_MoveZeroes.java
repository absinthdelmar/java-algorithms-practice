package LeetCode;

import java.util.Arrays;

/**
 * 283. Move Zeroes
 * Easy
 *
 * Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order
 * of the non-zero elements.
 *
 * Example:
 *
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 * Note:
 *
 * You must do this in-place without making a copy of the array.
 * Minimize the total number of operations.
 */
public class Easy_238_MoveZeroes {

    static void swap(int[] nums, int i, int j) {
        System.out.println("swap: " + nums[i] + " with " + nums[j]);
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // working properly at least
    // 31 ms
    static void moveZeroes0(int[] nums) {
        int i = 0;
        int zeroCount = 0;
        while ( i < nums.length ) {
            System.out.println("i = " + i);
            // got zero
            if ( nums[i] == 0 ) {
                zeroCount++;
            }
            // we have continuous zeroes to move
            else if ( zeroCount > 0 ) {
                // reset pointer i
                i -= zeroCount;
                // make new pointer j equals i
                int j = i;
                System.out.println("zeroCount = " + zeroCount);
                System.out.println("i = " + i);
                // iterate from j to zeroCount
                // swap j with j + zeroCount
                while ( zeroCount > 0 ) {
                    System.out.println("j = " + j);
                    swap(nums, j, j + zeroCount);
                    System.out.println(Arrays.toString(nums));
                    zeroCount--;
                    j++;
                }
            }
            i++;
        }
        System.out.println(Arrays.toString(nums));
    }

    // wow!!! so simple taken from previous submissions
    // 0 ms
    // needs step by step drawing explanation
    static void moveZeroes(int[] nums) {
        int count = 0;
        for ( int i = 0; i < nums.length; i++ ) {
            if ( nums[i] != 0 ) {
                nums[count++] = nums[i];
                System.out.println("count = " + count + ": " + Arrays.toString(nums));
                if (i != count - 1) {
                    nums[i] = 0;
                    System.out.println("replace with 0: " + Arrays.toString(nums));
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] nums = new int[] {0,1,0,3,12};
        moveZeroes(nums);
//        System.out.println(Arrays.toString(nums));
    }

}
