package LeetCode;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * 189. Rotate Array
 * Easy
 *
 * Given an array, rotate the array to the right by k steps, where k is non-negative.
 *
 * Follow up:
 *
 * Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
 * Could you do it in-place with O(1) extra space?
 *
 *
 * Example 1:
 *
 * Input: nums = [1,2,3,4,5,6,7], k = 3
 * Output: [5,6,7,1,2,3,4]
 * Explanation:
 * rotate 1 steps to the right: [7,1,2,3,4,5,6]
 * rotate 2 steps to the right: [6,7,1,2,3,4,5]
 * rotate 3 steps to the right: [5,6,7,1,2,3,4]
 * Example 2:
 *
 * Input: nums = [-1,-100,3,99], k = 2
 * Output: [3,99,-1,-100]
 * Explanation:
 * rotate 1 steps to the right: [99,-1,-100,3]
 * rotate 2 steps to the right: [3,99,-1,-100]
 *
 *
 * Constraints:
 *
 * 1 <= nums.length <= 2 * 10^4
 * It's guaranteed that nums[i] fits in a 32 bit-signed integer.
 * k >= 0
 *
 */
public class Easy_189_RotateArray {

    static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    static void rotate(int[] nums, int k) {
        while ( k > 0 ) {
            for (int i = nums.length - 1; i > 0; i--) {
                swap(nums, i - 1, i);
            }
            --k;
        }
    }

    @Test
    public void testRotate() {
        int[] nums = new int[]{1,2,3,4,5,6,7};
        int[] expected = new int[]{5,6,7,1,2,3,4};
        rotate(nums, 3);
        Assertions.assertArrayEquals(expected, nums);
    }

    @Test
    public void testRotate2() {
        int[] nums = new int[]{-1,-100,3,99};
        int[] expected = new int[]{3,99,-1,-100};
        rotate(nums, 2);
        Assertions.assertArrayEquals(expected, nums);
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3,4,5,6,7};
        System.out.println(Arrays.toString(nums));
        rotate(nums, 3);
        System.out.println(Arrays.toString(nums));
    }

}
