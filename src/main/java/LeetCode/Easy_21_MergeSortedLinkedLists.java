package LeetCode;

import java.util.*;

/**
 * 21. Merge Two Sorted Lists
 * Easy
 *
 * Merge two sorted linked lists and return it as a new sorted list. The new list should be made by splicing together the nodes of the first two lists.
 *
 * Example:
 *
 * Input: 1->2->4, 1->3->4
 * Output: 1->1->2->3->4->4
 */
public class Easy_21_MergeSortedLinkedLists {

    static class ListNode {
        int val;
        ListNode next;

        ListNode() {}

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            String dive = next != null ? "->" + next.toString() : "";
            return val + dive;
        }
    }

    static ListNode addNode(ListNode head, int value) {
        ListNode temp, p;
        // create a new listNode with specified value
        temp = new ListNode(value);
        // if head is not specified, our new listNode is the head
        if ( head == null ) {
            head = temp;
        }
        // otherwise, go to the end and add it
        else {
            p = head;
            while ( p.next != null ) {
                p = p.next;
            }
            p.next = temp;
        }
        return head;
    }

    static ListNode sort(ListNode listNode) {
        if (listNode == null)
            return null;

        // extract listNode values and sort
        List<Integer> list = new ArrayList<>();
        for ( ListNode walker = listNode; walker != null; walker = walker.next ) {
            list.add(walker.val);
        }

        // sort our list
        Collections.sort(list);

        // create new listNode with sorted values
        ListNode root = addNode(null, list.get(0));
        for ( int i=1; i<list.size(); i++ ) {
            root = addNode(root, list.get(i));
        }
        return root;
    }

    // my first version, sorts everything, even if initial are unsorted
    static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 != null && l2 != null) {
            return sort(new ListNode(l1.val, new ListNode(l2.val, mergeTwoLists(l1.next, l2.next))));
        }
        else if (l1 != null) {
            return sort(l1);
        }
        else if (l2 != null) {
            return sort(l2);
        }
        return null;
    }

    // 0 ms version, but > 40mb ram
    static ListNode mergeTwoLists2(ListNode l1, ListNode l2) {
        if ( l1 != null && l2 != null ) {
            ListNode prev = l1;
            if ( l1.val <= l2.val ) {
                l1.next = mergeTwoLists2(l1.next, l2);
            }
            else {
                l1 = l2;
                l1.next = mergeTwoLists2(l2.next, prev);
            }
            return l1;
        }
        else if ( l1 != null ) {
            return l1;
        }
        else if ( l2 != null ) {
            return l2;
        }
        return null;
    }

    // 0 ms version, less ram
    static ListNode mergeTwoLists3(ListNode l1, ListNode l2) {
        if (l1 == null)
            return l2;
        if (l2 == null)
            return l1;
        if (l1.val < l2.val) {
            l1.next = mergeTwoLists3(l1.next, l2);
            return l1;
        }
        else {
            l2.next = mergeTwoLists3(l1, l2.next);
            return l2;
        }
    }

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        ListNode l2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        System.out.println(l1);
        System.out.println(l2);
        System.out.println("my: " + mergeTwoLists(l1, l2));
        // additional cases
        l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        l2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        System.out.println(mergeTwoLists(l1, null));
        System.out.println(mergeTwoLists(null, l2));
        l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        l2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        System.out.println("other1: " + mergeTwoLists2(l1, l2));
        l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        l2 = new ListNode(1, new ListNode(3, new ListNode(4)));
        System.out.println("other2: " + mergeTwoLists3(l1, l2));
    }

}
