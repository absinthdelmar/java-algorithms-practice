package Parallel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.TimeUnit;

public class MergeSort {

    // basic non-parallel version
    static void mergeSort(int[] arr, int low, int high) {
        if ( low < high ) {
            int mid = ( low + high ) / 2;
            mergeSort(arr, low, mid);
            mergeSort(arr, mid + 1, high);
            merge(arr, low, mid, high);
        }
    }

    static void merge(int[] arr, int low, int mid, int high) {
        int n1 = mid - low + 1;
        int n2 = high - mid;

        int[] left = new int[n1];
        int[] right = new int[n2];

        for (int i = 0; i < n1; ++i) {
            left[i] = arr[low + i];
        }

        for (int j = 0; j < n2; ++j) {
            right[j] = arr[mid + j + 1];
        }

        int i = 0, j = 0;
        int k = low;

        while ( i < n1 && j < n2 ) {
            if ( left[i] <= right[j] ) {
                arr[k] = left[i];
                i++;
            }
            else {
                arr[k] = right[j];
                j++;
            }
            k++;
        }

        while ( i < n1 ) {
            arr[k] = left[i];
            i++;
            k++;
        }

        while ( j < n2 ) {
            arr[k] = right[j];
            j++;
            k++;
        }
    }

    // parallel version

    static class ParallelMergeSort extends RecursiveAction {
        private static final int MAX = 1 << 13;
        int[] arr;
//        int[] temp;
        int low;
        int high;

        ParallelMergeSort(final int[] arr, final int low, final int high) {
            this.arr = arr;
            this.low = low;
            this.high = high;
//            this.temp = new int[arr.length];
        }

        @Override
        protected void compute() {
            if ( low < high ) {
                if ( high - low <= MAX ) {
                    mergeSort(arr, low, high);
                }
                else {
                    final int mid = (low + high) / 2;
                    final ParallelMergeSort left = new ParallelMergeSort(arr, low, mid);
                    final ParallelMergeSort right = new ParallelMergeSort(arr, mid + 1, high);
                    invokeAll(left, right);
                    //                mergeParallel(arr, temp, low, mid, high);
                    mergeParallel(arr, low, mid, high);
                }
            }
        }
    }

//    static void mergeParallel(int[] arr, int[] temp, int low, int mid, int high) {
    static void mergeParallel(int[] arr, int low, int mid, int high) {
        int[] temp = Arrays.copyOf(arr, arr.length);

        int left = low;
        int right = mid + 1;
        int current = low;

        while ( left <= mid && right <= high ) {
            if ( temp[left] <= temp[right] )
                arr[current] = temp[left++];
            else
                arr[current] = temp[right++];
            current++;
        }

        while ( left <= mid ) {
            arr[current++] = temp[left++];
        }
    }

    static int[] random(final int n) {
        final int[] a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = new Random().nextInt(n);
        }

        return a;
    }

    private static final int SIZE = 100_000;

    @Benchmark
    @Fork( value = 1, warmups = 1 )
    @Warmup( iterations = 1 )
    @Measurement( iterations = 3 )
    @OutputTimeUnit( TimeUnit.MILLISECONDS )
    @BenchmarkMode( Mode.AverageTime )
    public void benchSequential() {
        final int[] nums = random(SIZE);
        mergeSort(nums, 0, nums.length - 1);
    }

    @Benchmark
    @Fork( value = 1, warmups = 1 )
    @Warmup( iterations = 1 )
    @Measurement( iterations = 3 )
    @OutputTimeUnit( TimeUnit.MILLISECONDS )
    @BenchmarkMode( Mode.AverageTime )
    public void benchParallel() {
        final int[] nums = random(SIZE);
        final ForkJoinPool pool = new ForkJoinPool( Runtime.getRuntime().availableProcessors() - 1 );
        pool.invoke(new ParallelMergeSort(nums, 0, nums.length - 1));
//        Arrays.parallelSort(nums);
    }

    public static void main(final String[] args) throws IOException {
//        int[] nums = new int[]{ 12, 11, 13, 5, 6, 7 };
//        mergeSort(nums, 0, nums.length - 1);
//        System.out.println(Arrays.toString(nums));
        org.openjdk.jmh.Main.main(args);
    }

    private static void check(final int[] array) {
//        System.out.println(Arrays.toString(array));
        int last = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < last) {
                Assertions.fail();
            }
            last = array[i];
        }
    }

    @Test
    public void testSequential() {
        final int[] nums = random(200000);
        mergeSort(nums, 0, nums.length - 1);
        check(nums);
    }

    @Test
    public void testParallel() {
        final int[] nums = random(200000);
        final ForkJoinPool forkJoinPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors() - 1);
        forkJoinPool.invoke(new ParallelMergeSort(nums, 0, nums.length - 1));
        check(nums);
    }

}
