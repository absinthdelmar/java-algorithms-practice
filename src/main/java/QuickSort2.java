import java.util.Arrays;
import java.util.Random;

public class QuickSort2 {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void quicksort(int[] arr, int start, int end) {
        if ( start >= end )
            return;
        int i = start;
        int j = end;
        int pi = (start + end) / 2;

        while (i < j) {
            while (arr[i] <= arr[pi] && i < pi) {
                 i++;
            }
            while (arr[j] >= arr[pi] && j > pi) {
                 j--;
            }
            swap(arr, i, j);
            if ( i == pi )
                pi = j;
            else
                pi = i;

            quicksort(arr, start, pi);
            quicksort(arr, pi + 1, end);
        }
    }

    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[50];
        for (int i = 0; i < 50; i++) {
            arr[i] = random.nextInt(100);
        }
        quicksort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

}
