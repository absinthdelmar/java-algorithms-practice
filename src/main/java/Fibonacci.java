import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

    // cache version
    static int fib(int n) {
        List<Integer> cache = new ArrayList<>();
        cache.add(0, 0);
        cache.add(1, 1);
        for (int i=2; i<= n; i++) {
            cache.add(i, cache.get(i - 1) + cache.get(i - 2));
        }
        System.out.println(cache);
        return cache.get(n);
    }

    // just simpliest version, we need only two numbers for calculation the next
    static int fib2(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        int a = 0, b = 1, c, i = 2;
        do {
            c = a + b;
            a = b;
            b = c;
            i++;
        } while ( i <= n );
        return b;
    }

    public static void main(String[] args) {
        System.out.println(fib(20));
        System.out.println(fib2(20));
    }

}
