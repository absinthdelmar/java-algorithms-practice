import java.util.Arrays;
import java.util.Random;

/**
 * Pivot is the last element
 */
public class QuickSort4 {

    // swap elements
    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partitionLast( int[] arr, int start, int end ) {
        int pivot = arr[end];
        int i = start - 1;

        for ( int j = start; j <= end; j++ ) {
            if ( arr[j] < pivot ) {
                i++;
                swap(arr, i, j);
            }
        }
        swap( arr, i + 1, end );

        return i + 1;
    }

    static void sort(int[] arr, int start, int end) {
        // check boundaries
        if (start >= end)
            return;
        int pi = partitionLast(arr, start, end);
        sort(arr, start, pi-1);
        sort(arr, pi+1, end);
    }

    static void sortMid( int[] arr, int start, int end ) {
        if ( start >= end )
            return;

//        System.out.println(start + ", " + end);

        int i = start;
        int j = end;
        int pivot = arr[(start + end) / 2];

        while ( i <= j )  {
            while (arr[i] < pivot)
                i++;

            while (arr[j] > pivot)
                j--;

            if ( i <= j ) {
                swap(arr, i, j);
                i++;
                j--;
            }
//            System.out.println(Arrays.toString(arr));
        }
        sortMid(arr, start, j);
        sortMid(arr, i, end);
    }

    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[20];
        for (int i = 0; i < 20; i++)
            arr[i] = random.nextInt(100);

        int[] arr2 = Arrays.copyOf(arr, arr.length);

        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));

        sort(arr, 0, arr.length - 1);
        sortMid(arr2, 0, arr2.length - 1);

        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(arr2));
    }

}
