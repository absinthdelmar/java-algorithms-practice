import java.util.Arrays;

public class QuickSort5 {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void sort(int[] arr, int start, int end) {
        if ( end > start ) {
            int pi = (start + (end - start)) >>> 1;
            int i = start;
            int j = end;
            while (i < j) {
                while (i < pi && arr[i] <= arr[pi]) {
                    System.out.println(i);
                    i++;
                }
                while (j > pi && arr[j] >= arr[pi]) {
                    System.out.println(j);
                    j--;
                }
                // update pivot index
                if (i != pi)
                    pi = i;
                else
                    pi = j;

                swap(arr, i, j);

                // sort left part
                sort(arr, start, pi - 1);
                // sort right part
                sort(arr, pi + 1, end);
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{5,4,10,2,8,3,9,1,6};
        sort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

}
