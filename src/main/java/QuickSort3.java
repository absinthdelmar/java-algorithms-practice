import java.util.Arrays;
import java.util.Random;

// quick sort on the same array
public class QuickSort3 {

    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static void sort(int[] arr, int start, int end) {
        if ( start < end ) {
            // set left iteration index to start value
            int leftIndex = start;
            // set right iteration index to end value
            int rightIndex = end;
            // choose pivot as start + end index divided in half
            int pivotIndex = (start + end) >>> 1;

            while ( leftIndex < rightIndex ) {
                // increase leftIndex (move to the right) if leftIndex is less than pivotIndex
                // and array element with leftIndex is less or equal to array element with pivotIndex
                while (leftIndex < pivotIndex && arr[leftIndex] <= arr[pivotIndex])
                    leftIndex++;
                // decrease rightIndex (move to the left) if rightIndex is greater than pivotIndex
                // and array element with rightIndex is greater or equal to array element with pivotIndex
                while (rightIndex > pivotIndex && arr[rightIndex] >= arr[pivotIndex])
                    rightIndex--;
                // swap array elements
                swap(arr, leftIndex, rightIndex);
                // set pivotIndex to the latest leftIndex value only if they are not equal
                if ( leftIndex != pivotIndex )
                    pivotIndex = leftIndex;
                // set to latest rightIndex otherwise
                else
                    pivotIndex = rightIndex;
            }
            // run again through array left part or subarray (less than pivotIndex)
            sort(arr, start, pivotIndex - 1);
            // run again through right part or subarray (greater than pivotIndex)
            sort(arr, pivotIndex + 1, end);
        }
    }

    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[10000];
        for (int i = 0; i < 10000; i++)
            arr[i] = random.nextInt(10000);

        int[] arr1 = Arrays.copyOf(arr, arr.length);
        int[] arr2 = Arrays.copyOf(arr, arr.length);

        long start = System.currentTimeMillis();
        sort(arr1, 0, arr1.length - 1);
        System.out.println(Arrays.toString(arr1));
        long end = System.currentTimeMillis();
        System.out.println((end - start) + " ms");

        start = System.currentTimeMillis();
        Arrays.sort(arr2);
        System.out.println(Arrays.toString(arr2));
        end = System.currentTimeMillis();
        System.out.println((end - start) + " ms");
    }

}
