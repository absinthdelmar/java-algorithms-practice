package Codility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// largest sibling 123 -> [1,2,3] -> [3,2,1] ? [123, 321, 213, 132, 312, 231] -> 321
public class Codility_Guidewire {

    // convert number to list
    static List<Integer> numberToList(int number) {
        if ( number == 0 )
            return Collections.singletonList(0);

        List<Integer> list = new ArrayList<>();

        for (; number != 0; number /= 10)
            list.add( number % 10 );

        return list;
    }

    // assemble list of digits back to integer assuming possible overflow so use long
    static int listToNumber(List<Integer> list) {
        long result = 0;
        for (int i = 0; i < list.size(); i++) {
            result = 10 * result + list.get(i);
        }
        return result > 100000000 ? -1 : (int)result;
    }

    static int solution(int N) {
        List<Integer> digits = numberToList(N);
        digits.sort((x, y) -> -Integer.compare(x, y));
        return listToNumber(digits);
    }

    @Test
    public void testSolution() {
        Assertions.assertEquals(321, solution(213));
    }

    @Test
    public void testSolutionLarge() {
        Assertions.assertEquals(99994321, solution(19293949));
    }

    @Test
    public void testSolutionVeryLarge() {
        Assertions.assertEquals(-1, solution(2147483647));
    }

    public static void main(String[] args) {
//        System.out.println(solution(new int[]{1,3,6,4,1,2}));
        System.out.println(solution(19293949));
//        System.out.println(numberToList(321));
    }

}
