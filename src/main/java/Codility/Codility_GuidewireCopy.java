package Codility;

// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class Codility_GuidewireCopy {
    // convert our integer number to a list of digits it consists of
    static List<Integer> numberToList(int number) {
        if ( number == 0 )
            return Collections.singletonList( 0 );
        List<Integer> result = new ArrayList<>();
        for ( ; number != 0; number = number / 10 ) {
            result.add( number % 10 );
        }
        return result;
    }

    // assemble list of digits back to integer number, assuming it may overflow so use long
    static int listToNumber(List<Integer> list) {
        long result = 0;
        for (int i=0; i < list.size(); i++) {
            result = 10 * result + list.get(i);
        }
        return result > 100000000 ? -1 : (int)result;
    }

    // so the idea is to create a list of digits from given number, sort them in descendant order, to get the largest number, then assembly back, return it if less than 100,000,000 and return -1 if greater
    // looks like long number cannot arrive here :)
    // but negative can, what should we do? just return -1 by default if negative?
    static int solution(int N) {
        if ( N < 0 )
            return -1;
        List<Integer> digits = numberToList(N);
        digits.sort((x, y) -> -Integer.compare(x, y));  // descending order
        return listToNumber(digits);
    }

    public static void main(String[] args) {
        System.out.println(solution(19293949));
        System.out.println(solution(999999991));
    }
}
