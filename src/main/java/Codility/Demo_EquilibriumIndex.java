package Codility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

/**
 * Similar LeetCode - 1013. Partition Array Into Three Parts With Equal Sum,
 * The one below is Equal Two Parts
 *
 * Codility - Equi
 *
 * This is a demo task.
 *
 * An array A consisting of N integers is given. An equilibrium index of this array is any integer P
 * such that 0 ≤ P < N and the sum of elements of lower indices is equal to the sum of elements of higher indices, i.e.
 * A[0] + A[1] + ... + A[P−1] = A[P+1] + ... + A[N−2] + A[N−1].
 * Sum of zero elements is assumed to be equal to 0. This can happen if P = 0 or if P = N−1.
 *
 * For example, consider the following array A consisting of N = 8 elements:
 *
 *   A[0] = -1
 *   A[1] =  3
 *   A[2] = -4
 *   A[3] =  5
 *   A[4] =  1
 *   A[5] = -6
 *   A[6] =  2
 *   A[7] =  1
 * P = 1 is an equilibrium index of this array, because:
 *
 * A[0] = −1 = A[2] + A[3] + A[4] + A[5] + A[6] + A[7]
 * P = 3 is an equilibrium index of this array, because:
 *
 * A[0] + A[1] + A[2] = −2 = A[4] + A[5] + A[6] + A[7]
 * P = 7 is also an equilibrium index, because:
 *
 * A[0] + A[1] + A[2] + A[3] + A[4] + A[5] + A[6] = 0
 * and there are no elements with indices greater than 7.
 *
 * P = 8 is not an equilibrium index, because it does not fulfill the condition 0 ≤ P < N.
 *
 * Write a function:
 *
 * def solution(A)
 *
 * that, given an array A consisting of N integers, returns any of its equilibrium indices.
 * The function should return −1 if no equilibrium index exists.
 *
 * For example, given array A shown above, the function may return 1, 3 or 7, as explained above.
 *
 * Write an efficient algorithm for the following assumptions:
 *
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [−2,147,483,648..2,147,483,647].
 *
 */
public class Demo_EquilibriumIndex {

    // calculate the sum of elements within certain boundaries
    // consider using (long) in sum and back to (int) cast to prevent overflow
    static long sum(int[] a, int start, int end) {
        // assume sum is 0
        if (start > end)
            return 0;
//        System.out.println("start: " + start + " end: " + end);
        long sum = 0;
        // start inclusive, end exclusive
        for (int i = start; i < end; i++) {
            sum += (long) a[i];
        }
//        System.out.println(Arrays.toString(Arrays.copyOfRange(a, start, end)) + " -> sum = " + sum);
        return sum;
    }

    // max 10^5 elements
    // -Integer.MAX_VALUE <= a[i] <= Integer.MAX_VALUE
    // equi index is the index of the element where
    // sum of elements before it (lower) equals sum of elements after it (higher)
    //
    // very bad solution :)
    static int solution1(int[] a) {
        if ( a == null || a.length == 0 )
            return -1;

        int n = a.length;

        List<Integer> solutions = new ArrayList<>();

        // to check that a chosen pivotIndex ( equilibriumIndex ) is correct
        // lets calculate a lowSum (before it) and highSum (after it)
        // 1st case: lets use single pivot and go from 0 to n, must be ok for small sized arrays
        int pi = 0;
        while ( pi < n ) {
            long lowSum = sum(a, 0, pi);
            long highSum = sum(a, pi + 1, n);
//            System.out.println("lowSum " + lowSum + " highSum " + highSum);
            if ( lowSum == highSum )
                solutions.add( pi );
            pi++;
        }

        System.out.println("solutions: " + solutions);

        // check if any solutions available return just the first one
        // if none - return -1
        return !solutions.isEmpty() ? solutions.get(0) : -1;
    }

    // equilibrium index example, fast
    static int solution2(int[] nums) {
        // total sum of all elements
        long sum = Arrays.stream(nums).sum();
        // initial left sum
        long ls = 0;
        for (int i = 0; i < nums.length; i++) {
            // if current left sum equals total sum - current left sum - current value return current index
            // in other words left sum equals right sum or the sum of elements remaining on the right of the current index
            if ( ls == ( sum - ls - nums[i] ) ) {
                return i;
            }
            // add to left sum - sum of elements from 0 to i
            else {
                ls += nums[i];
            }
        }
        return -1;
    }

    // should be fast also
    static int solution(int[] nums) {
        int result = -1;
        // init left and right sums as long to prevent integer overflows
        long ls = 0;
        long rs = 0;
        // make right sum the sum of all element values
        for (int i = 0; i < nums.length; i++)
            rs += nums[i];
        // traverse again, decrease rs by current value on each step, check ls == rs, if so return current index
        // if not, increase ls by current value, until they collide, return default -1 in case it never happens
        for (int i = 0; i < nums.length; i++) {
            rs -= nums[i];
            if ( ls == rs )
                return i;
            else
                ls += nums[i];
        }
        return result;
    }

    @Test
    public void testSolution() {
        int[] a = new int[] { -1, 3, -4, 5, 1, -6, 2, 1 };
        Assertions.assertEquals(1, solution(a));
//        Assertions.assertEquals(3, solution(a));
//        Assertions.assertEquals(7, solution(a));
    }

    @Test
    public void testSolutionSmallSizedArray() {
        // 1, because -1 == -1
        int[] a = new int[] { -1, -1, -1 };
        // -1, because 0 vs -1 and -1 vs 0
        int[] b = new int[] { -1, -1 };
        // 0, because sum by default is 0, thus having no elements on the left and right, their sums are also zero
        int[] c = new int[] { -1 };
        // -1, no elements at all, meaning there is no index at all
        int[] d = new int[] { };
        Assertions.assertEquals(1, solution(a));
        Assertions.assertEquals(-1, solution(b));
        Assertions.assertEquals(0, solution(c));
        Assertions.assertEquals(-1, solution(d));
    }

    @Test
    public void testSolutionSumOverflowPositive() {
        // if sum function was int not long, then the solution would result in 1 instead of -1 due to overflow
        int[] a = new int[] { Integer.MAX_VALUE, Integer.MAX_VALUE, -1, -2 };
        Assertions.assertEquals(-1, solution(a));
    }

    @Test
    public void testSolutionSumOverflowNegative() {
        // if sum function was int not long, then the solution would result in 1 instead of -1 due to overflow
        int[] b = new int[] { -Integer.MAX_VALUE, -100, -1, 2147483549 };
        Assertions.assertEquals(-1, solution(b));
    }

//    @Test
    public void testSolutionLargeSizedArray() {
        int n = 100000;
        int[] a = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            a[i] = random.nextInt(2);
        }
//        System.out.println(Arrays.toString(a));
        System.out.println(solution(a));
    }

    public static void main(String[] args) {
//        int[] a = new int[] { -1, 3, -4,  5, 1, -6,  2, 1 };
//        int[] a = new int[] { 0,2,1, -6,6,-7,9,1,  2,0,1 };      // correct
//        int[] a = new int[] { 0,2,1,    -6,6,7,-2,-2,   1,2 };
//        int[] a = new int[] { 2,-1,1,1,1,2,-2 };
//        int[] a = new int[] { -1, 3, -4, 5, 1, -6, 2, 1, -2 };
        int[] a = new int[] { 1, 2, 4, 3 };     // correct
        System.out.println(solution(a));
    }

}
