package Codility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// from scratch, solve again
public class Codility_PlaneSeats {

    // e.g.: 1A 2B 1K 4C 40A
    final private static Pattern pattern = Pattern.compile("^(\\d+)([ABCDEFGHJK])$");
    final private static Map<String, Integer> letterToNumber;

    static {
        letterToNumber = new HashMap<>();
        letterToNumber.put( "A", 1 );
        letterToNumber.put( "B", 2 );
        letterToNumber.put( "C", 3 );
        letterToNumber.put( "D", 4 );
        letterToNumber.put( "E", 5 );
        letterToNumber.put( "F", 6 );
        letterToNumber.put( "G", 7 );
        letterToNumber.put( "H", 8 );
        letterToNumber.put( "J", 9 );
        letterToNumber.put( "K", 10 );
    }

    static Map<Integer, List<Integer>> parseReservations(int availableRows, String s) {
        Map<Integer, List<Integer>> result = new HashMap<>();
        String[] seats = s.split(" ");
        for ( String seat: seats ) {
            Matcher matcher = pattern.matcher(seat);
            if ( matcher.find() ) {
                Integer row = Integer.parseInt( matcher.group(1) );

                // skip reservations for rows that are not available
                if ( row > availableRows ) {
                    System.out.println("Max seat rows is " + availableRows + ". Row " + row + " ignored.");
                    continue;
                }

                Integer num = letterToNumber.get( matcher.group(2) );
                List<Integer> list = result.getOrDefault( row, new ArrayList<>() );
                list.add( num );
                result.put( row, list );
            }
        }

        System.out.println(result);

        return result;
    }

    static int totalGroupsPerRow( List<Integer> row ) {
        int aisle1 = 1;
        int aisle2 = 1;
        int justOne = 0;

        // aisle fit
        for ( Integer seat : row ) {
            if ( seat >= 2 && seat <= 5 )
                aisle1 = 0;
            if ( seat >= 6 && seat <= 9 )
                aisle2 = 0;
        }

        // check if one per row is still possible
        if ( aisle1 == 0 && aisle2 == 0 ) {
            justOne = 1;
            for (Integer seat : row) {
                if ( seat >= 4 && seat <= 7 )
                    justOne = 0;
            }
        }

        return aisle1 + aisle2 + justOne;
    }

    /**
     * Plane seats
     * @param n total available seat rows
     * @param s list of reservations in a form of string: 1A 2F 1C
     * @return total number of four people groups possible to fit
     */
    static int canFit(int n, String s) {
        // no rows, impossible to fit anybody
        if ( n == 0 )
            return 0;
        // any numbers of rows, no reservations, max fit is n * 2
        if ( n >= 1 && ( s == null || s.length() == 0 ) )
            return n * 2;

        int result = n * 2;

        for ( List<Integer> row : parseReservations( n, s ).values() ) {
            int groups = totalGroupsPerRow( row );
            System.out.println(row + " => " + groups);
            result -= 2 - groups;
        }

        System.out.println(result);

        return result;
    }

    @Test
    public void testSingleRowOneReservationOneGroup() {
        Assertions.assertEquals(1, canFit(1, "1F"),
                "1 row, 6 reserved, 1 group");
        Assertions.assertEquals(1, canFit(1, "1E"),
                "1 row, 5 reserved, 1 group");
    }

    @Test
    public void testSingleRowFitAisleTwoGroups() {
        Assertions.assertEquals(2, canFit(1, "1A 1K"),
                "1 row, 1 and 10 reserved, 2 groups");
    }

    @Test
    public void testTwoRowsFitTwoGroups() {
        Assertions.assertEquals(2, canFit(2, "1A 2F 1C"),
                "2 rows, 1-1 and 1-3 reserved, 2-6 reserved, 2 groups");
    }

    // reservations 40 must be ignored because max rows is 2
    @Test
    public void testTwoRowsFitThreeGroups40Reservation() {
        Assertions.assertEquals(3, canFit(2, "1A 40F 1C"),
                "2 rows, 1-1 and 1-3 reserved, 40-6 reservation ignored, 3 groups");
    }

    // reservations 40 must NOT be ignored
    @Test
    public void test40RowsFit78Groups40Reservation() {
        Assertions.assertEquals(78, canFit(40, "1A 40F 1C"),
                "40 rows, 1-1 and 1-3 reserved, 40-6 reservation, 1 + 76 + 1 = 78 groups");
    }

    @Test
    public void testOneTwoThreeRowsFitAisleNoReservations() {
        Assertions.assertEquals(2, canFit(1, "1 row, no reservations, 2 groups total"));
        Assertions.assertEquals(4, canFit(2, "2 rows, no reservations, 4 groups total"));
        Assertions.assertEquals(6, canFit(3, "3 rows, no reservations, 6 groups total"));
    }

    public static void main(String[] args) {
//        System.out.println(canFit(2, "1A 1K"));
//        System.out.println(canFit(1, "1F"));
        System.out.println(canFit(2, "1A 2F 1C"));
    }

}
