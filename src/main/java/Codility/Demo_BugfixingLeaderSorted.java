package Codility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class Demo_BugfixingLeaderSorted {

    static int solution(int[] A) {
        int n = A.length;
        int[] L = new int[n + 1];
        L[0] = -1;
        for (int i = 0; i < n; i++) {
            L[i + 1] = A[i];
        }
        System.out.println(Arrays.toString(L));

        int count = 0;
        int pos = (n + 1) / 2;
        int candidate = L[pos];

        System.out.println(pos);
        System.out.println(candidate);

        for (int i = 1; i <= n; i++) {
            System.out.println(L[i] + " vs " + candidate);
            if (L[i] == candidate)
                count = count + 1;
        }

        System.out.println("count: " + count + " vs pos: " + pos );
        System.out.println("count: " + count + " vs n: " + n );

//      if (count > pos)
        if (2*count > n)
            return candidate;
        return -1;
    }

    @Test
    public void testSolution1() {
        int[] a = new int[]{ 2,2,2,2,2,3,4,4,4,6 };
        Assertions.assertEquals(-1, solution(a));
    }

    @Test
    public void testSolution2() {
        int[] a = new int[]{ 2,2,2,2,2,2,4,4,4,6 };
        Assertions.assertEquals(2, solution(a));
    }

    @Test
    public void testSolution3() {
        int[] a = new int[]{ 1,1,1,1,50 };
        Assertions.assertEquals(1, solution(a));
    }

    @Test
    public void testSolution4() {
        int[] a = new int[]{ 1, 2, 2 };
        Assertions.assertEquals(2, solution(a));
    }

    @Test
    public void testSolution5() {
        int[] a = new int[]{ 2, 1, 2, 1 };
        Assertions.assertEquals(-1, solution(a));
    }

    @Test
    public void testSolution6() {
        int[] a = new int[]{ 2, 1, 2, 1, 1 };
        Assertions.assertEquals(1, solution(a));
    }

    public static void main(String[] args) {
        int[] b = new int[]{ 2,2,2,2,2,2,4,4,4,6 };
        System.out.println(solution(b));
    }

}
