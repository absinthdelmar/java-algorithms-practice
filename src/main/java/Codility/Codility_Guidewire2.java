package Codility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * plane seat allocation for a family (group) of 4
 * similar to cinema seat allocation on leetcode, but seats are encoded with ABCDEFGHJK
 */
public class Codility_Guidewire2 {

    static Pattern pattern = Pattern.compile("^([0-9]+)([ABCDEFGHJK])$");

    // result is the map of row and list of reserved seats per row
    // key is the row number
    // value is the list of seats
    static Map<Integer, List<Integer>> parseOccupiedSeats(String s) {
        if ( s == null )
            return new HashMap<>();
        Map<String, Integer> map = new HashMap<>();
        map.put("A", 1);
        map.put("B", 2);
        map.put("C", 3);
        map.put("D", 4);
        map.put("E", 5);
        map.put("F", 6);
        map.put("G", 7);
        map.put("H", 8);
        map.put("J", 9);
        map.put("K", 10);

        // row, list of reserved seats
        Map<Integer,List<Integer>> result = new HashMap<>();
        String[] reserved = s.split(" ");
        for ( String seat : reserved ) {
            Matcher m = pattern.matcher(seat);
            while ( m.find() ) {
                String row = m.group(1);
                String column = m.group(2);
                System.out.println(row + " at " + column);
                System.out.println(Integer.parseInt(row) + " at " + map.get(column));
                result.put( Integer.parseInt(row), Collections.singletonList( map.get(column) ) );
            }
        }
        return result;
    }

    // calculate total groups of four people can seat based on reservations per row
    static int totalGroupsOfFour(List<Integer> reservationRow){
        // default allocation
        int first = 1;
        int second = 1;
        int third = 0;

        // check if normal two variants of allocation is possible, excluding obvious scenarios
        // set counters to zero if reserved seat is between two numbers, so family cannot fit
        for (int i = 0; i < reservationRow.size(); i++) {
            int seat = reservationRow.get(i);
            if ( seat >= 2 && seat <= 5 )
                first = 0;
            if ( seat >= 6 && seat <= 9 )
                second = 0;
        }

        // check if third variant of allocation is still possible
        // when only leftmost or rightmost or both seats are reserved,
        // so two families can fit when aisle split possible
        if ( first == 0 && second == 0 ) {
            third = 1;
            for (int i = 0; i < reservationRow.size(); i++) {
                int seat = reservationRow.get(i);
                if ( seat >= 4 && seat <= 7 )
                    third = 0;
            }
        }

        // summarize obtained results from all cases
        return first + second + third;
    }

    static int solution(int N, String S) {
        // basic case: 1 row, no allocated seats, so maximum two families
        if ( N == 1 && S != null && S.length() == 0 )
            return 2;

        // default result is 2 multiplied by N rows, which is 2 families (groups of four) per row
        int result = 2 * N;

        // get rows of reserved seats
        for ( List<Integer> row : parseOccupiedSeats(S).values() ) {
            int groups = totalGroupsOfFour(row);
//            System.out.println(row + " => " + groups);
            result -= 2 - groups;
        }

        return result;
    }

    @Test
    public void testSolutionOneRow() {
        Assertions.assertEquals(2, solution(1, ""));
    }

    @Test
    public void testSolutionTwoRows() {
        Assertions.assertEquals(2, solution(2, "1A 2F 1C"));
    }

    public static void main(String[] args) {
//        System.out.println(parseOccupiedSeats(40, "1A 3C 2B 40G 5A"));
//        System.out.println(parseOccupiedSeats("1A 2F 1C"));
//        parseOccupiedSeats(1, "");

//        for ( List<Integer> row : parseOccupiedSeats("1A 2F 1C") ) {
//            System.out.println(row + " => " + totalGroupsOfFour(row));
//        }

//        System.out.println(solution(1, ""));
        System.out.println(solution(2, "1A 2F 1C"));
//        System.out.println(solution(40, "1A 3C 2B 40G 5A"));
    }

}
