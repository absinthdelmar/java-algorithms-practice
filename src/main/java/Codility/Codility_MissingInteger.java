package Codility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class Codility_MissingInteger {

    // equilibrium index example
    static int solution0(int[] nums) {
        long sum = Arrays.stream(nums).sum();
        long ls = 0;
        for (int i = 0; i < nums.length; i++) {
            if ( ls == ( sum - ls - nums[i] ) ) {
                return i;
            }
            else {
                ls += nums[i];
            }
        }
        return -1;
    }

    /**
     * This is a demo task.
     *
     * Write a function:
     *
     * class Solution { public int solution(int[] A); }
     *
     * that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
     *
     * For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
     *
     * Given A = [1, 2, 3], the function should return 4.
     *
     * Given A = [−1, −3], the function should return 1.
     *
     * Write an efficient algorithm for the following assumptions:
     *
     * N is an integer within the range [1..100,000];
     * each element of array A is an integer within the range [−1,000,000..1,000,000].
     *
     */
    static int solution1(int[] nums) {
        nums = Arrays.stream(nums).filter(i -> i > 0).toArray();

        if ( nums.length == 0 )
            return 1;

        Arrays.sort(nums);

        System.out.println(Arrays.toString(nums));

        if ( nums[0] != 1 )
            return 1;

        for (int i = 0; i < nums.length - 1; i++) {
            if ( nums[i+1] - nums[i] > 1 )
                return nums[i] + 1;
        }

        return Arrays.stream(nums).max().getAsInt() + 1;
    }

    // still slow using TreeSet
    static int solution2(int[] nums) {
        Set<Integer> s1 = new TreeSet<>();
        Set<Integer> s2 = new TreeSet<>();
        for (int i = 0; i < nums.length; i++) {
            s1.add(nums[i]);
            s2.add(i + 1);
        }
        for (int i : s2)
            if ( !s1.contains(i) )
                return i;
        if ( s1.size() == s2.size() )
            return s2.size() + 1;
        return 1;
    }

    // fast, 100% Codility MissingInteger O(N*log(N))
    static int solution3(int[] A) {
        Arrays.sort(A);
        int min = 1;
        for ( int i : A )
            min += i == min ? 1 : 0;
        return min;
    }

    static int solution(int[] A) {
        if ( A.length == 0 )
            return 1;
        boolean[] b = new boolean[A.length+1];
        System.out.println(Arrays.toString(b));
        for (int i = 0; i < A.length; i++) {
            if ( A[i] < 0 || A[i] > b.length - 1 )
                continue;
            b[A[i]] = true;
        }
        System.out.println(Arrays.toString(b));
        for (int i = 1; i < b.length; i++) {
            if ( !b[i] )
                return i;
        }
        return A.length + 1;
    }

    @Test
    public void testSolution() {
        Assertions.assertEquals(5, solution(new int[]{1,3,6,4,1,2}));
    }

    @Test
    public void testSolution2() {
        Assertions.assertEquals(4, solution(new int[]{1,2,3}));
    }

    @Test
    public void testSolution3() {
        Assertions.assertEquals(1, solution(new int[]{-1, -3}));
    }

    @Test
    public void testSolution4() {
        Assertions.assertEquals(4, solution(new int[]{-999999, -999998, 1, 2, 3, 999998, 999997}));
    }

    @Test
    public void testSolution5() {
        Assertions.assertEquals(2, solution(new int[]{-999999, -999998, 999998, 999997, 999999, 1, 1000000}));
    }

    @Test
    public void testSolution6() {
        Assertions.assertEquals(1, solution(new int[]{-1,0,2,3,4,5,6,7,8,9}));
    }

    @Test
    public void testSolution7() {
        Assertions.assertEquals(2, solution(new int[]{ 1, 299999 }));
    }

    public static void main(String[] args) {
        System.out.println(solution(new int[]{1,3,6,4,1,2}));
    }

}
