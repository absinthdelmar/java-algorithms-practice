import java.util.Arrays;
import java.util.Random;

public class QuickSort {

    // swaps arr[i] and arr[j]
    static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    static int partition(int[] arr, int lowIndex, int highIndex) {
        // pick up pivot which is the latest element in array
        int pivot = arr[highIndex];
        int i = lowIndex - 1;
        System.out.println("lowIndex - 1 = " + i);
        System.out.println("pivot = " + pivot);
        for (int j = lowIndex; j < highIndex; j++) {
            System.out.println("j = " + j);
            if (arr[j] < pivot) {
                i++;
                swap(arr, i, j);
            }
        }
        swap(arr, i + 1, highIndex);
        return i + 1;
    }

    static void quickSort(int[] arr, int lowIndex, int highIndex) {
        System.out.println(Arrays.toString(arr));
        if ( lowIndex < highIndex ) {
            int pivotIndex = partition(arr, lowIndex, highIndex);
            System.out.println("pivotIndex: " + pivotIndex);
            // quick sort left from pivot part
            quickSort(arr, lowIndex, pivotIndex - 1);
            // quick sort right from pivot part
            quickSort(arr, pivotIndex + 1, highIndex);
        }
    }

    public static void main(String[] args) {
        Random random = new Random();
        int[] input = new int[10];
        for (int i = 0; i < input.length; i++)
            input[i] = random.nextInt(100);
        long start = System.currentTimeMillis();
        quickSort(input, 0, input.length - 1);
        long end = System.currentTimeMillis();
        System.out.println((end - start) + " ms");
        System.out.println(Arrays.toString(input));
    }

}
