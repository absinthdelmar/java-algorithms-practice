package Facebook2020;

import java.io.*;
import java.util.Collections;
import java.util.Stack;

/**
 * Problem A: Travel Restrictions
 */
public class TravelRestrictions {

    static Stack<String> readInput(BufferedReader br) throws IOException {
        Stack<String> list = new Stack<>();
        String line;
        while ( ( line = br.readLine() ) != null ) {
            System.out.println(line);
            list.push(line);
        }
        Collections.reverse(list);
        return list;
    }

    static void writeOutput(PrintWriter pw, String s) {
        pw.println(s);
        pw.flush();
    }

    static String solution(Stack<String> input) {
        String result = "";

        // number of airlines
        int T = Integer.parseInt(input.pop());

        System.out.println(T);

        String line;
        int i = 0, k = 0;
        while ( i < T ) {
            int countries = Integer.parseInt(input.pop());
            System.out.println("countries: " + countries);
            k = 0;
            while ( k < 2 ) {
                String directions = input.pop();
                System.out.println("dirs: " + directions);
                k++;
            }
            i++;
        }

        return result;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(args[0]));

        Stack<String> input = readInput(br);

        System.out.println(input);

        String output = solution(input);

        PrintWriter pw = new PrintWriter(new FileWriter(args[1]));
        writeOutput(pw, output);
    }
}
