import java.util.Arrays;

public class ArrayCopy {

    public static void main(String[] args) {
        int[] test = new int[]{1,2,3,4,5};
        System.out.println(Arrays.toString(test));
        System.out.println(Arrays.toString(Arrays.copyOf(test, 10)));
    }
}
